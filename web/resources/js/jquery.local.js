jQuery(function($) {
	$("#ciclo_actualiza").cycle();
	
	$.gCalReader({
        feedUri2: 'https://www.google.com/calendar/feeds/ji4tl4ukc2h87pnfv2fv77ukj8%40group.calendar.google.com/public/full',
		  feedUri: 'https://www.google.com/calendar/feeds/exalumnos.usm%40gmail.com/public/full',
        maxresultsEach: 5
      });
	
	
	$(".tab_content").hide();
	$(".tabs ul li:first").addClass("active").show();
	$(".tab_content:first").show();

	$(".tabs ul li").click(function()
	   {
		$(".tabs ul li").removeClass("active");
		$(this).addClass("active");
		$(".tab_content").hide();

		var activeTab = $(this).find("a").attr("href");
		$(activeTab).fadeIn();
		return false;
	});
	
	<!--Slider-->
	$(".paging").show();
	$(".paging a:first").addClass("active");
		
		var imageWidth = $(".window").width();
		var imageSum = $(".image_reel img").size();
		var imageReelWidth = imageWidth * imageSum;

	$(".image_reel").css({'width' : imageReelWidth});
	rotate = function(){
		var triggerID = $active.attr("rel") - 1; 
		var image_reelPosition = triggerID * imageWidth; 
	
		$(".paging a").removeClass('active'); 
		$active.addClass('active'); 
		$(".image_reel").animate({
			left: -image_reelPosition
		}, 500 );
	
	}; 
	
	rotateSwitch = function(){
		play = setInterval(function(){ //Set timer - this will repeat itself every X seconds
			$active = $('.paging a.active').next(); 
			if ( $active.length === 0) { 
				$active = $('.paging a:first'); 
			}
			rotate(); 
		}, 7000); 
	};
	
	rotateSwitch(); 
	$(".image_reel a").hover(function() {
		clearInterval(play);
	}, function() {
		rotateSwitch();
	});	
	
	//On Click
	$(".paging a").click(function() {
		$active = $(this); 
		clearInterval(play); 
		rotate(); 
		rotateSwitch(); 
		return false;
	});		
	<!-- toggle Maps -->
	$("#FiltroTimeHead").click(function () {
	$("#FiltrosTime").toggle("300");
	return false;
	}); 
	
	$("#FiltroDeptoHead").click(function () {
	$("#FiltrosDepto").toggle("300");
	return false;
	});
	$("#Filtroetiquetahead").click(function () {
	$("#Filtrosetiquetatime").toggle("300");
	return false;
	}); 
	
	$("#FiltroapellHead").click(function () {
	$("#Filtrosapell").toggle("300");
	return false;
	});
	
	$("#FiltroEmpresaHead").click(function () {
	$("#FiltrosEmpresa").toggle("300");
	return false;
	}); 
	
	$("#FiltroUbicacionHead").click(function () {
	$("#FiltrosUbicacion").toggle("300");
	return false;
	}); 
	
	$("#FiltrocttHead").click(function () {
	$("#Filtrosctt").toggle("300");
	return false;
	}); 
	
	$("#FiltroactHead").click(function () {
	$("#Filtrosact").toggle("300");
	return false;
	}); 
	
	$('#logout').click(function(){
	window.location.replace("/_logout");
	});
	
	$("#FiltroSectorHead").click(function () {
	$("#FiltrosSector").toggle("300");
	return false;
	}); 
	$(".HeaderMenu a").click(function () {
	  $(this).toggleClass("active");
	});
	
	$('#search-input input').focus(function(){
		if($('#search-input input').val() == "Filtrar Contactos"){
			$('#search-input input').val("");
		}
	});
	
	$('#search-input input').blur(function(){
		if($('#search-input input').val() == ""){
			$('#search-input input').val("Filtrar Contactos");
		}
	});
//En Revisi�n, Omitir x el momento
	 /*$(".HeaderMenu a").click(function () {
	  $(this).attr("title", ($(this).hasClass("active")?"Cerrar":"Abrir") + " Panel");
	}); */
	
	
	$(".messageBox").click(function(){
		var type = $(this).attr("class").split(" ")[1]; // todos los links messageBox deben ser de la forma <a href="..." class="messageBox tipoDeMensaje">
		/* TODO: asignar estilo segun tipo de messageBox */
		
		$("#messageBox").fadeIn("fast");

	});
	
	$("#noButton").click(function(){
		$("#messageBox").fadeOut("slow");
	});
	$("#okButton").click(function(){
		/* Enviar datos AJAX */
		$("#messageBox").fadeOut("slow");
	});
	
	
	
	/* feed candy */
	$(".navList.preview .item").click(function(){
		var top = $(this).attr("href"); // cual hay que marcar
		var item = $(top);
		
		$("#feedList .item.focus").removeClass("focus");
		$(item).addClass("focus");
		jQuery.scrollTo( item, 800, {axis:'y'});
		
		return false;
	});
	
	$("#novedades, #mensajeria, #grupos, #red").click(function(){
		$("#feedList .item.focus").removeClass("focus");
		$(this).addClass("focus");
		jQuery.scrollTo( this, 800, {axis:'y'});
	});
	
	/* /feed */	
	
	/* events */
	$(".navList.events .item").live("click",function(){
		var top = $(this).attr("href"); // cual hay que marcar
		var item = $(top);
		
		$(item).toggle("fast");
		
		return false;

	});
}); 