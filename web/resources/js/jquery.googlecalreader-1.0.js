/// <reference path="jquery-1.5.1.min.js" />

/*
* Google Calendar feed reader - plugin to get upcoming events from a *public* google calendar
* Parameters: FeedUri, MaxResults & DisplayCount
* @version 1.0
*/

(function ($) {
    //Add gcal element
    $(document).ready(function () {
        $('.navList.events.aexa .info, .navList.events.insercion .info').html('<img src="/resources/img/ajax-loader.gif" height="12" width="12" alt="cargando..."> Cargando eventos...');
    });

    //Resize image on ready or resize
    $.gCalReader = function (options) {
        //Default settings
        var settings = {
            feedUri: 'https://www.google.com/calendar/feeds/ji4tl4ukc2h87pnfv2fv77ukj8%40group.calendar.google.com/public/full',
            feedUri2: 'https://www.google.com/calendar/feeds/exalumnos.usm%40gmail.com/public/full',
            maxresultsEach: 5,
            displayCount: 1, 
            notAvailable: '<span style="font-style:italic; color:#999;">No disponible</span>'
        };

        var feedUri = options.feedUri;
        if (feedUri.indexOf("public/full") == -1) {
            feedUri = settings.feedUri;
        }
        var feedUri2 = options.feedUri2;
        if (feedUri2.indexOf("public/full") == -1) {
            feedUri2 = settings.feedUri2;
        }

        var options = $.extend(settings, options);

        function _run() {
            var calendarService = new google.gdata.calendar.CalendarService('GoogleInc-jsguide-1.0');

            var query = new google.gdata.calendar.CalendarEventQuery(feedUri);
            var query2 = new google.gdata.calendar.CalendarEventQuery(feedUri2);
            query.setOrderBy('starttime');
            query2.setOrderBy('starttime');
            query.setSortOrder('ascending');
            query2.setSortOrder('ascending');
            query.setFutureEvents(true);
            query2.setFutureEvents(true);
            query.setSingleEvents(true);
            query2.setSingleEvents(true);
            query.setMaxResults(options.maxresultsEach);
            query2.setMaxResults(options.maxresultsEach);

            var callback = function (result) {

                var entries = result.feed.getEntries();
                $('.navList.events.insercion').html('');
                if (options.displayCount) {
                    $('#insercionCount').html("[" + entries.length + "]");
                }
                

                for (var i = 0; i < entries.length; i++) {
                    var eventEntry = entries[i];
                    var eventTitle = eventEntry.getTitle().getText();
                    var startDateTime = null;
                    var eventDate = null;
                    var eventWhere = null;
                    var eventContent = eventEntry.getContent().getText();

                    var times = eventEntry.getTimes();
                    if (times.length > 0) {
                        startDateTime = times[0].getStartTime();
                        eventDate = startDateTime.getDate();
                    }

                    var d_names = new Array("Domingo", "Lunes", "Martes", "Mi&eacute;rcoles", "Jueves", "Viernes", "S&aacute;bado");
                    var m_names = new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

                    var a_p = "";
                    var d = eventDate;
                    var curr_hour = d.getHours();
                    if (curr_hour < 12) {
                        a_p = "am";
                    }
                    else {
                        a_p = "pm";
                    }
                    if (curr_hour == 0) {
                        curr_hour = 12;
                    }
                    if (curr_hour > 12) {
                        curr_hour = curr_hour - 12;
                    }

                    var curr_min = d.getMinutes();
                    curr_min = curr_min + "";

                    if (curr_min.length == 1) {
                        curr_min = "0" + curr_min;
                    }

                    var time = curr_hour + ':' + curr_min + a_p;
                    var day = eventDate.getDay();
                    var month = eventDate.getMonth();
                    var date = eventDate.getDate();
                    var dayname = d_names[day];
                    var monthname = m_names[month];
                    var location = eventEntry.getLocations();
                    var eventWhere = location[0].getValueString();
                    var uid = eventEntry.getUid().getValue().split("@")[0];
                    
                    if(eventWhere == "") {eventWhere = settings.notAvailable;}
                    if(eventContent == "") {eventContent = settings.notAvailable;}
                  
                    $('.navList.events.insercion').append('<li><i class="icon"><u>evento</u></i><a class="item" href="#' + uid +'">' + eventTitle + '</a></li>');
                    $('.navList.events.insercion').append('<li id="' + uid + '" class="eventInfo"><strong>Cuando:</strong> ' + dayname + ' ' + date + ' de ' + monthname + '<br /><strong>Hora: </strong>' + time + '<br /><strong>Donde: </strong>' + eventWhere + '<br /><strong>Descripci&oacute;n: </strong>' + eventContent +'</li>');
                }
            };

            // Error handler to be invoked when getEventsFeed() produces an error
            var handleError = function (error) {
                $('.navList.events .info').html('Se ha producido un error al obtener los eventos.<br />Puedes intentar <a href="">recargar los eventos</a> o ver directamente el <a href="">Google Calendar</a>');
            };

            // Submit the request using the calendar service object
            calendarService.getEventsFeed(query, callback, handleError);
        }
        google.setOnLoadCallback(_run);

        $(window).load(function () {

        }); //End window load
    };

})(jQuery);