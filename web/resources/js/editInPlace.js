$.fn.editInPlace = function(options) { 
    var defaults = { 
       editClass: '.edit'
    };  
    var options = $.extend(defaults, options);

    return this.each(function() { 
        $(this).click(function(){
            $(options.editClass).each(function(index){
                if(!$(this).is(':visible'))
                    return false;
                var name = $(this).attr('id');
                var value = $(this).html();
                $(this).hide().after(createInput(name, value));    
            });    
        });
    });  
};

function createInput(name, value, opt){
    var optString = " ";
    if(opt !== undefined){
        $.map(opt, function(val, index){
            optString += index+'="'+val+'" ';
        });
    }
    return '<input type="text" name="'+name+'" value="'+value+'" '+optString+'>';
}