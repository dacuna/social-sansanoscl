package cl.utfsm.sansanoscl.dao;

import java.util.Date;
import java.util.List;

import cl.utfsm.academia.CalidadAlumno;
import cl.utfsm.aexa.AntecedenteEducacional;
import cl.utfsm.aexa.AntecedenteLaboral;
import cl.utfsm.aexa.personas.UsuarioAexa;
import cl.utfsm.aexa.redsocial.ContactoExAlumnos;
import cl.utfsm.aexa.redsocial.GrupoExAlumnos;
import cl.utfsm.aexa.redsocial.MensajeExAlumnos;
import cl.utfsm.aexa.redsocial.PaginaExAlumno;
import cl.utfsm.sansanoscl.models.ActividadExalumno;

public interface UsuarioDao {
	public List<ContactoExAlumnos> getContactosRecientes(Long userId, int cantidad);
	public List<PaginaExAlumno> getPaginasWebRecientes(Long userId, int cantidad);
	public List<AntecedenteEducacional> getAntecedenteEducacionalReciente(Long userId, int cantidad);
	public List<GrupoExAlumnos> getGruposCreadosReciente(Long userId, int cantidad);
	//public List<ContactoExAlumnos> getContactosDeUsuario(int userId, int cantidad, int offset);
	public List<UsuarioAexa> getUsuariosPorAntecedenteEducacional(Long userId, AntecedenteEducacional ant, int size);
	public List<UsuarioAexa> getUsuariosPorAntecedenteLaboral(Long userId, AntecedenteLaboral ant, int size);
	public AntecedenteLaboral getAntecedenteLaboralMasReciente(Long userId);
	public Number getTotalContactosDeUsuario(Long userId);
	public List<AntecedenteLaboral> getAntecedenteLaboralAnterior(Long userId, Integer size);
	public List<AntecedenteLaboral> getAntecedenteLaboralActual(Long userId, Integer size);
	public List<AntecedenteLaboral> getActividadLaboralReciente(Long userId, Integer dias, String actualOanterior);
	public List<MensajeExAlumnos> buscarMensajes(String termino, Long userId, Integer size, Integer offset);
	public Number getTotalMensajesEncontrados(String termino, Long userId);
	public Integer getCodigoVigenciaSolicitud(Long idEmisor, Long idReceptor);
	public List<ContactoExAlumnos> getSolicitudesContacto(Long idUser);
	public List<UsuarioAexa> superConsulta(int userId, AntecedenteLaboral ant, AntecedenteEducacional antl, int size);
	public Boolean paginaExAlumnoPerteneceUsuario(Long paginaId, Long userId);
	public List<MensajeExAlumnos> getMensajesRecibidosExAlumno(Long userId, Integer size, Integer offset);
	public Number getTotalMensajesRecibidosExAlumno(Long userId);
	public Number getTotalGrupos();
	public List<MensajeExAlumnos> getMensajesEnviadosExAlumno(Long userId, Integer size, Integer offset);
	public Number getTotalMensajesEnviadosExAlumno(Long userId);
	public UsuarioAexa getUsuarioAexaPorId(Long userId);
	public UsuarioAexa getUsuarioAexaPorEMail(String email);
	public List<PaginaExAlumno> getMensajerosUsuario(Long userId, Integer cantidad);
	public List<PaginaExAlumno> getPaginasWebUsuario(Long userId, Integer cantidad);
	public Date getFechaIngresoExAlumno(Integer rutUsuario);
	public String getSedeIngresoExAlumno(Integer rutUsuario);
	public List<GrupoExAlumnos> buscarGrupos(String cadena);
	public List<UsuarioAexa> buscarPersonas(String cadena);
	public List<GrupoExAlumnos> directorioGrupos(Integer size, Integer offset);
	public List<String> getCarreraAlumno(UsuarioAexa user);
	public List<ActividadExalumno> antecedentesLaboralesPasadosUsuario(Long userId);
	public List<ActividadExalumno> antecedentesLaboralesActualesUsuario(Long userId);
	public ActividadExalumno antecedentesLaboralMasActualUsuario(Long userId);
}
