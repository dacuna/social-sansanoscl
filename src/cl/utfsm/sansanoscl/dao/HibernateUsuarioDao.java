package cl.utfsm.sansanoscl.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import cl.utfsm.academia.CalidadAlumno;
import cl.utfsm.aexa.AntecedenteEducacional;
import cl.utfsm.aexa.AntecedenteLaboral;
import cl.utfsm.aexa.personas.UsuarioAexa;
import cl.utfsm.aexa.redsocial.ContactoExAlumnos;
import cl.utfsm.aexa.redsocial.GrupoExAlumnos;
import cl.utfsm.aexa.redsocial.MensajeExAlumnos;
import cl.utfsm.aexa.redsocial.PaginaExAlumno;
import cl.utfsm.sansanoscl.controller.perfil.PerfilController;
import cl.utfsm.sansanoscl.models.ActividadExalumno;


/**
 * Contiene los metodos necesarios para obtener, guardar, eliminar
 * y actualizar informacion correspondiente a usuarios de la plataforma,
 * esto incluye tambien datos de los usuarios como sitios web, 
 * mensajes y contactos. La principal diferencia con el dao por defecto
 * en la arquitectura es que el actual dao posee en la mayoria de sus 
 * metodos funcionalidades de paginacion de resultados.
 * 
 * @author      dacuna
 * @version     %I%, %G%                    
 * @since       1.0
 */
@Repository("usuarioDao")
public class HibernateUsuarioDao  implements UsuarioDao {
	
	private static final Logger logger = Logger.getLogger(HibernateUsuarioDao.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	/**
     * Retorna una lista compuesta por 'cantidad' contactos de un usuario ordenados 
     * por fecha de modificacion de manera descendente, es decir, los contactos mas 
     * recientes de un usuario especificado. Si no existen 'cantidad' de contactos
     * para devolver, entonces se devuelven 'n' resultados donde 'n' < 'cantidad'.
     *
     * @param     userId La id del usuario al que se le buscan los contactos recientes
     * @param     cantidad La maxima cantidad de resultados que se desean obtener
     * @return    Lista de contactos mas recientes de un usuario.
     * @see       ContactoExAlumnos
     * @since     1.0
     */
	@Override
	@SuppressWarnings("unchecked")
	public List<ContactoExAlumnos> getContactosRecientes(Long userId, int cantidad) {
		Criteria c = sessionFactory.getCurrentSession().createCriteria(ContactoExAlumnos.class);
		Criterion first = Restrictions.eq("usuarioAexaEmisor.id", userId);
		Criterion second = Restrictions.eq("usuarioAexaReceptor.id", userId);
		c.add(Restrictions.or(first, second));
		c.add(Restrictions.eq("tipoVigencia.codigo", 1));
		//c.addOrder(Order.desc("fechaModificacion"));
		if(cantidad != 0)
			c.setMaxResults(cantidad);
		return (List<ContactoExAlumnos>)c.list();
	}

	/**
     * Retorna una lista compuesta por 'cantidad' paginas web de un usuario ordenadas 
     * por fecha de modificacion de manera descendente, es decir, los sitios web mas 
     * recientes de un usuario especificado. Si no existen 'cantidad' de sitios web
     * para devolver, entonces se devuelven 'n' resultados donde 'n' < 'cantidad'.
     *
     * @param     userId La id del usuario al que se le buscan los sitios web mas recientes
     * @param     cantidad La maxima cantidad de resultados que se desean obtener
     * @return    Lista de paginas web mas recientes de un usuario
     * @see       PaginaExAlumno
     * @since     1.0
     */
	@Override
	@SuppressWarnings("unchecked")
	public List<PaginaExAlumno> getPaginasWebRecientes(Long userId, int cantidad) {
		Criteria c = sessionFactory.getCurrentSession().createCriteria(PaginaExAlumno.class);
		c.add(Restrictions.eq("usuarioAexa.id", userId));
		c.addOrder(Order.desc("fechaModificacion"));
		c.setMaxResults(cantidad);
		return (List<PaginaExAlumno>)c.list();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<AntecedenteEducacional> getAntecedenteEducacionalReciente(Long userId, int cantidad) {
		Criteria c = sessionFactory.getCurrentSession().createCriteria(AntecedenteEducacional.class).
		add(Restrictions.eq("idUsuarioAexa", userId)).
		add(Restrictions.ne("idUniversidad", 25)).
		addOrder(Order.desc("fechaModificacion")).
		setMaxResults(cantidad).
		setFetchMode("universidad",FetchMode.JOIN);
		return (List<AntecedenteEducacional>)c.list();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<GrupoExAlumnos> getGruposCreadosReciente(Long userId, int cantidad) {
		Criteria c = sessionFactory.getCurrentSession().createCriteria(GrupoExAlumnos.class);
		c.add(Restrictions.eq("usuarioEncargado.id", userId));
		c.addOrder(Order.desc("fechaModificacion"));
		c.setMaxResults(cantidad);
		return (List<GrupoExAlumnos>)c.list();
	}

	/**
     * Retorna una lista compuesta por 'cantidad' usuarios que poseen antecedentes  
     * educacionales similares al usuario especificado en userId. Se entiende por
     * similares a usuarios con igual anoIngreso o anoEgreso y que pertenescan a la
     * misma intitucion.
     *
     * @param     userId La id del usuario al que se le buscan los sitios web mas recientes
     * @param     ant Antecedente educacional del usuario especificado por userId
     * @param     size La maxima cantidad de resultados que se desean obtener
     ** @return   Lista de usuarios... 
     * @see       UsuarioAexa
     * @since     1.0
     */
	@Override
	@SuppressWarnings("unchecked")
	public List<UsuarioAexa> getUsuariosPorAntecedenteEducacional(Long userId, AntecedenteEducacional ant, int size) {
		Criteria c = sessionFactory.getCurrentSession().createCriteria(UsuarioAexa.class, "user");
		c.createAlias("antecedentesEducacionales", "antecedente");
		c.add(Restrictions.ne("id", userId));
		c.add(Restrictions.eq("antecedente.idUniversidad", ant.getIdUniversidad()));
		
		//busco similares por ano ingreso y egreso
		Criterion first = Restrictions.eq("antecedente.anoIngreso", ant.getAnoIngreso());
		Criterion second = Restrictions.eq("antecedente.anoEgreso", ant.getAnoEgreso());
		
		c.add(Restrictions.or(first, second));
		c.setMaxResults(size);		
		
		return (List<UsuarioAexa>)c.list();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<UsuarioAexa> getUsuariosPorAntecedenteLaboral(Long userId, AntecedenteLaboral ant, int size) {
		Criteria c = sessionFactory.getCurrentSession().createCriteria(UsuarioAexa.class, "user");
		c.createAlias("antecedentesLaborales", "antecedente");
		c.add(Restrictions.ne("id", userId));
		c.add(Restrictions.eq("antecedente.sucursalEmpresa.empresa.rut", ant.getSucursalEmpresa().getEmpresa().getRut()));
		c.setMaxResults(size);	
		
		return (List<UsuarioAexa>)c.list();
	}

	/**
     * Retorna el antecedente laboral mas reciente de un usuario especificado por 
     * userId. El criterio de mas reciente es el antecedente laboral con fecha de
     * ingreso mas cercana a la fecha actual.
     *
     * @param     userId La id del usuario al que se le buscan el antecedente
     * 			  laboral mas reciente
     * @return    Antecedente laboral mas reciente de un usuario
     * @see       UsuarioAexa
     * @see   	  AntecedenteLaboral
     * @since     1.0
     */
	@Override
	public AntecedenteLaboral getAntecedenteLaboralMasReciente(Long userId) {
		String query = "from AntecedenteLaboral m "+
	               "where m.idUsuarioAexa = :id "+
				   "order by fechaIngreso DESC";
		try{
		Query q = sessionFactory.getCurrentSession().createQuery(query).setParameter("id", userId);
		      q.setMaxResults(1);
		AntecedenteLaboral devolver=(AntecedenteLaboral)q.uniqueResult();
		return devolver;
		}catch (Exception e) {
			logger.error(e);
		}
		return null;
	}

	@Override
	public Number getTotalContactosDeUsuario(Long userId) {
		Criteria c = sessionFactory.getCurrentSession().createCriteria(ContactoExAlumnos.class);
		Criterion first = Restrictions.eq("usuarioAexaEmisor.id", userId);
		Criterion second = Restrictions.eq("usuarioAexaReceptor.id", userId);
		c.add(Restrictions.or(first, second));
		c.add(Restrictions.eq("tipoVigencia.codigo", 1));
		c.setProjection(Projections.rowCount());

		return (Number) c.uniqueResult();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<UsuarioAexa> superConsulta(int userId, AntecedenteLaboral ant,
			AntecedenteEducacional antl, int size) {
		Criteria c = sessionFactory.getCurrentSession().createCriteria(UsuarioAexa.class, "user");
		c.createAlias("antecedentesEducacionales", "eantecedente");
		c.add(Restrictions.ne("id", userId));
		
		Criterion universidad = Restrictions.eq("eantecedente.idUniversidad", antl.getIdUniversidad());
		//busco similares por ano ingreso y egreso
		Criterion first = Restrictions.eq("eantecedente.anoIngreso", antl.getAnoIngreso());
		Criterion second = Restrictions.eq("eantecedente.anoEgreso", antl.getAnoEgreso());
		Criterion anoIngresoOEgreso = Restrictions.or(first, second);
		
		Criterion antecedenteEducacional = Restrictions.and(universidad, anoIngresoOEgreso);
		
		if(ant != null && ant.getSucursalEmpresa() != null){
			c.createAlias("antecedentesLaborales", "lantecedente");
			Criterion antecedenteLaboral = Restrictions.eq("lantecedente.sucursalEmpresa.empresa.rut", ant.getSucursalEmpresa().getEmpresa().getRut()); 
			c.add(Restrictions.or(antecedenteEducacional, antecedenteLaboral));
		}else{
			c.add(antecedenteEducacional);
		}
	
		c.setMaxResults(size);		
		
		return (List<UsuarioAexa>)c.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AntecedenteLaboral> getAntecedenteLaboralAnterior(Long userId, Integer size) {
		String query = "from AntecedenteLaboral m "+
	               "where m.idUsuarioAexa = :id "+
				   "and m.fechaEgreso is not null "+
				   "order by fechaIngreso DESC";
		Query q = sessionFactory.getCurrentSession().createQuery(query).setParameter("id", userId);
		      q.setMaxResults(size);
		return (List<AntecedenteLaboral>)q.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AntecedenteLaboral> getAntecedenteLaboralActual(Long userId, Integer size) {
		Criteria c = sessionFactory.getCurrentSession().createCriteria(AntecedenteLaboral.class).
		setMaxResults(size).
		add(Restrictions.eq("idUsuarioAexa", userId)).
		add(Restrictions.isNull("fechaEgreso")).
		setFetchMode("tipoCargo",FetchMode.JOIN).
		addOrder(Order.desc("fechaIngreso"));  //<-- los mas recientes primero
		
		return (List<AntecedenteLaboral>)c.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AntecedenteLaboral> getActividadLaboralReciente(Long userId, Integer dias, String actualOanterior) {
		Criteria c = sessionFactory.getCurrentSession().createCriteria(AntecedenteLaboral.class).
		add(Restrictions.eq("idUsuarioAexa", userId));
		
		if(actualOanterior.compareTo("actual") == 0)
			c.add(Restrictions.isNull("fechaEgreso"));
		else
			c.add(Restrictions.isNotNull("fechaEgreso"));
		
		c.addOrder(Order.desc("fechaIngreso"));  //<-- los mas recientes primero
				
		//se calcula la fecha dada por fechaActual - dias
		Date fechaActual = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(fechaActual);
		cal.add(Calendar.DATE, -dias);  
		Date inicio = cal.getTime();
		
		//basta con agregar la condicion between en la consulta:
		c.add(Restrictions.between("fechaModificacion", inicio, fechaActual));
		
		return (List<AntecedenteLaboral>)c.list();
	}

	/**
     * Realiza una busqueda de mensajes utilizando como parametro 'termino', la  
     * busqueda se efectua buscando coincidencias por asunto del mensaje o nombre
     * ya sea de autor o receptor del mensaje con la condicion de que alguno de 
     * estos sea el usuario correspondiente a userId.
     *
     * @param 	  termino El String utilizado como termino de busqueda
     * @param     userId La id del usuario que tiene una sesion iniciada en el sistema
     * @param 	  size La cantidad de mensajes que se quieren obtener de la busqueda
     * @param     offset Posicion de inicio de busqueda en la base de datos
     * 
     * @return    Lista de mensajes encontrados en la busqueda
     * 
     * @see       MensajeExAlumnos
     * @since     1.0
     */
	@SuppressWarnings("unchecked")
	@Override
	public List<MensajeExAlumnos> buscarMensajes(String termino, Long userId, Integer size,
			Integer offset) {
		String query = "from MensajeExAlumnos m "+
				   "where (m.usuarioAexa.id = :id or m.usuarioAexaReceptor.id = :id) "+
				   	"and (m.usuarioAexa.nombres like upper(:name) "+
					   	"or m.usuarioAexaReceptor.nombres like upper(:name) "+
					   	"or m.asunto like :name)";
		Query q = sessionFactory.getCurrentSession().createQuery(query);
		q.setParameter("name","%"+termino+"%");
		q.setParameter("id", userId);
		q.setMaxResults(size);
		q.setFirstResult(offset);
		return (List<MensajeExAlumnos>)q.list();
		
//		
//		IndexerBean indexer = new IndexerBean(sessionFactory.getCurrentSession());
//		indexer.indexAllSmallClasses(MensajeExAlumnos.class);
//
//		BooleanQuery booleanQuery = new BooleanQuery();
//	    booleanQuery.add( new PrefixQuery( new Term( "asunto", termino ) ), BooleanClause.Occur.SHOULD );
//	    booleanQuery.add( new PrefixQuery( new Term( "mensaje", termino ) ), BooleanClause.Occur.SHOULD );
//	    //booleanQuery.add( new PrefixQuery( new Term( "id", termino ) ), BooleanClause.Occur.SHOULD);
//	    
//	    org.hibernate.Query fullTextQuery = Search.getFullTextSession(sessionFactory.getCurrentSession()).
//	    									createFullTextQuery(booleanQuery, MensajeExAlumnos.class);
//	
//	    List<MensajeExAlumnos> result = fullTextQuery.list();
//		return result;	
//		
		
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<GrupoExAlumnos> buscarGrupos(String cadena) {
	
		String query = "from GrupoExAlumnos m "+
		   "where upper(m.nombre) like :cadena "+
		   "or m.tipoGrupoExAlumnos.nombre like :cadena ";
			   
		Query q = sessionFactory.getCurrentSession().createQuery(query);
		q.setParameter("cadena","%"+cadena.toUpperCase()+"%");

		return (List<GrupoExAlumnos>) q.list();
		
		
//			
//		//indexerBean.indexAllSmallClasses(GrupoExAlumnos.class);
//	    BooleanQuery booleanQuery = new BooleanQuery();
//	    booleanQuery.add( new PrefixQuery( new Term( "nombre", cadena ) ), BooleanClause.Occur.SHOULD );
//	    booleanQuery.add( new PrefixQuery( new Term( "descripcion", cadena ) ), BooleanClause.Occur.SHOULD );
//	    booleanQuery.add( new PrefixQuery( new Term( "id", cadena ) ), BooleanClause.Occur.SHOULD);
//	    
//	    org.hibernate.Query fullTextQuery = Search.getFullTextSession(sessionFactory.getCurrentSession()).
//	    									createFullTextQuery(booleanQuery, GrupoExAlumnos.class);
//	    List<GrupoExAlumnos> result = fullTextQuery.list();
//		return result;	
		
		
	}
	@Override
	public Number getTotalMensajesEncontrados(String termino, Long userId) {
		String query = "select count(*) as n from MensajeExAlumnos m "+
				   "where (m.usuarioAexa.id = :id or m.usuarioAexaReceptor.id = :id) "+
				   	"and (m.usuarioAexa.nombres like upper(:name) "+
				   	"or m.usuarioAexaReceptor.nombres like upper(:name) "+
				   	"or m.asunto like :name)";
		Query q = sessionFactory.getCurrentSession().createQuery(query);
		q.setParameter("name","%"+termino+"%");
		q.setParameter("id", userId);
		q.setMaxResults(1);
		return (Number)q.uniqueResult();
	}

	@Override
	public Integer getCodigoVigenciaSolicitud(Long idEmisor,Long idReceptor) {
		String query = "select codVigencia from ContactoExAlumnos "+
					   "where idUsuarioAexaEmisor = :idEmisor and idUsuarioAexaReceptor = :idReceptor";
		Query q = sessionFactory.getCurrentSession().createQuery(query).
		setParameter("idEmisor", idEmisor).
		setParameter("idReceptor", idReceptor).
		setMaxResults(1);
		return (Integer)q.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ContactoExAlumnos> getSolicitudesContacto(Long idUser) {
		Criteria c = sessionFactory.getCurrentSession().createCriteria(ContactoExAlumnos.class).
		add(Restrictions.eq("idUsuarioAexaReceptor", idUser)).
		add(Restrictions.eq("codVigencia", 2));
		return (List<ContactoExAlumnos>)c.list();
	}

	@Override
	public Boolean paginaExAlumnoPerteneceUsuario(Long paginaId, Long userId) {
		String query = "select paginaExAlumnoId from PaginaExAlumno "+ 
					   "where paginaExAlumnoId = :paginaId and usuarioAexa.id = :userId";
		Query q = sessionFactory.getCurrentSession().createQuery(query).
		setParameter("paginaId", paginaId).
		setParameter("userId", userId).
		setMaxResults(1);
		
		Long id = (Long)q.uniqueResult();
		
		return id==null ? false : true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MensajeExAlumnos> getMensajesRecibidosExAlumno(Long userId,
			Integer size, Integer offset) {
		Criteria c = sessionFactory.getCurrentSession().createCriteria(MensajeExAlumnos.class).
		add(Restrictions.eq("usuarioAexaReceptor.id", userId)).
		addOrder(Order.desc("fechaEnvio")).
		setMaxResults(size).
		setFirstResult(offset);
		return (List<MensajeExAlumnos>)c.list();
	}

	@Override
	public Number getTotalMensajesRecibidosExAlumno(Long userId) {
		String query = "select count(*) as n from MensajeExAlumnos mensaje "+
					   "where mensaje.usuarioAexaReceptor.id = :userId ";
		Query q = sessionFactory.getCurrentSession().createQuery(query).
				  setParameter("userId", userId);
		return (Number)q.uniqueResult();
	}
	
	@Override
	public Number getTotalGrupos() {
		String query = "select count(*) as n from GrupoExAlumnos grupo";
		Query q = sessionFactory.getCurrentSession().createQuery(query);
		return (Number)q.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MensajeExAlumnos> getMensajesEnviadosExAlumno(Long userId,
			Integer size, Integer offset) {
		Criteria c = sessionFactory.getCurrentSession().createCriteria(MensajeExAlumnos.class).
				add(Restrictions.eq("usuarioAexa.id", userId)).
				addOrder(Order.desc("fechaEnvio")).
				setMaxResults(size).
				setFirstResult(offset);
		return (List<MensajeExAlumnos>)c.list();
	}

	@Override
	public Number getTotalMensajesEnviadosExAlumno(Long userId) {
		String query = "select count(*) as n from MensajeExAlumnos mensaje "+
				   "where mensaje.usuarioAexa.id = :userId ";
		Query q = sessionFactory.getCurrentSession().createQuery(query).
			  setParameter("userId", userId);
		return (Number)q.uniqueResult();
	}

	@Override
	public UsuarioAexa getUsuarioAexaPorId(Long userId) {
		String query = "from UsuarioAexa as user "+
					   "inner join fetch user.comuna "+
				       "where user.id = :usuarioId";
		Query q = sessionFactory.getCurrentSession().createQuery(query).
				  setParameter("usuarioId", userId).
				  setMaxResults(1);
		return (UsuarioAexa)q.uniqueResult();
	}
	
	@Override
	public UsuarioAexa getUsuarioAexaPorEMail(String email) {
		String query = "from UsuarioAexa as user "+
					   "inner join fetch user.comuna "+
				       "where user.email = :email";
		Query q = sessionFactory.getCurrentSession().createQuery(query).
				  setParameter("email", email).
				  setMaxResults(1);
		return (UsuarioAexa)q.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PaginaExAlumno> getMensajerosUsuario(Long userId, Integer cantidad) {
		String query = "from PaginaExAlumno as pagina "+
		               "where pagina.usuarioAexa.id = :usuarioId "+
				       "and pagina.url not like 'http%' order by fechaModificacion desc";
		Query q = sessionFactory.getCurrentSession().createQuery(query).setParameter("usuarioId", userId);
		q.setMaxResults(cantidad);
		return (List<PaginaExAlumno>)q.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PaginaExAlumno> getPaginasWebUsuario(Long userId,
			Integer cantidad) {
		String query = "from PaginaExAlumno as pagina "+
		               "where pagina.usuarioAexa.id = :usuarioId "+
				       "and pagina.url like 'http%' order by fechaModificacion desc";
		Query q = sessionFactory.getCurrentSession().createQuery(query).setParameter("usuarioId", userId);
		q.setMaxResults(cantidad);
		return (List<PaginaExAlumno>)q.list();
	}

	@Override
	public Date getFechaIngresoExAlumno(Integer rutUsuario) {
		String query = "select fechaCalidad from CalidadAlumno as calidad "+
	                   "where calidad.alumno.rut = :usuariorut "+
	                   "order by fechaCalidad asc";
		Query q = sessionFactory.getCurrentSession().createQuery(query).
				  setParameter("usuariorut", rutUsuario).
				  setMaxResults(1);
		return (Date)q.uniqueResult();
	}

	@Override
	public String getSedeIngresoExAlumno(Integer rutUsuario) {
		String query = "select sede.nombre from Sede sede, CalidadAlumno calidad "+
					   "where calidad.alumno.rut = :usuarioRut "+
					   "AND calidad.codigoSedeAlumno = sede.codigo "+
					   "order by calidad.fechaCalidad asc";
		Query q = sessionFactory.getCurrentSession().createQuery(query).
				  setParameter("usuarioRut", rutUsuario).
				  setMaxResults(1);
		return (String)q.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UsuarioAexa> buscarPersonas(String cadena) {
		if(cadena.compareTo("")==0)
			return null;
		String query = "select user from UsuarioAexa user "+
					   "where user.nombres like upper(:cadena) "+
					   "or user.paterno like upper(:cadena) "+
					   "or user.materno like upper(:cadena)";
		Query q = sessionFactory.getCurrentSession().createQuery(query).
				  setParameter("cadena", "%"+cadena+"%");
		return (List<UsuarioAexa>)q.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<GrupoExAlumnos> directorioGrupos(Integer size, Integer offset) {
		String query = "select grupo from GrupoExAlumnos grupo "+
					   "order by integrantes desc";
		Query q = sessionFactory.getCurrentSession().createQuery(query).
				  setMaxResults(size).setFirstResult(offset);
		return (List<GrupoExAlumnos>)q.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getCarreraAlumno(UsuarioAexa user) {		
		//Se ejecuta la parte 1 para obtener las carreras
		String query = "select cal from CalidadAlumno cal " +
			           "where cal.alumno.rut = :rut and cal.codigoSedeCarrera=1"+
			           "order by cal.codigoCarrera asc, cal.fechaCalidad asc";
		Query q = sessionFactory.getCurrentSession().createQuery(query);
		q.setParameter("rut", user.getRut());
		
		List<CalidadAlumno> calidades = q.list();
		
		logger.info("TENGO "+calidades.size()+" CALIDADES.");
		List<String> carrerasFinales = new ArrayList<String>();
		if(calidades.size() != 0)
		{
			//NUEVO CODIGO
			String queryCarreras="SELECT cs.nom_carrera as 'nombre_carrera',"+
								 "CASE WHEN (cal.cod_calidad = 1 OR cal.cod_calidad = 2) "+ 
								"THEN 'Estudiante de' WHEN cal.cod_calidad = 3 THEN 'Estudiante especial de' "+
								"WHEN cal.cod_calidad = 3    THEN 'Estudiante oyente de' WHEN (cal.cod_calidad IN (5,15,16)  AND u.usuaex_sexo = 1) THEN 'Egresado de' "+ 
								"WHEN (cal.cod_calidad IN (5,15,16) AND u.usuaex_sexo = 0) THEN 'Egresada de' "+
								"WHEN cal.cod_calidad = 6  THEN 'Memorista'  "+
								"WHEN (cal.cod_calidad = 7 AND u.usuaex_sexo = 1) THEN 'Titulado' "+ 
								"WHEN (cal.cod_calidad = 7 AND u.usuaex_sexo = 0) THEN 'Titulada'  "+
								"WHEN (cal.cod_calidad = 18) THEN 'Estudi�'  "+
								"WHEN (cal.cod_calidad = 20) THEN 'Estudiante de' "+ 
								"WHEN (cal.cod_calidad = 30) THEN 'Tesista'  "+ 
								"WHEN (cal.cod_calidad = 31) THEN 'Graduado'  "+
								"WHEN (cal.cod_calidad = 9)  THEN  cs.nom_carrera || 'Congelado' "+ 
								"END AS nombre_calidad "+
								"FROM usuario_aexa u JOIN carrera_alumno ca "+
								"ON (u.usuaex_rut = ca.alu_rut) JOIN calidad_alumno cal ON (ca.alu_rut = cal.alu_rut) "+
								"JOIN carrera_sede cs ON (ca.cod_carrera = cs.cod_carrera AND ca.cod_sede_carrera = cs.sed_cod_sede) "+
								"WHERE u.usuaex_id= "+user.getId()+" AND CONVERT(varchar, cs.sed_cod_sede) || '-' || "+
								"CONVERT(varchar, cs.cod_carrera) NOT IN ('1-3', '1-4', '1-5', '1-6', '1-86', '1-90', '1-99','3-0') "+
								"AND cal.cod_calidad IN (1,2,3,4,5,6,7,9,15,16,18,20,30,31)  AND cs.sed_cod_sede NOT IN (3,4,5) "+
								"AND ca.car_vigente = 1 AND cal.cal_fecha >= ALL ( SELECT cal2.cal_fecha FROM usuario_aexa u2  "+
								"JOIN carrera_alumno ca2 ON (u2.usuaex_rut = ca2.alu_rut) JOIN calidad_alumno cal2 ON (ca2.alu_rut = cal2.alu_rut ) "+
								"JOIN carrera_sede cs2 ON (ca2.cod_carrera = cs2.cod_carrera AND ca2.cod_sede_carrera = cs2.sed_cod_sede) "+
								"WHERE u2.usuaex_id= "+user.getId()+" AND cal2.cod_calidad IN (1,2,3,4,5,6,7,9,15,16,18,20,30,31) "+
								"AND cs2.cod_carrera = cs.cod_carrera AND cs2.sed_cod_sede = cs.sed_cod_sede AND ca2.car_vigente = 1 ) "+ 
								"AND year(cal.cal_fecha) - year(ca.car_fecha_ingreso) >= ALL ( SELECT year(cal2.cal_fecha) - year(ca2.car_fecha_ingreso) "+
								"FROM usuario_aexa u2 JOIN carrera_alumno ca2 ON (u2.usuaex_rut = ca2.alu_rut) JOIN calidad_alumno cal2 "+
								"ON (ca2.alu_rut = cal2.alu_rut ) JOIN carrera_sede cs2 ON (ca2.cod_carrera = cs2.cod_carrera AND ca2.cod_sede_carrera = cs2.sed_cod_sede) "+
								"WHERE u2.usuaex_id= "+user.getId()+" AND cal2.cod_calidad IN (1,2,3,4,5,6,7,9,15,16,18,20,30,31)  "+
								"AND cs2.cod_carrera = cs.cod_carrera  AND cs2.sed_cod_sede = cs.sed_cod_sede AND ca2.car_vigente = 1 )";
			
			Query qCarreras = sessionFactory.getCurrentSession().createSQLQuery(queryCarreras);
			List<Object[]> lCarreras = qCarreras.list();
			for(Object[] lista:lCarreras)
			{
				String nombreCarrera = (String)lista[0];
				String nombreCalidad = (String)lista[1];
				logger.info("CARRERA: "+nombreCarrera+ " NOMBRE CALIDAD "+nombreCalidad);
				carrerasFinales.add(nombreCalidad+' '+nombreCarrera);
			}
			/*List<CalidadAlumno> calidadesFinales = new ArrayList<CalidadAlumno>();
			
			if(calidades.size() == 1)
			{
				calidadesFinales.add(calidades.get(0));
			}
			else
			{
				int lastVisited = calidades.get(0).getCodigoCarrera();
				int anterior = -1;
				for(CalidadAlumno calidad:calidades)
				{
					if(lastVisited != calidad.getCodigoCarrera())
						calidadesFinales.add(calidades.get(anterior));
					lastVisited = calidad.getCodigoCarrera();
					anterior++;
				}
				//agrego la ultima
				calidadesFinales.add(calidades.get(anterior));
			}
			
			//con las calidades se obtienen las carreras
			String queryCarreras ="SELECT DATEPART(YEAR, ca.car_fecha_ingreso) AS a�o_ingreso, s.sed_cod_sede, s.sed_nom_sede, cs.sed_cod_sede, cs.abreviacion, cs.cod_carrera,"+
				"cs.nom_carrera, cm.cod_mencion, cm.nom_mencion, ca.pla_cod_plan, cs.dep_cod_departamento, cm.cod_jornada, cs.cod_clasificacion,"+
				"convert(varchar(8),ca.car_fecha_ingreso,112),  ca.car_vigente,ISNULL(ca.car_curso_alumno,0), p.pla_numero, cs.cod_clasificacion, cs.duracion "+ 
				"FROM carrera_alumno AS ca, carrera_sede AS cs,  carrera_mencion AS cm,  sede AS s, plan_general p "+
				"WHERE ca.alu_rut = "+user.getRut()+" "+
				"AND ca.cod_sede_alumno = s.sed_cod_sede "+ 
				"AND ca.car_vigente BETWEEN 1 AND 2 "+
				"AND ca.cod_sede_carrera = cs.sed_cod_sede "+ 
				"AND ca.cod_carrera = cs.cod_carrera "+
				"AND ca.cod_mencion = cm.cod_mencion "+
				"AND ca.cod_sede_carrera = cm.sed_cod_sede "+ 
				"AND cm.cod_carrera = ca.cod_carrera "+
				"AND p.pla_cod_plan = ca.pla_cod_plan "+
				"AND s.sed_cod_sede=1 "+
				"ORDER BY ca.car_fecha_ingreso DESC";
			Query qCarreras = sessionFactory.getCurrentSession().createSQLQuery(queryCarreras);
			List<Object[]> lCarreras = qCarreras.list();
			//se deben igualar los cod_carrera, en la consulta anterior estan en la columna 5, el nombre en la 6
			*/
			/*for(CalidadAlumno calidad:calidadesFinales)
			{
				logger.info("TENGO UNA CALIDAD!");
				Integer codigoCarrera = calidad.getCodigoCarrera();
				//hago el match
				for(Object[] lista:lCarreras)
				{
					Short codigoShort = (Short)lista[5];
					Integer codigo=Integer.parseInt(codigoShort.toString());
					String agregar=(String)lista[6];
					logger.info("CODIGO "+codigoShort + " CODIGO INT "+codigo+" CARRERA: "+agregar+ " CODIGO CARRERA "+codigoCarrera);
					if(codigo.compareTo(codigoCarrera)==0 && !carrerasFinales.contains(agregar))
					{
						carrerasFinales.add((String)lista[6]);
						logger.info("AGREGUE UNA CARRERA A LA LISTA FINAL");
					}
				}
			}*/
		}
		else
		{
			//ahora debo ejecutar la segunda parte, para los alumnos de las sedes que no tienen antecedentes bien puestos
			String carrerasPortalEmpleos ="SELECT car.nom_carrera AS CARRERA "+ 
										  "FROM carrera_sede car "+ 
										  "INNER JOIN antecedente_educacional a "+ 
										  "ON a.sed_cod_sede=car.sed_cod_sede AND a.cod_carrera=car.cod_carrera "+
										  "WHERE cod_institucion=25 AND a.usuaex_id="+user.getId();
			Query carrerasEmpleos = sessionFactory.getCurrentSession().createSQLQuery(carrerasPortalEmpleos).
					addScalar("CARRERA",Hibernate.STRING);
			List<String> carrerasSedes = carrerasEmpleos.list();
			for(String carrera:carrerasSedes)
			{
				carrerasFinales.add(carrera);
				
			}
		}
		
		return carrerasFinales;
		
	}

	@Override
	public List<ActividadExalumno> antecedentesLaboralesPasadosUsuario(Long userId) {
		String sql="SELECT a.actexa_cargo AS cargo, a.actexa_departamento AS departamento, " +
				"CASE " +
	"WHEN a.rut_empresa IS NULL THEN " +
	"ee.empextaex_nombre " + 
	"WHEN a.empextaex_id IS NULL THEN " +
	"e.nom_empresa " +
	"END as nombre_empresa , " + 
	"CASE WHEN datepart(mm,a.actexa_fecha_ingreso)=1 THEN 'Enero ' WHEN datepart(mm,a.actexa_fecha_ingreso)=2 THEN 'Febrero ' WHEN datepart(mm,a.actexa_fecha_ingreso)=3 THEN 'Marzo ' " +
	"WHEN datepart(mm,a.actexa_fecha_ingreso)=4 THEN 'Abril ' WHEN datepart(mm,a.actexa_fecha_ingreso)=5 THEN 'Mayo ' WHEN datepart(mm,a.actexa_fecha_ingreso)=6 THEN 'Junio ' " +
	"WHEN datepart(mm,a.actexa_fecha_ingreso)=7 THEN 'Julio ' WHEN datepart(mm,a.actexa_fecha_ingreso)=8 THEN 'Agosto ' WHEN datepart(mm,a.actexa_fecha_ingreso)=9 THEN 'Septiembre ' " +
	"WHEN datepart(mm,a.actexa_fecha_ingreso)=10 THEN 'Octubre ' WHEN datepart(mm,a.actexa_fecha_ingreso)=11 THEN 'Noviembre ' WHEN datepart(mm,a.actexa_fecha_ingreso)=12 THEN 'Diciembre ' " +
	"END +convert(char,datepart(yy,a.actexa_fecha_ingreso)) AS fecha_ingreso, " + 
	"CASE WHEN datepart(mm,a.actexa_fecha_egreso)=1 THEN 'Enero ' WHEN datepart(mm,a.actexa_fecha_egreso)=2 THEN 'Febrero ' WHEN datepart(mm,a.actexa_fecha_egreso)=3 THEN 'Marzo ' " +
	"WHEN datepart(mm,a.actexa_fecha_egreso)=4 THEN 'Abril ' WHEN datepart(mm,a.actexa_fecha_egreso)=5 THEN 'Mayo ' WHEN datepart(mm,a.actexa_fecha_egreso)=6 THEN 'Junio ' " +
	"WHEN datepart(mm,a.actexa_fecha_egreso)=7 THEN 'Julio ' WHEN datepart(mm,a.actexa_fecha_egreso)=8 THEN 'Agosto ' WHEN datepart(mm,a.actexa_fecha_egreso)=9 THEN 'Septiembre ' " +
	"WHEN datepart(mm,a.actexa_fecha_egreso)=10 THEN 'Octubre ' WHEN datepart(mm,a.actexa_fecha_egreso)=11 THEN 'Noviembre ' WHEN datepart(mm,a.actexa_fecha_egreso)=12 THEN 'Diciembre ' " +
	"END +convert(char,datepart(yy,a.actexa_fecha_egreso)) AS fecha_egreso, " +
	"a.actexa_descripcion as Descripcion " +
	" FROM actividad_exalumno a " +
	"LEFT JOIN sucursal_empresa se ON se.rut_empresa=a.rut_empresa AND se.suc_codigo=a.suc_codigo " + 
					"LEFT JOIN suc_empresa_extranjera_aexa sea ON sea.empextaex_id=a.empextaex_id AND sea.sucempextaex_id=a.sucempextaex_id " + 
					"LEFT JOIN empresa e ON e.rut_empresa=a.rut_empresa " +
					"LEFT JOIN empresa_extranjera_aexa ee ON ee.empextaex_id=a.empextaex_id " + 
					"WHERE a.actexa_fecha_egreso IS NOT NULL AND a.usuaex_id="+userId+ " " +
					"ORDER BY a.actexa_fecha_ingreso DESC";
		
		Query queryAnt = sessionFactory.getCurrentSession().createSQLQuery(sql);
		List<Object[]> antecedentes = queryAnt.list();
		List<ActividadExalumno> finales=new ArrayList<ActividadExalumno>();
		for(Object[] lista:antecedentes)
		{
			String cargo = (String)lista[0];
			String departamento = (String)lista[1];
			String nombre_empresa = (String)lista[2];
			ActividadExalumno nueva=new ActividadExalumno(cargo, departamento, nombre_empresa);
			nueva.setFecha_ingreso((String)lista[3]);
			nueva.setFecha_egreso((String)lista[4]);
			nueva.setDescripcion((String)lista[5]);
			finales.add(nueva);
		}
		
		return finales;
	}
	
	@Override
	public List<ActividadExalumno> antecedentesLaboralesActualesUsuario(Long userId) {
		String sql="SELECT a.actexa_cargo AS cargo, a.actexa_departamento AS departamento, " +
			"CASE " +
"WHEN a.rut_empresa IS NULL THEN " +
"ee.empextaex_nombre " + 
"WHEN a.empextaex_id IS NULL THEN " +
"e.nom_empresa " +
"END as nombre_empresa , " + 
"CASE WHEN datepart(mm,a.actexa_fecha_ingreso)=1 THEN 'Enero ' WHEN datepart(mm,a.actexa_fecha_ingreso)=2 THEN 'Febrero ' WHEN datepart(mm,a.actexa_fecha_ingreso)=3 THEN 'Marzo ' " +
"WHEN datepart(mm,a.actexa_fecha_ingreso)=4 THEN 'Abril ' WHEN datepart(mm,a.actexa_fecha_ingreso)=5 THEN 'Mayo ' WHEN datepart(mm,a.actexa_fecha_ingreso)=6 THEN 'Junio ' " +
"WHEN datepart(mm,a.actexa_fecha_ingreso)=7 THEN 'Julio ' WHEN datepart(mm,a.actexa_fecha_ingreso)=8 THEN 'Agosto ' WHEN datepart(mm,a.actexa_fecha_ingreso)=9 THEN 'Septiembre ' " +
"WHEN datepart(mm,a.actexa_fecha_ingreso)=10 THEN 'Octubre ' WHEN datepart(mm,a.actexa_fecha_ingreso)=11 THEN 'Noviembre ' WHEN datepart(mm,a.actexa_fecha_ingreso)=12 THEN 'Diciembre ' " +
"END +convert(char,datepart(yy,a.actexa_fecha_ingreso)) AS fecha_ingreso, " + 
"CASE WHEN datepart(mm,a.actexa_fecha_egreso)=1 THEN 'Enero ' WHEN datepart(mm,a.actexa_fecha_egreso)=2 THEN 'Febrero ' WHEN datepart(mm,a.actexa_fecha_egreso)=3 THEN 'Marzo ' " +
"WHEN datepart(mm,a.actexa_fecha_egreso)=4 THEN 'Abril ' WHEN datepart(mm,a.actexa_fecha_egreso)=5 THEN 'Mayo ' WHEN datepart(mm,a.actexa_fecha_egreso)=6 THEN 'Junio ' " +
"WHEN datepart(mm,a.actexa_fecha_egreso)=7 THEN 'Julio ' WHEN datepart(mm,a.actexa_fecha_egreso)=8 THEN 'Agosto ' WHEN datepart(mm,a.actexa_fecha_egreso)=9 THEN 'Septiembre ' " +
"WHEN datepart(mm,a.actexa_fecha_egreso)=10 THEN 'Octubre ' WHEN datepart(mm,a.actexa_fecha_egreso)=11 THEN 'Noviembre ' WHEN datepart(mm,a.actexa_fecha_egreso)=12 THEN 'Diciembre ' " +
"END +convert(char,datepart(yy,a.actexa_fecha_egreso)) AS fecha_egreso, " +
"a.actexa_descripcion as Descripcion " +
" FROM actividad_exalumno a " +
"LEFT JOIN sucursal_empresa se ON se.rut_empresa=a.rut_empresa AND se.suc_codigo=a.suc_codigo " + 
				"LEFT JOIN suc_empresa_extranjera_aexa sea ON sea.empextaex_id=a.empextaex_id AND sea.sucempextaex_id=a.sucempextaex_id " + 
				"LEFT JOIN empresa e ON e.rut_empresa=a.rut_empresa " +
				"LEFT JOIN empresa_extranjera_aexa ee ON ee.empextaex_id=a.empextaex_id " + 
				"WHERE a.actexa_fecha_egreso IS NULL AND a.usuaex_id="+userId+ " " +
				"ORDER BY a.actexa_fecha_ingreso DESC";
		
		Query queryAnt = sessionFactory.getCurrentSession().createSQLQuery(sql);
		List<Object[]> antecedentes = queryAnt.list();
		List<ActividadExalumno> finales=new ArrayList<ActividadExalumno>();
		for(Object[] lista:antecedentes)
		{
			String cargo = (String)lista[0];
			String departamento = (String)lista[1];
			String nombre_empresa = (String)lista[2];
			ActividadExalumno nueva=new ActividadExalumno(cargo, departamento, nombre_empresa);
			nueva.setFecha_ingreso((String)lista[3]);
			nueva.setFecha_egreso("Presente");
			nueva.setDescripcion((String)lista[5]);
			finales.add(nueva);
		}
		
		return finales;
	}
	
	@Override
	public ActividadExalumno antecedentesLaboralMasActualUsuario(Long userId) {
		String sql="SELECT a.actexa_cargo AS cargo, a.actexa_departamento AS departamento, " +
				"CASE " +
	"WHEN a.rut_empresa IS NULL THEN " +
	"ee.empextaex_nombre " + 
	"WHEN a.empextaex_id IS NULL THEN " +
	"e.nom_empresa " +
	"END as nombre_empresa  " + 
	" FROM actividad_exalumno a " +
	"LEFT JOIN sucursal_empresa se ON se.rut_empresa=a.rut_empresa AND se.suc_codigo=a.suc_codigo " + 
					"LEFT JOIN suc_empresa_extranjera_aexa sea ON sea.empextaex_id=a.empextaex_id AND sea.sucempextaex_id=a.sucempextaex_id " + 
					"LEFT JOIN empresa e ON e.rut_empresa=a.rut_empresa " +
					"LEFT JOIN empresa_extranjera_aexa ee ON ee.empextaex_id=a.empextaex_id " + 
					"WHERE a.actexa_fecha_egreso IS NULL AND a.usuaex_id="+userId+ " " +
					"ORDER BY a.actexa_fecha_ingreso DESC";
		
		Query queryAnt = sessionFactory.getCurrentSession().createSQLQuery(sql);
		queryAnt.setMaxResults(1);
		List<Object[]> antecedentes = queryAnt.list();
		List<ActividadExalumno> finales=new ArrayList<ActividadExalumno>();
		for(Object[] lista:antecedentes)
		{
			String cargo = (String)lista[0];
			String departamento = (String)lista[1];
			String nombre_empresa = (String)lista[2];
			ActividadExalumno nueva=new ActividadExalumno(cargo, departamento, nombre_empresa);
			finales.add(nueva);
		}
		
		if(finales.size()>0)
			return finales.get(0);
		return null;
	}
	
	
	
}
