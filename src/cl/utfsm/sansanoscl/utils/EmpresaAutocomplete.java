package cl.utfsm.sansanoscl.utils;

public class EmpresaAutocomplete {
	private Integer rut;
	private String nombre;
	
	public Integer getRut() {
		return rut;
	}
	public void setRut(Integer rut) {
		this.rut = rut;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String toString(){
		return this.nombre;
	}
}
