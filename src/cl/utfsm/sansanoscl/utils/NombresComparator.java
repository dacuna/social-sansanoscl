package cl.utfsm.sansanoscl.utils;

import java.lang.reflect.Method;
import java.util.Comparator;

public class NombresComparator implements Comparator<Object> {

	@Override
	@SuppressWarnings("all")
	public int compare(Object arg0, Object arg1) {
		// se ordena por fechas
		try {
			Method for1 = arg0.getClass().getMethod("getNombreCompleto", null);
			Method for2 = arg1.getClass().getMethod("getNombreCompleto", null);
			// ahora se compara
			String dateObj1 = (String) for1.invoke(arg0, null);
			String dateObj2 = (String) for2.invoke(arg1, null);
			return dateObj1.compareTo(dateObj2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

}
