package cl.utfsm.sansanoscl.utils;

import java.util.Date;

import cl.utfsm.academia.zona.Comuna;
import cl.utfsm.aexa.AntecedenteLaboral;
import cl.utfsm.aexa.redsocial.PaginaExAlumno;

public class UsuarioSocial {
	private Long id;
	private String nombreCompleto;
	private String nombreIncompleto;
	private String foto;
	private AntecedenteLaboral laboralActual;
	private String titulo;
	private Comuna comuna;
	private Date fechaIngresoUSM;
	private String nombreSedeIngresoUSM;
	private PaginaExAlumno mensajeroInstantaneo;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	} 
	public String getNombreCompleto() {
		return nombreCompleto;
	}
	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = ModuloUtilidades.getNombreFormateado(nombreCompleto);
	}
	public String getNombreIncompleto() {
		return nombreIncompleto;
	}
	public void setNombreIncompleto(String nombreIncompleto) {
		this.nombreIncompleto = ModuloUtilidades.getNombreFormateado(nombreIncompleto);
	}
	public String getFoto() {
		return foto;
	}
	public void setFoto(String foto) {
		this.foto = foto;
	}
	public AntecedenteLaboral getLaboralActual() {
		return laboralActual;
	}
	public void setLaboralActual(AntecedenteLaboral laboralActual) {
		this.laboralActual = laboralActual;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public Comuna getComuna() {
		return comuna;
	}
	public void setComuna(Comuna comuna) {
		this.comuna = comuna;
	}
	public Date getFechaIngresoUSM() {
		return fechaIngresoUSM;
	}
	public void setFechaIngresoUSM(Date fechaIngresoUSM) {
		this.fechaIngresoUSM = fechaIngresoUSM;
	}
	public String getNombreSedeIngresoUSM() {
		return nombreSedeIngresoUSM;
	}
	public void setNombreSedeIngresoUSM(String nombreSedeIngresoUSM) {
		this.nombreSedeIngresoUSM = nombreSedeIngresoUSM;
	}
	public PaginaExAlumno getMensajeroInstantaneo() {
		return mensajeroInstantaneo;
	}
	public void setMensajeroInstantaneo(PaginaExAlumno mensajeroInstantaneo) {
		this.mensajeroInstantaneo = mensajeroInstantaneo;
	}
	
}
