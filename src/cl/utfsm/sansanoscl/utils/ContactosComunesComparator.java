package cl.utfsm.sansanoscl.utils;

import java.lang.reflect.Method;
import java.util.Comparator;
import java.util.Date;

public class ContactosComunesComparator implements Comparator<Object> {
	@Override
	@SuppressWarnings("all")
	public int compare(Object arg0, Object arg1) {
		// se ordena por fechas
		try {
			Method for1 = arg0.getClass().getMethod("getFechaModificacion", null);
			Method for2 = arg1.getClass().getMethod("getFechaModificacion", null);
			// ahora se compara
			Date dateObj1 = (Date) for1.invoke(arg0, null);
			Date dateObj2 = (Date) for2.invoke(arg1, null);
			return -dateObj1.compareTo(dateObj2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
}
