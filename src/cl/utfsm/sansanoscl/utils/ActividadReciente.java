package cl.utfsm.sansanoscl.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cl.utfsm.aexa.AntecedenteEducacional;
import cl.utfsm.aexa.AntecedenteLaboral;
import cl.utfsm.aexa.personas.UsuarioAexa;
import cl.utfsm.aexa.redsocial.GrupoExAlumnos;
import cl.utfsm.aexa.redsocial.PaginaExAlumno;

public class ActividadReciente {
	private String mensaje;
	private Date fechaModificacion;
	private String categoria;
	
	public String getMensaje() {
		return mensaje;
	}
	
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	public Date getFechaModificacion() {
		return fechaModificacion;
	}
	
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	
	public String getCategoria() {
		return categoria;
	}
	
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	
	public static List<ActividadReciente> setActividad(List<? extends Object> actividades, UsuarioAexa user){
		List<ActividadReciente> listaActividad = new ArrayList<ActividadReciente>();
		for(Object actividad:actividades){
			ActividadReciente act = new ActividadReciente();
			if(actividad instanceof UsuarioAexa){
				act.setFechaModificacion(((UsuarioAexa) actividad).getFecmodificacion());
				String mensaje = ModuloUtilidades.getNombreFormateado(user.getPrimerNombre()) + " ahora es amigo con "+ 
								 ModuloUtilidades.getNombreFormateado(((UsuarioAexa) actividad).getNombreIncompleto());
				act.setMensaje(mensaje);
				act.setCategoria("Amistad");
				listaActividad.add(act);
			}
			
			if(actividad instanceof PaginaExAlumno){
				act.setFechaModificacion(((PaginaExAlumno) actividad).getFechaModificacion());
				String mensaje = ModuloUtilidades.getNombreFormateado(user.getPrimerNombre()) + " ha agregado "+ 
								 ((PaginaExAlumno) actividad).getUrl()+ " a sus sitios web";
				act.setMensaje(mensaje);
				act.setCategoria("Sitios Web");
				listaActividad.add(act);
			}
			
			if(actividad instanceof AntecedenteLaboral){
				act.setFechaModificacion(((AntecedenteLaboral) actividad).getFechaModificacion());
				String mensaje = ModuloUtilidades.getNombreFormateado(user.getPrimerNombre()) + " ha agregado "+ 
								 ((AntecedenteLaboral) actividad).getSucursalEmpresa().getEmpresa().getNombre()+ " a sus lugares de trabajo";
				act.setMensaje(mensaje);
				act.setCategoria("Antecedentes Laborales");
				listaActividad.add(act);
			}
			
			if(actividad instanceof AntecedenteEducacional){
				act.setFechaModificacion(((AntecedenteEducacional) actividad).getFechaModificacion());
				String mensaje = ModuloUtilidades.getNombreFormateado(user.getPrimerNombre()) + " ha agregado "+ 
								 ((AntecedenteEducacional) actividad).getCarrera()+ " a sus estudios";
				act.setMensaje(mensaje);
				act.setCategoria("Antecedentes Educacionales");
				listaActividad.add(act);
			}
			
			if(actividad instanceof GrupoExAlumnos){
				act.setFechaModificacion(((GrupoExAlumnos) actividad).getFechaModificacion());
				String mensaje = ModuloUtilidades.getNombreFormateado(user.getPrimerNombre()) + " ha agregado "+ 
								 ((GrupoExAlumnos) actividad).getNombre()+ " a sus grupos";
				act.setMensaje(mensaje);
				act.setCategoria("Grupos");
				listaActividad.add(act);
			}
		}

		return listaActividad;
	}
}
