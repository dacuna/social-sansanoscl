package cl.utfsm.sansanoscl.utils;

import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import java.util.List;
import java.util.Calendar;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.log4j.Logger;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import cl.utfsm.aexa.personas.UsuarioAexa;

public class ModuloUtilidades {

	/** FUNCIONES DE UTILIDAD * */
	// cuando se entrega esos nombres que salen todos en mayusculas, esta
	// funcion los retorna mas elegantes el nombre que se le pasa es el que 
	// proviene de UsuarioAexa.getNombreIncompleto() o cualquier string que 
	// venga lleno de mayusculas o mezclados el metodo retorna cada inicial 
	// de cada palabra en mayuscula , el resto en minuscula
	public static String getNombreFormateado(String nombre) {
		String name = StringEscapeUtils.unescapeHtml(nombre);
		return WordUtils.capitalize(WordUtils.swapCase(name));
	}

	/* diferencia en dias entre date1 y la fecha actual now() */
	public static long diffDateActual(Date date1) {
		Date now = new Date();
		final long MILLSECS_PER_DAY = 24 * 60 * 60 * 1000; // Milisegundos al
															// d�a
		long dias = (now.getTime() - date1.getTime()) / MILLSECS_PER_DAY;

		return dias;
	}

	//fecha sin segundos
	public static String fechaDefault(Date fecha){
		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		String fechaFinal = formatter.format(fecha);
		return fechaFinal;
	}
	
	public static String fechaEventoEdit(Date fecha){
		DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		String fechaFinal = formatter.format(fecha);
		return fechaFinal;
	}
	
	//fecha solo anios
	public static String fechaGetYear(Date fecha){
		DateFormat formatter = new SimpleDateFormat("yyyy");
		String fechaFinal = formatter.format(fecha);
		return fechaFinal;
	}
	
	public static String fechaSocial(Date fecha){
		SimpleDateFormat formateador = new SimpleDateFormat("dd 'de' MMMM 'de' yyyy", new Locale("ES"));
		if(fecha != null){
			String fechaFinal = formateador.format(fecha);
			return fechaFinal;
		}
		return null;
	}
	
	public static String fechaEventos(Date fecha){
		SimpleDateFormat formateador = new SimpleDateFormat("dd 'de' MMMM 'de' yyyy, HH:mm 'Hrs.'", new Locale("ES"));
		if(fecha != null){
			String fechaFinal = formateador.format(fecha);
			return fechaFinal;
		}
		return null;
	}
	
	// entrega una fecha de manera amigable, ej: "5 dias", "6 segundo", "4
	// horas" etc.
	// autor dacuna 
	public static String fechaAmigable(Date fecha) {
		Date now = new Date();
		long diffDate = now.getTime() - fecha.getTime();
		// como referencia
		long segundos = 60 * 1000;
		long horas = 60 * segundos;
		long dias = 24 * horas;
		long result = 0;
		// se calcula si es amigable en segundos:
		if ((result = diffDate / segundos) < 60) {
			return Long.toString(result) + " minutos";
		}
		if ((result = diffDate / horas) < 24) {
			return Long.toString(result) + " horas";
		}

		return Long.toString(diffDate / dias) + " dias";
	}

	public static List<String> getListaAnios() {
		List<String> lista = new ArrayList<String>();

		Calendar actual = Calendar.getInstance();
		Integer anioActual = actual.get(Calendar.YEAR);

		for (Integer i = anioActual; i >= 1900; i--) {
			lista.add(i.toString());

		}

		return lista;

	}

	public static List<String> getListaMeses() {
		List<String> lista = new ArrayList<String>();

		lista.add("Enero");
		lista.add("Febrero");
		lista.add("Marzo");
		lista.add("Abril");
		lista.add("Mayo");
		lista.add("Junio");
		lista.add("Julio");
		lista.add("Agosto");
		lista.add("Septiembre");
		lista.add("Octubre");
		lista.add("Noviembre");
		lista.add("Diciembre");

		return lista;

	}
	
	@SuppressWarnings("all")  
	 public static void ordena(List lista, final String propiedad) {  
	  Collections.sort(lista, new Comparator() {  
	     
	   public int compare(Object obj1, Object obj2) {  
	      
	    Class clase = obj1.getClass();  
	    String getter = "get" + Character.toUpperCase(propiedad.charAt(0)) + propiedad.substring(1);  
	    try {  
	     Method getPropiedad = clase.getMethod(getter);  
	       
	     Object propiedad1 = getPropiedad.invoke(obj1);  
	     Object propiedad2 = getPropiedad.invoke(obj2);  
	       
	     if(propiedad1 instanceof Comparable && propiedad2 instanceof Comparable) {  
	      Comparable prop1 = (Comparable)propiedad1;  
	      Comparable prop2 = (Comparable)propiedad2;  
	      return prop1.compareTo(prop2);  
	     }//CASO DE QUE NO SEA COMPARABLE  
	     else {  
	      if(propiedad1.equals(propiedad2))  
	       return 0;  
	      else  
	       return 1;  
	  
	     }  
	    
	    }  
	    catch(Exception e) {  
	     e.printStackTrace();  
	    }  
	    return 0;  
	   }  
	  });  
	 }
	
	public static boolean checkIfNumber(String in) {
        try {
            Long.parseLong(in);    
        } catch (NumberFormatException ex) {
            return false;
        }     
        return true;
    }
	
	//parsea los errores de una entidad devolviendo un map<string,string> que puede ser utilizado por ejemplo
	//en una respuesta json. Si existen errores, entonces agrega un field extra llamado errores.exist
	//autor: dacuna
	public static Map<String, String> parseErrors(BindingResult result){
		Map<String, String> errores = new HashMap<String, String>();
		if (result.hasErrors()) {
			//response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			for (Object object : result.getAllErrors()) {
				if (object instanceof FieldError) {
					FieldError fieldError = (FieldError) object;
					errores.put(fieldError.getCode(), fieldError.getDefaultMessage());
					Logger.getRootLogger().warn("ERROR CODE: " + fieldError.getCode() + " MESSAGE: " + fieldError.getDefaultMessage());
				}
			}
		}
		
		if (!errores.isEmpty()){
			errores.put("errores.exist", "true");
		}
		
		return errores;
	}
	
	public static UsuarioSocial cargarUsuarioSocial(UsuarioAexa user){
		UsuarioSocial usuario = new UsuarioSocial();
		usuario.setId(user.getId());
		usuario.setNombreCompleto(user.getNombreCompleto());
		usuario.setNombreIncompleto(user.getNombreIncompleto());
		usuario.setComuna(user.getComuna());
		usuario.setFoto((user.getFoto() != null)?user.getFoto().toLowerCase():null);
		
		return usuario;
	}
	
	public static String formatCelular(String celular)
	{
		//es del tipo 0Xalgo
		String devolver=celular;
		if(celular.startsWith("0"))
			devolver=celular.substring(1);
		//ahora se cuenta el total de caracteres, un celular tiene 8 numeros
		Integer largo = devolver.length();
		if(largo>8) //devuelvo los ultimos 8 caracteres
			devolver=devolver.substring(largo-8);
		return devolver;
	}

}
