package cl.utfsm.sansanoscl.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

@Component("messageResource")
public class MessageResource {
	private String messageFile = "messages.properties";
	private Properties messages;
	private static final Logger logger = Logger.getLogger(MessageResource.class);
	
	public MessageResource(){
		InputStream is = MessageResource.class.getResourceAsStream(messageFile);

		if(is == null)
			logger.error("No se ha podido leer el archivo de mensajes");
		else{
			messages = new Properties();
			try{
				messages.load(is);
				is.close();
			}catch(IOException e){
				logger.error(e);
			}
		}
	}
	
	public String getMessage(String key){
		String value = messages.getProperty(key);
		return value == null ? null : value;
	}
}
