package cl.utfsm.sansanoscl.utils;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;

import org.apache.log4j.Logger;

public class JsonDeserializer {
	/**
	 * Transforma data JSON a un objeto determinado pasado por parametro Este
	 * metodo trata de resolver el problema que se tiene al pasar data JSON a un
	 * objeto complejo (es decir, compuesto por otros objetos no tipicos) usando
	 * RequestBody y ResponseBody. Esto se puede lograr utilizando las funciones
	 * de Reflection provistas por Java, en esta version del metodo solo
	 * contiene funcionalidad basica que DEBE ser mejorada y optimizada.
	 * 
	 * @param Object
	 *            object: representa el tipo de objeto que se quiere poblar con
	 *            data Json Object toParse: representa el objeto que contiene la
	 *            data (en spring es un LinkedHashMap)
	 * @return Object el objeto poblado con la data Json, posterior a recibirlo
	 *         se debe hacer un cast EXPLICITO
	 * @author dacuna
	 * @version 0.1a
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static Object parsingClass(Object object, Object toParse) {
		Class type = object.getClass();

		Field[] fields = type.getDeclaredFields();

		Object o = new Object();
		try {
			o = object.getClass().newInstance();
		} catch (InstantiationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IllegalAccessException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// se deben parsear los hashes obtenidos desde el objeto
		LinkedHashMap<String, String> data = (LinkedHashMap<String, String>) toParse;
		LinkedHashMap<String, String> c = new LinkedHashMap<String, String>();
		Iterator iterator = data.keySet().iterator();
		while (iterator.hasNext()) {
			String key = (String) iterator.next();
			// System.out.println(key + " " + value);
			// se verifica si es una propiedad compuesta, por defecto estas
			// propiedades deberian venir con un . separandola del tipo y de la
			// propiedad que
			// representa, por ejemplo si se tiene una propiedad llamada tipo
			// que es un objecto y
			// donde el valor representa a la propiedad de tipo llamada id
			// entonces el formulario
			// deberia tener un input llamado tipo.id
			int index = key.indexOf(".");
			if (index > 0) {
				// tengo un tipo compuesto
				String[] converTo = key.split("\\.");
				c.put(converTo[0], converTo[1]);
			}
		}

		for (Field field : fields) {
			if (c.size() > 0) {
				if (c.containsKey(field.getName())) {
					String getValue = c.get(field.getName());
					String dataGet = data.get(field.getName() + "." + getValue);
					field.setAccessible(true);
					// se sabe qe es un objeto
					try {
						Object tipo = field.getType().newInstance();
						// voy a obtener los fields del objeto que se quiere
						// llenar
						Field[] fieldObject = field.getType()
								.getDeclaredFields();
						for (Field f : fieldObject) {
							if (getValue.equals(f.getName())) {
								f.setAccessible(true);
								Object value;
								// tratar de asignar un int primitivo
								boolean flag = false;
								try {
									Integer num = Integer.parseInt(dataGet);
									value = num.intValue();
									f.set(tipo, value);
									flag = true;
								} catch (NumberFormatException e) {
									// TODO: handle exception
									flag = false;
								} catch (IllegalArgumentException e) {
									// TODO Auto-generated catch block
									// e.printStackTrace();
									flag = false;
								} catch (IllegalAccessException e) {
									// TODO Auto-generated catch block
									// e.printStackTrace();
									flag = false;
								}
								// sino, trato de asignar a otros tipos comunes
								// TODO: Se debe antes de realizar la transformacion verificar
								// que la data que se quiere transformar
								// no sea un campo vacio, Si no se verifica esto, el campo sera
								// asignable por el tipo de dato en la
								// clase pero arrojara una excepcion al transformar un campo
								// vacio a un tipo de dato distinto a string!!
								if (flag == false) {
									if (Integer.class.isAssignableFrom(f.getType())) {
										try{
											value = (Integer) Integer.parseInt(dataGet);
										}catch(NumberFormatException e){
											e.printStackTrace();
											value = (Integer) null;
										}
									} else if (Date.class.isAssignableFrom(f.getType())){
										SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd-MM-yyyy");
										String strFecha = data.get(dataGet);
										Date fecha = null;
										try {
											fecha = formatoDelTexto.parse(strFecha);
										} catch (ParseException ex) {
											ex.printStackTrace();
										}
										value = fecha;
									}
									
									else {
										value = (String) dataGet;
									}
									
									try{
										f.set(tipo, value);
									}catch (IllegalArgumentException e) {
										Logger.getRootLogger().warn(e);
									}
									
								}
							}
						}

						field.set(o, tipo);
					} catch (InstantiationException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (IllegalAccessException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}

			String fieldName = field.getName();
			if (data.containsKey(fieldName)) {
				data.get(fieldName);
				field.setAccessible(true);

				// System.out.println("TIPO: "+field.getType());
				Object value;
				// tratar de asignar un int primitivo
				boolean flag = false;
				try {
					Integer num = Integer.parseInt(data.get(fieldName));
					value = num.intValue();
					field.set(o, value);
					flag = true;
				} catch (NumberFormatException e) {
					// TODO: handle exception
					flag = false;
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					// e.printStackTrace();
					flag = false;
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					// e.printStackTrace();
					flag = false;
				}
				// sino, trato de asignar a otros tipos comunes
				// TODO: Se debe antes de realizar la transformacion verificar
				// que la data que se quiere transformar
				// no sea un campo vacio, Si no se verifica esto, el campo sera
				// asignable por el tipo de dato en la
				// clase pero arrojara una excepcion al transformar un campo
				// vacio a un tipo de dato distinto a string!!
				if (flag == false) {
					if (Integer.class.isAssignableFrom(field.getType()) && ModuloUtilidades.checkIfNumber(data.get(fieldName))) {
						value = (Integer) Integer.parseInt(data.get(fieldName));
					} else if (Long.class.isAssignableFrom(field.getType())) {
						value = (Long) Long.parseLong(data.get(fieldName));
					} else if (Boolean.class.isAssignableFrom(field.getType())) {
						value = (Boolean) Boolean.parseBoolean(data.get(fieldName));	
					} else if (String.class.isAssignableFrom(field.getType())){
						value = (String) data.get(fieldName);
					} else if (Date.class.isAssignableFrom(field.getType())){
						SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd-MM-yyyy HH:mm");
						String strFecha = data.get(fieldName);
						Date fecha = null;
						try {
							fecha = formatoDelTexto.parse(strFecha);
						} catch (ParseException ex) {
							Logger.getRootLogger().warn(ex);
						}
						value = fecha;
					} else {
						value = (String) data.get(fieldName);
					}
					try {
						field.set(o, value);
					} catch (IllegalArgumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				// Method toCall =
				// type.getDeclaredMethod("set"+WordUtils.capitalize(methodName),
				// null);

			}
		}

		return o;

	}
}
