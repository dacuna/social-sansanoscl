package cl.utfsm.sansanoscl.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import cl.utfsm.aexa.login.LoginAexa;
import cl.utfsm.aexa.modulos.ModuloAexa;
import cl.utfsm.aexa.modulos.ModuloGrupoAexa;
import cl.utfsm.aexa.modulos.ModuloUsuarioAexa;
import cl.utfsm.aexa.personas.UsuarioAexa;
import cl.utfsm.sansanoscl.service.UsuarioAexaService;

@Component
public class LoginInterceptor extends HandlerInterceptorAdapter {
	@Autowired ModuloUsuarioAexa moduloUsuario;
	@Autowired ModuloGrupoAexa moduloGrupo;
	@Autowired ModuloAexa moduloAexa;
	@Autowired UsuarioAexaService usuarioService;
	
	private static final Logger logger = Logger.getLogger(LoginInterceptor.class);
	
	@Override
	public void afterCompletion(HttpServletRequest arg0,
			HttpServletResponse response, Object arg2, Exception arg3)
			throws Exception {
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response,
			Object arg2, ModelAndView mav) throws Exception {
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse arg1,
			Object arg2) throws Exception {
		String param=request.getParameter("int");
		if(param==null || param.compareTo("asure")!=0){
			HttpSession session = request.getSession();
			if(session.getAttribute("userID") == null){
				String login = ((User)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
				LoginAexa usuario = moduloUsuario.getUsuarioByUsername(login);
				if (usuario != null) {
					UsuarioAexa user = usuario.getUsuarioAexa();
					session.setAttribute("userID", user.getId());
					session.setAttribute("nombreLoggedUser", ModuloUtilidades.getNombreFormateado(user.getNombreIncompleto()));
					logger.info("Creando sesiones de usuario para usuario "+ModuloUtilidades.getNombreFormateado(user.getNombreCompleto()));
					//obtengo las carreras del usuario
					/**List<String> carreras = usuarioService.getCarreraAlumno(user.getRut());
					for(String carrera:carreras){
						logger.info(carrera);
						GrupoExAlumnos grupo = moduloGrupo.loadGrupo(carrera);
						MiembroGrupoExalumnos miembro = moduloGrupo.usuarioPerteneceGrupo(user, carrera);
						//el usuario no pertenece al grupo, lo agrego
						if(grupo != null && miembro == null){
							MiembroGrupoExalumnos nuevoMiembro = new MiembroGrupoExalumnos();
							nuevoMiembro.setUsuarioAexa(user);
							nuevoMiembro.setTipoRolMiembroExAlumnos(moduloGrupo.getTipoRolesMiembrosExAlumnos("USUARIO"));
							nuevoMiembro.setGrupoExAlumnos(grupo);
							moduloGrupo.insertarMiembroGrupoExAlumnos(nuevoMiembro);
							logger.info("Se ha agregado al usuario al grupo "+carrera);
						}else{
							logger.info("Usuario ya pertenece a grupo: "+carrera);
						}
					}**/
				}
				else{
					logger.info("DESCONECTADO...");
					arg1.sendRedirect("/_logout");
				}
			}
			else{
				logger.info("Sesiones de usuario ya han sido creadas.");
				logger.info("El usuario ingresado corresponda al user ID "+session.getAttribute("userID"));
			}
			return true;
		}
		return true;
	}

}
