package cl.utfsm.sansanoscl.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import cl.utfsm.aexa.redsocial.EventoExAlumnos;

@Component("eventoExAlumnosValidator")
public class EventoExAlumnosValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return EventoExAlumnos.class.isAssignableFrom(clazz);
	}

	public void validateCreateEvent(EventoExAlumnos evento, Errors errors){
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nombre",
				"nombre.required", "Evento debe poseer un nombre");
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "descripcion", 
				"descripcion.required", "Debes especificar una descripci&oacute;n para tu evento");
		
		ValidationUtils.rejectIfEmpty(errors, "fecha", 
				"fecha.required", "Debes especificar una fecha para tu evento");
		
		if (evento.getGrupoExAlumnos() == null || evento.getGrupoExAlumnos().getId() == 0)
			errors.rejectValue("grupoExAlumnos","grupoExAlumnos.required", "Evento debe pertenecer a un grupo");
		
		if (evento.getGrupoExAlumnos() != null && evento.getGrupoExAlumnos().getId() == 0)
			errors.rejectValue("grupoExAlumnos","grupoExAlumnos.required", "Evento debe pertenecer a un grupo");
	}
	
	@Override
	public void validate(Object object, Errors errors) {
		EventoExAlumnos evento = (EventoExAlumnos) object;
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "id",
				"id.required", "Evento debe poseer un id");

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nombre",
				"nombre.required", "Evento debe poseer un nombre");
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "descripcion", 
				"descripcion.required", "Debes especificar una descripci&oacute;n para tu evento");
		
		ValidationUtils.rejectIfEmpty(errors, "fecha", 
				"fecha.required", "Debes especificar una fecha para tu evento");
		
		if (evento.getGrupoExAlumnos() == null || evento.getGrupoExAlumnos().getId() == 0)
			errors.rejectValue("grupoExAlumnos","grupoExAlumnos.required", "Evento debe pertenecer a un grupo");
		
		if (evento.getGrupoExAlumnos() != null && evento.getGrupoExAlumnos().getId() == 0)
			errors.rejectValue("grupoExAlumnos","grupoExAlumnos.required", "Evento debe pertenecer a un grupo");

	}

}
