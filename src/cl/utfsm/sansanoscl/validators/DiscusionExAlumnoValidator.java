package cl.utfsm.sansanoscl.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import cl.utfsm.aexa.redsocial.DiscusionExAlumnos;
import cl.utfsm.sansanoscl.utils.MessageResource;
@Component("DiscusionExAlumnoValidator")
public class DiscusionExAlumnoValidator implements Validator {
	@Autowired MessageResource messageSource;
	@Override
	public boolean supports(Class<?> clazz) {
		return DiscusionExAlumnos.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object object, Errors errors) {
		DiscusionExAlumnos discusion = (DiscusionExAlumnos) object;
		
		

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "titulo", 
				"titulo.required", "Debes incluir un t&iacute;tulo para tu descripci&oacute;n.");
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "detalle", 
				"detalle.required", "Debes escribir un mensaje para tu discusi&oacute;n.");
		
		if(discusion.getGrupoExAlumnos() == null){
			errors.rejectValue("grupoExAlumnos", "grupoExAlumnos.required", 
					"Tu discusi&oacute;n debe pertenecer a algun grupo v&aacute;lido");
		}
		
	}
	public void validarIdDiscusion(String idDiscusion, Errors errors){
		
		try{
			Integer.parseInt(idDiscusion);
		}catch (NumberFormatException e) {
			e.printStackTrace();
			errors.rejectValue("discusionExAlumnos", "discusion.invalid", messageSource.getMessage("discusion.invalid"));	
			
		}
		 
	}

}
