package cl.utfsm.sansanoscl.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import cl.utfsm.aexa.redsocial.PaginaExAlumno;
import cl.utfsm.sansanoscl.service.UsuarioAexaService;
import cl.utfsm.sansanoscl.utils.MessageResource;
import cl.utfsm.sansanoscl.utils.ModuloUtilidades;

@Component("paginaExAlumnoValidator")
public class PaginaExAlumnoValidator implements Validator {
	@Autowired MessageResource messageSource;
	@Autowired UsuarioAexaService userService;

	@Override
	public boolean supports(Class<?> clazz) {
		return PaginaExAlumno.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object object, Errors errors) {
		PaginaExAlumno website = (PaginaExAlumno) object;

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "descripcion",
				"descripcion.required", messageSource.getMessage("descripcion.required"));
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "url",
				"url.required", messageSource.getMessage("url.required"));

		if (website.getTipoPagina() == null)
			errors.rejectValue("tipoPagina", "tipoPagina.required", messageSource.getMessage("tipoPagina.required"));
		
	}
	
	public void validate(String paginaId, Long userId, Errors errors){
		if(!ModuloUtilidades.checkIfNumber(paginaId)){
			errors.rejectValue("paginaExAlumnoId", "paginaExAlumnoId.invalid", 
					messageSource.getMessage("paginaExAlumnoId.invalid"));
		}else{
			Boolean flag = userService.paginaExAlumnoPerteneceUsuario(Long.parseLong(paginaId), userId);	
			if(!flag)
				errors.rejectValue("paginaExAlumnoId", "idPagina.notOwner", 
						messageSource.getMessage("idPagina.notOwner"));
		}
	}
	
	public void validateEditarWeb(PaginaExAlumno paginaExAlumno, Long userId, Errors errors){
		
		if(paginaExAlumno.getPaginaExAlumnoId() == null)
			errors.rejectValue("paginaExAlumnoId", "idPagina.required", 
					messageSource.getMessage("idPagina.required"));
		else{
			Boolean flag = userService.paginaExAlumnoPerteneceUsuario(paginaExAlumno.getPaginaExAlumnoId(), userId);	
			if(!flag)
				errors.rejectValue("paginaExAlumnoId", "idPagina.invalid", 
						messageSource.getMessage("idPagina.invalid"));
		}
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "url",
				"url.required", messageSource.getMessage("url.required"));

		if (paginaExAlumno.getTipoPagina() == null)
			errors.rejectValue("tipoPagina", "tipoPagina.required", messageSource.getMessage("tipoPagina.required"));
		
	}

}
