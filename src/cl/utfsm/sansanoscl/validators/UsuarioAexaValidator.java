package cl.utfsm.sansanoscl.validators;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import cl.utfsm.aexa.personas.UsuarioAexa;

public class UsuarioAexaValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return UsuarioAexa.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object object, Errors errors) {
		// UsuarioAexa usuario = (UsuarioAexa) object;

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nombres",
				"nombres.required", "Debes especificar un nombre");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "paterno",
				"paterno.required", "Debes especificar un apellido paterno");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "materno",
				"materno.required", "Debes especificar un apellido materno");
	}

}
