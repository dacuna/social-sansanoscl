package cl.utfsm.sansanoscl.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

import cl.utfsm.aexa.modulos.ModuloUsuarioAexa;
import cl.utfsm.aexa.personas.UsuarioAexa;
import cl.utfsm.aexa.redsocial.MensajeExAlumnos;
import cl.utfsm.sansanoscl.utils.MessageResource;

@Component("mensajeExAlumnoValidator")
public class MensajeExAlumnoValidator{
	@Autowired ModuloUsuarioAexa moduloUsuario;
	@Autowired MessageResource messageSource;
	
	//valida el envio de mensajes
	public void validate(Object object, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "asunto",
				"asunto.required", messageSource.getMessage("asunto.required"));
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "mensaje",
				"mensaje.required", messageSource.getMessage("mensaje.required"));
	}
	
	//valida la carga y eliminacion de mensajes (userId permite validar si el usuario tiene permisos para ver
	//o eliminar el mensaje especificado
	public void validate(Object object, Errors errors, Long userId){
		MensajeExAlumnos mensaje = (MensajeExAlumnos) object;
		
		Boolean[] flag = {false, false};
		if (mensaje.getUsuarioAexa() == null || mensaje.getUsuarioAexa().getId() == null){
			errors.rejectValue("usuarioAexa", "remitente.invalid", messageSource.getMessage("remitente.invalid"));
			flag[0] = true;
		}
		if (mensaje.getUsuarioAexaReceptor() == null || mensaje.getUsuarioAexaReceptor().getId() == null){
			errors.rejectValue("usuarioAexaReceptor", "receptor.invalid", messageSource.getMessage("receptor.invalid"));
			flag[1] = true;
		}
		
		UsuarioAexa sender = !flag[0] ? moduloUsuario.loadUsuarioAexaById(mensaje.getUsuarioAexa().getId()) : null;
		UsuarioAexa receiver = !flag[1] ? moduloUsuario.loadUsuarioAexaById(mensaje.getUsuarioAexaReceptor().getId()) : null;
		
		if (sender == null)
			errors.rejectValue("usuarioAexa", "remitente.invalid", messageSource.getMessage("remitente.invalid"));
		if (receiver == null)
			errors.rejectValue("usuarioAexaReceptor", "receptor.invalid", messageSource.getMessage("receptor.invalid"));

		// ademas debo validar que alguno de los dos anteriores sea el usuario logeado
		if (sender != null && receiver != null)
			if (userId.compareTo(sender.getId()) != 0 && userId.compareTo(receiver.getId()) != 0)
				errors.reject("verMensaje.invalid", messageSource.getMessage("verMensaje.invalid"));
	}

}
