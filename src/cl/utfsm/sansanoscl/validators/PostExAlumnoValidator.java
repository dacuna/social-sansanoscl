package cl.utfsm.sansanoscl.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import cl.utfsm.aexa.redsocial.PostExAlumnos;
@Component("PostExAlumnoValidator")
public class PostExAlumnoValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.isAssignableFrom(PostExAlumnos.class);
	}

	@Override
	public void validate(Object obj, Errors errors) {
		PostExAlumnos post = (PostExAlumnos)obj;
		
		if(post.getRespuesta()==null || post.getRespuesta().equals(""))
			errors.rejectValue("respuesta", "respuesta.required", "Debes ingresar una respuesta a la discusion");
		if(post.getDiscusionExAlumnos() == null){
			errors.rejectValue("discusionExAlumnos", "discusion.required", "Debes seleccionar una discusion");
		}

	}

}
