package cl.utfsm.sansanoscl.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import cl.utfsm.aexa.redsocial.ContactoExAlumnos;
import cl.utfsm.sansanoscl.service.UsuarioAexaService;
import cl.utfsm.sansanoscl.utils.MessageResource;

@Component("contactoExAlumnosValidator")
public class ContactoExAlumnosValidator{
	@Autowired MessageResource messageSource;
	@Autowired UsuarioAexaService userService;
	
	public void validateSolicitud(Object object, Errors errors) {
		ContactoExAlumnos solicitud = (ContactoExAlumnos) object;
		
		if (solicitud.getIdUsuarioAexaReceptor() == null) {
			errors.rejectValue("idUsuarioAexaReceptor", "receptor.illegalValue",
					messageSource.getMessage("receptor.illegalValue"));
		} 
		
		if(solicitud.getIdUsuarioAexaEmisor() == null){
			errors.rejectValue("idUsuarioAexaEmisor", "emisor.illegalValue",
					messageSource.getMessage("emisor.illegalValue"));
		}
		
		//se obtiene el codigo de vigencia de la solicitud, este debe ser 2
		//TODO: Esta validacion esta mala puesto que se esta buscando el codigo de vigencia en la 
		//base de datos cuando la solicitud aun NO HA SIDO agregada a la db por lo que siempre la 
		//validacion fallara.
//		Integer codVigencia = userService.getCodigoVigenciaSolicitud(solicitud.getIdUsuarioAexaEmisor(), 
//				solicitud.getIdUsuarioAexaReceptor());
//		if(codVigencia == null || codVigencia != 2)
//			errors.rejectValue("codVigencia", "codVigencia.error",
//					messageSource.getMessage("codVigencia.error"));
	}

}
