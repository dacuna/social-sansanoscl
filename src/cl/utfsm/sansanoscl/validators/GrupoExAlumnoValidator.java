package cl.utfsm.sansanoscl.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import cl.utfsm.aexa.modulos.ModuloGrupoAexa;
import cl.utfsm.aexa.personas.UsuarioAexa;
import cl.utfsm.aexa.redsocial.GrupoExAlumnos;
import cl.utfsm.aexa.redsocial.MiembroGrupoExalumnos;
import cl.utfsm.sansanoscl.utils.MessageResource;

@Component("grupoExAlumnoValidator")
public class GrupoExAlumnoValidator implements Validator {
	@Autowired ModuloGrupoAexa moduloGrupo;
	@Autowired MessageResource messageSource;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return GrupoExAlumnos.class.isAssignableFrom(clazz);
	}
	
	@Override
	public void validate(Object object, Errors errors) {
		GrupoExAlumnos grupo = (GrupoExAlumnos) object;
		
		if(grupo.getNombre() == null || grupo.getNombre().compareTo("") == 0)
			errors.rejectValue("nombre", "nombre.required", "Debes ingresar un nombre para el grupo");
		
		
		//validar los permisos
		try{
			Integer.valueOf(grupo.getPermisos());
		}catch (NumberFormatException e) {
			errors.rejectValue("permisos", "permisos.required", "Debes ingresar algun tipo de permiso valido para el grupo");
		}catch (NullPointerException e){
			errors.rejectValue("nombre",  "permisos.required", "Debes ingresar algun tipo de permiso valido para el grupo");
		}		
		if (grupo.getTipoGrupoExAlumnos()== null || grupo.getTipoGrupoExAlumnos().getCodigo() == null)
			errors.rejectValue("tipoGrupoExAlumnos", "tipoGrupoExAlumnos.required", "Debes ingresar una categoria para tu grupo");
		
			
	}
	
	public void validateUsuarioPerteneceGrupo(GrupoExAlumnos grupo, UsuarioAexa user, Errors errors) {
		if (moduloGrupo.usuarioPerteneceGrupo(user, grupo) == null)
		{	//usuario no pertenece al grupo
			errors.rejectValue("miembros", "miembro.invalid", messageSource.getMessage("miembro.invalid"));		
		}
	}
	public void validateUsuarioPermisoInvitarGrupo(Object memb, Errors errors) {
		MiembroGrupoExalumnos membresia = (MiembroGrupoExalumnos) memb;
				
		String nombreRolUsuario = membresia.getTipoRolMiembroExAlumnos().getNombreUsuario();
		if (!nombreRolUsuario.equals("USUARIO") && !nombreRolUsuario.equals("ADMIN")) {
			//usuario no tiene permisos para invitar a usuario
			errors.rejectValue("usuarioAexa", "grupo.invitar.invalid", messageSource.getMessage("grupo.invitar.invalid"));
			
		}
		
	}
	public void validateUsuarioEncargadoGrupo(Object gr,Object us, Errors errors) {
		GrupoExAlumnos grupo = (GrupoExAlumnos) gr;
		UsuarioAexa user =  (UsuarioAexa) us;
		
		if (grupo.getUsuarioEncargado().getId() != user.getId())
		{	//usuario no es encargado de grupo
			errors.rejectValue("usuarioEncargado", "miembro.admin.invalid", messageSource.getMessage("miembro.admin.invalid"));
		}
			
	}
	public void validateUsuarioUnicoIntegranteGrupo(Object gr,Object us, Errors errors) {
		GrupoExAlumnos grupo = (GrupoExAlumnos) gr;
		
		if (grupo.getIntegrantes() != 1)
		{	
			errors.rejectValue("usuarioEncargado", "grupo.minimointegrante.invalid", messageSource.getMessage("grupo.minimointegrante.invalid"));
			
		}
		else{
			if(!grupo.getMiembros().contains(us)){
				errors.rejectValue("usuarioEncargado", "miembro.admin.invalid", messageSource.getMessage("miembro.admin.invalid"));
				
			}
			
		}
			
	}
	

	public void validateUsuarioInvitadoGrupo(Object gru,Object memb, Errors errors) {
		GrupoExAlumnos grupo = (GrupoExAlumnos) gru;
		MiembroGrupoExalumnos membresia =  (MiembroGrupoExalumnos) memb;
		
		String nombreRolUsuario = membresia.getTipoRolMiembroExAlumnos().getNombreUsuario();
    	if (!nombreRolUsuario.equals("INVITADO") && grupo.getMiembros().contains(membresia.getUsuarioAexa()))
		{	
		errors.rejectValue("usuarioAexa", "grupo.miembro.invalid", messageSource.getMessage("grupo.miembro.invalid"));
		
		
		}
		
			
	}
	public void validateUsuarioCrearEventos(Object memb, Errors errors) {
		MiembroGrupoExalumnos membresia =  (MiembroGrupoExalumnos) memb;
		
		boolean permiso = membresia.getTipoRolMiembroExAlumnos().getCrearEventos();
		if (!permiso)
			errors.rejectValue("usuarioAexa", "evento.permiso.invalid", messageSource.getMessage("evento.permiso.invalid"));
		
		
			
	}

	


}
