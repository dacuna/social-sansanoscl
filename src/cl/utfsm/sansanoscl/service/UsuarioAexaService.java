package cl.utfsm.sansanoscl.service;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import cl.utfsm.academia.CalidadAlumno;
import cl.utfsm.academia.Carrera;
import cl.utfsm.academia.Sede;
import cl.utfsm.aexa.AntecedenteEducacional;
import cl.utfsm.aexa.AntecedenteLaboral;
import cl.utfsm.aexa.modulos.ModuloAexa;
import cl.utfsm.aexa.modulos.ModuloUsuarioAexa;
import cl.utfsm.aexa.personas.UsuarioAexa;
import cl.utfsm.aexa.redsocial.ContactoExAlumnos;
import cl.utfsm.aexa.redsocial.GrupoExAlumnos;
import cl.utfsm.aexa.redsocial.MensajeExAlumnos;
import cl.utfsm.aexa.redsocial.PaginaExAlumno;
import cl.utfsm.aexa.tipos.TipoPagina;
import cl.utfsm.aexa.util.UtilAexa;
import cl.utfsm.sansanoscl.dao.UsuarioDao;
import cl.utfsm.sansanoscl.models.ActividadExalumno;
import cl.utfsm.sansanoscl.utils.ActividadReciente;
import cl.utfsm.sansanoscl.utils.ModuloUtilidades;
import cl.utfsm.sansanoscl.utils.UsuarioSocial;

@Service
public class UsuarioAexaService {
	@Autowired UsuarioDao usuarioDao;
	@Autowired ModuloUsuarioAexa moduloUsuario;
	@Autowired ModuloAexa moduloAexa;
	private static final int minimoSugeridos = 6;
	private static final Logger logger = Logger.getLogger(UsuarioAexaService.class);
	
	public List<UsuarioAexa> getContactosSugeridos(Long userId) {
		UsuarioAexa user = this.getUsuarioAexaPorId(userId);
		UsuarioSocial usuario = ModuloUtilidades.cargarUsuarioSocial(user);

		List<UsuarioAexa> listaSugeridos= new ArrayList<UsuarioAexa>();
		List<UsuarioAexa> contactos = moduloUsuario.getContactosByIdUser(user.getId());
		
		Set<AntecedenteEducacional> educacionales =  user.getAntecedentesEducacionales();
		Set<AntecedenteLaboral> laborales =  user.getAntecedentesLaborales();
		
		
		for(UsuarioAexa c : contactos){
			List<UsuarioAexa> contactsContacto = moduloUsuario.getContactosByIdUser(c.getId());
			for(UsuarioAexa cc : contactsContacto){
				if(user.getId()!= cc.getId() && !listaSugeridos.contains(cc) && !moduloUsuario.isContactoExAlumnos(cc.getId(), user.getId())){
						if(moduloUsuario.getContactosComunes(user, cc).size() > 1){
							listaSugeridos.add(cc);
			                break;
						}
						
				        if(cc.getComuna().getNombre().equals(usuario.getComuna().getNombre())){
				                listaSugeridos.add(cc);
				                break;
				        }
				        if(this.getFechaIngresoExAlumno(cc.getRut()).
				        		equals(this.getFechaIngresoExAlumno(user.getRut()))){
				                listaSugeridos.add(cc);
				                break;
				        }
				        Set<AntecedenteEducacional> educacionesContacto =  c.getAntecedentesEducacionales();
			            for(AntecedenteEducacional e : educacionesContacto )
			                if(comparaEducacional(e,educacionales)){
			                	listaSugeridos.add(cc);
				                break;
			                }
			            Set<AntecedenteLaboral> laboralesContacto = c.getAntecedentesLaborales();
			            for(AntecedenteLaboral e : laboralesContacto ){
			                if(comparaLaboral(e,laborales)){
			                	listaSugeridos.add(cc);
				                break;
			                }
						}
				}
			}
		}
		if(minimoSugeridos >= listaSugeridos.size() ){
			//ESTA TIRANDO ERROR EN PRODUCCION, ESPECIFICAMENTE LA FUNCION 
			//getUsuariosAexaPorAnoIngreso
			//List<AntecedenteEducacional> listaAe  = moduloUsuario.getAntecedenteEducacional(user);
			//listaAe.addAll(this.getCarrerasUSM(user));
			//System.out.println("listaAe: " + listaAe.size());
			//for(AntecedenteEducacional ae : listaAe){
		    // 	Integer anioIngreso = ae.getAnoIngreso();
			//	if(ae.getSedeCarrera()!= null){
			//		Set<UsuarioAexa> lista = moduloUsuario.getUsuariosAexaPorAnoIngreso(ae.getSedeCarrera().getSede().getCodigo(), ae.getSedeCarrera().getCodigo(), anioIngreso);
			//		logger.info("Encontre: " + lista.size());
			//		for(UsuarioAexa ua : lista ){
			//			if(ua.getId() != user.getId())
			//				listaSugeridos.add(ua);
			//		}
			//	}
			//}
		}
		
		System.out.println("cant. contactos sugeridoss: "+ listaSugeridos.size());
	
		//model.addAttribute("Utilidades", new ModuloUtilidades());
		//Collections.sort(listaSugeridos, new ContactosComunesComparator());
		//model.addAttribute("sugeridos", listaSugeridos);
		HashSet<UsuarioAexa> hs = new HashSet<UsuarioAexa>();
		hs.addAll(listaSugeridos);
		listaSugeridos.clear();
		listaSugeridos.addAll(hs);
		
		return listaSugeridos;

	}
			
	private boolean comparaEducacional(AntecedenteEducacional e, Set<AntecedenteEducacional> educaciones ){
		
		for(AntecedenteEducacional i: educaciones){
			try {
				if(i.getCarrera().equals(e.getCarrera()))return true;
				if(i.getIdUniversidad().equals(e.getIdUniversidad()))return true;
				if(i.getOtraInstitucion().equals(e.getOtraInstitucion()))return true;
				if(i.getUniversidad().getNombre().equals(e.getUniversidad().getNombre()))return true;
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			
		}
		
		return false;
		
	}
	
	private boolean comparaLaboral(AntecedenteLaboral l, Set<AntecedenteLaboral> laborales ){
		
		for(AntecedenteLaboral i: laborales){
			
			try {
				if(i.getCargo().equals(i.getCargo()))return true;
				if(i.getComuna().getNombre().equals(i.getComuna().getNombre()))return true;
				if(i.getDepartamento().equals(i.getDepartamento()))return true;
				if(i.getSucursalEmpresaExtranjeraAexa().getEmpresa().getNombre().
						equals(i.getSucursalEmpresaExtranjeraAexa().getEmpresa().getNombre()))return true;
				else if (i.getSucursalEmpresa().getEmpresa().getNombre().
						equals(i.getSucursalEmpresa().getEmpresa().getNombre()))return true;
			} catch (Exception e) {
				e.printStackTrace();
			}
					
		}
		
		return false;
		
	}
	
	
	
	public List<UsuarioAexa> getContactosComunes(UsuarioAexa usuario1, UsuarioAexa usuario2){
		
		List<UsuarioAexa> contactos1 = this.getContactosRecientes(usuario1.getId(), 0);
		List<UsuarioAexa> contactos2 = this.getContactosRecientes(usuario2.getId(), 0);
		List<UsuarioAexa> contactos = new ArrayList<UsuarioAexa>();
		
		for(UsuarioAexa c : contactos1 ){
			for(UsuarioAexa k : contactos2 ){
				if(c.getId() == k.getId())
					contactos.add(c);
			}
		}
		return contactos;	
	}
	
	public boolean obtenerActividadPerfilReciente(UsuarioAexa user){
		Date ultimaMod = user.getFecmodificacion();
		long dias = ModuloUtilidades.diffDateActual(ultimaMod);
		if(dias <= 15 ){ //ha pasado una semana desde que se modifico el perfil
			return true;
		}
		return false;
	}
	
	public String getCarrerasUSMUltimo(UsuarioAexa ua){
		CalidadAlumno lista = moduloAexa.getAntecedentesSigaUltimo(ua.getRut().toString());
		if(lista != null){
			Carrera carrera = moduloAexa.loadCarrera(lista.getCodigoCarrera(), lista.getCodigoSedeCarrera());
			String nombreCarrera = carrera.getNombre();
	        	
			return nombreCarrera;
		}
		
		return null;
	}
	
	//Este metodo se encontraba en el antiguo proyecto redsocial, escrito como parte de otro metodo en 
	//un interceptor, se adapto para que quedara como un metodo unico
	public List<AntecedenteEducacional> getCarrerasUSM(UsuarioAexa ua){
		List<CalidadAlumno> lista = moduloAexa.getAntecedentesSiga(ua.getRut().toString());
		
        List<AntecedenteEducacional> carrerasUsm = new ArrayList<AntecedenteEducacional>();
        List<Integer> porBorrar = new ArrayList<Integer>();

        for(Integer i = 0; i < lista.size(); i++){
            if(lista.get(i).getCalidad().getCodigo() == 1 && !porBorrar.contains(i))
                for(Integer j = 0; j < lista.size(); j++)
                    if(i != j && lista.get(i).getCalidad().getCodigo() == 1 && lista.get(j).getCalidad().getCodigo() == 1 && lista.get(i).getCodigoCarrera() == lista.get(j).getCodigoCarrera())
                        porBorrar.add(j);
        }

        for (Integer i = 0; i < lista.size(); i++) {
            if(porBorrar.contains(i))
                continue;
            CalidadAlumno c = lista.get(i);
            if (c.getCalidad().getCodigo() == 1) {
                AntecedenteEducacional ae = new AntecedenteEducacional();
                Sede se = new Sede();
                se.setCodigo(c.getCodigoSedeAlumno());

                Carrera ca = new Carrera();
                ca.setSede(se);
                ca.setCodigo(c.getCodigoCarrera());

                Carrera carr_pre_pro = moduloAexa.loadCarrera(c.getCodigoCarrera(), c.getCodigoSedeAlumno());
                if(carr_pre_pro != null)
                	ca.setNombre(carr_pre_pro.getNombre());

                ae.setSedeCarrera(ca);
                ae.setAnoIngreso(UtilAexa.getDatePart(c.getFechaCalidad(),"year"));
                for (CalidadAlumno cal : lista) {
                    if (c.getCodigoCarrera() == cal.getCodigoCarrera() && cal.getCalidad().getCodigo() == 7){
                        ae.setAnoTitulo(UtilAexa.getDatePart(cal.getFechaCalidad(),"year"));
                    }
                    if(c.getCodigoCarrera() == cal.getCodigoCarrera() && cal.getCalidad().getCodigo() == 31){
                        ae.setAnoEgreso(UtilAexa.getDatePart(cal.getFechaCalidad(),"year"));
                        ae.setAnoTitulo(UtilAexa.getDatePart(cal.getFechaCalidad(),"year"));
                    }
                    if (c.getCodigoCarrera() == cal.getCodigoCarrera() && cal.getCalidad().getCodigo() == 5){
                        ae.setAnoEgreso(UtilAexa.getDatePart(cal.getFechaCalidad(),"year"));
                    }
                }
                carrerasUsm.add(ae);
            }
        }
        
        return carrerasUsm;
	}
	

	public List<UsuarioAexa> getContactosRecientes(Long userId, int cantidad) {
				
		List<ContactoExAlumnos> listaContactos =  usuarioDao.getContactosRecientes(userId, cantidad);
		List<UsuarioAexa> listaUsuarios = new ArrayList<UsuarioAexa>();
		UsuarioAexa user = this.getUsuarioAexaPorId(userId);
		
		for(ContactoExAlumnos c: listaContactos){
			if(c.getUsuarioAexaEmisor().getId() != user.getId())listaUsuarios.add(c.getUsuarioAexaEmisor());
			else listaUsuarios.add(c.getUsuarioAexaReceptor());
		}
		
		return	listaUsuarios;
	}

	public PaginaExAlumno getMensajeroInstantaneo(Long userId){
		List<PaginaExAlumno> mensajeros = this.getMensajerosDeUsuario(userId, 1); //el ultimo
		if(mensajeros != null && mensajeros.size()>0){
			//agrego el primero
			PaginaExAlumno pag = mensajeros.get(0);
			String[] datosMensajero = pag.getUrl().split(":");
			TipoPagina tipo = new TipoPagina();
			tipo.setNombre(datosMensajero[0]);
			pag.setTipoPagina(tipo);
			pag.setUrl(datosMensajero[1]);
			return pag;
		}
		return null;
	}
	
	public List<ActividadReciente> getActividadRecienteUsuario(Long userId){
		UsuarioAexa user = this.getUsuarioAexaPorId(userId);
		List<ActividadReciente> actividadReciente = new ArrayList<ActividadReciente>();
		actividadReciente.addAll(ActividadReciente.setActividad(this.getContactosRecientes(userId, 2), user));
		actividadReciente.addAll(ActividadReciente.setActividad(this.getPaginasWebRecientes(userId, 2), user));
		actividadReciente.addAll(ActividadReciente.setActividad(this.getActividadLaboralReciente(userId, 15, "actual"), user));
		actividadReciente.addAll(ActividadReciente.setActividad(this.getActividadLaboralReciente(userId, 15, "anterior"), user));
		actividadReciente.addAll(ActividadReciente.setActividad(this.getAntecedenteEducacionalReciente(userId, 2), user));
		actividadReciente.addAll(ActividadReciente.setActividad(this.getGruposCreadosReciente(userId, 2), user));
		return actividadReciente;
	}
	
	/** METODOS HEREDADOS DE USUARIO DAO **/
	public List<PaginaExAlumno> getPaginasWebRecientes(Long userId, int cantidad) {
		return usuarioDao.getPaginasWebRecientes(userId, cantidad);
	}

	public List<AntecedenteEducacional> getAntecedenteEducacionalReciente(Long userId, int cantidad) {
		return usuarioDao.getAntecedenteEducacionalReciente(userId, cantidad);
	}

	public List<GrupoExAlumnos> getGruposCreadosReciente(Long userId, int cantidad) {
		return usuarioDao.getGruposCreadosReciente(userId, cantidad);
	}

	public List<UsuarioAexa> getUsuariosPorAntecedenteEducacional(Long userId, AntecedenteEducacional ant, int size) {		
		return usuarioDao.getUsuariosPorAntecedenteEducacional(userId, ant, size);
	}

	public List<UsuarioAexa> getUsuariosPorAntecedenteLaboral(int userId, AntecedenteLaboral ant, int size) {
		return getUsuariosPorAntecedenteLaboral(userId, ant, size);
	}

	public AntecedenteLaboral getAntecedenteLaboralMasReciente(Long userId) {	
		return usuarioDao.getAntecedenteLaboralMasReciente(userId);
	}

	public Number getTotalContactosDeUsuario(Long userId) {
		return usuarioDao.getTotalContactosDeUsuario(userId);
	}
	
	public List<AntecedenteLaboral> getAntecedenteLaboralAnterior(Long userId, Integer size) {
		return usuarioDao.getAntecedenteLaboralAnterior(userId, size);
	}

	public List<AntecedenteLaboral> getAntecedenteLaboralActual(Long userId, Integer size) {
		return usuarioDao.getAntecedenteLaboralActual(userId, size);
	}
	
	public List<UsuarioAexa> superConsulta(int userId, AntecedenteLaboral ant, AntecedenteEducacional antl, int size){
		return usuarioDao.superConsulta(userId, ant, antl, size);
	}
	
	public List<MensajeExAlumnos> buscarMensajes(String termino, Long userId, Integer size,
			Integer offset){
		return usuarioDao.buscarMensajes(termino, userId, size, offset);
	}
	
	public List<GrupoExAlumnos> buscarGrupos(String cadena) {
		return usuarioDao.buscarGrupos(cadena);	
	}
	
	public Number getTotalMensajesEncontrados(String termino, Long userId){
		return usuarioDao.getTotalMensajesEncontrados(termino, userId);
	}
	
	public Integer getCodigoVigenciaSolicitud(Long idEmisor,Long idReceptor){
		return usuarioDao.getCodigoVigenciaSolicitud(idEmisor, idReceptor);
	}
	
	public List<ContactoExAlumnos> getSolicitudesContacto(Long idUser){
		return usuarioDao.getSolicitudesContacto(idUser);
	}
	
	public Boolean paginaExAlumnoPerteneceUsuario(Long paginaId, Long userId) {
		return usuarioDao.paginaExAlumnoPerteneceUsuario(paginaId, userId);
	}
	
	public List<MensajeExAlumnos> getMensajesRecibidosExAlumno(Long userId, Integer size, Integer offset){
		return usuarioDao.getMensajesRecibidosExAlumno(userId, size, offset);
	}
	
	public Number getTotalMensajesRecibidosExAlumno(Long userId) {
		return usuarioDao.getTotalMensajesRecibidosExAlumno(userId);
	}
	
	public Number getTotalGrupos(){
		return usuarioDao.getTotalGrupos();
	}
	
	public List<MensajeExAlumnos> getMensajesEnviadosExAlumno(Long userId, Integer size, Integer offset){
		return usuarioDao.getMensajesEnviadosExAlumno(userId, size, offset);
	}
	
	public Number getTotalMensajesEnviadosExAlumno(Long userId) {
		return usuarioDao.getTotalMensajesEnviadosExAlumno(userId);
	}
	
	public UsuarioAexa getUsuarioAexaPorId(Long userId){
		return usuarioDao.getUsuarioAexaPorId(userId);
	}
	
	public UsuarioAexa getUsuarioAexaPorEmail(String email){
		return usuarioDao.getUsuarioAexaPorEMail(email);
	}
	
	public List<PaginaExAlumno> getMensajerosDeUsuario(Long userId, Integer cantidad){
		return usuarioDao.getMensajerosUsuario(userId, cantidad);
	}
	
	public List<PaginaExAlumno> getPaginasWebUsuario(Long userId, Integer cantidad){
		return usuarioDao.getPaginasWebUsuario(userId, cantidad);
	}
	
	public Date getFechaIngresoExAlumno(Integer rutUsuario) {
		return usuarioDao.getFechaIngresoExAlumno(rutUsuario);
	}
	
	public String getSedeIngresoExAlumno(Integer rutUsuario) {
		return usuarioDao.getSedeIngresoExAlumno(rutUsuario);
	}
	
	public List<AntecedenteLaboral> getActividadLaboralReciente(Long userId, Integer dias, String actualOanterior){
		return usuarioDao.getActividadLaboralReciente(userId, dias, actualOanterior);
	}
	
	public List<UsuarioAexa> buscarPersonas(String cadena){
		return usuarioDao.buscarPersonas(cadena);
	}
	
	public List<GrupoExAlumnos> directorioGrupos(Integer size, Integer offset) {
		return usuarioDao.directorioGrupos(size, offset);
	}
	
	public List<String> getCarreraAlumno(UsuarioAexa user){
		return usuarioDao.getCarreraAlumno(user);
	}
	
	public List<ActividadExalumno> antecedentesLaboralesPasadosUsuario(Long userId){
		return usuarioDao.antecedentesLaboralesPasadosUsuario(userId);
	}
	
	public List<ActividadExalumno> antecedentesLaboralesActualesUsuario(Long userId){
		return usuarioDao.antecedentesLaboralesActualesUsuario(userId);
	}
	
	public ActividadExalumno antecedentesLaboralMasActualUsuario(Long userId){
		return usuarioDao.antecedentesLaboralMasActualUsuario(userId);
	}
}
