package cl.utfsm.sansanoscl.controller.perfil;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cl.utfsm.sansanoscl.service.UsuarioAexaService;

@Controller
@RequestMapping(value = "/perfil/mensajeroInstantaneo")
public class AgregarMensajeroInstantaneoController {
	@Autowired UsuarioAexaService usuarioService;
	
	@RequestMapping(method = RequestMethod.GET)
	public String verPerfil(Model model, HttpSession session) {
		return "perfil/agregarMensajeroInstantaneo";
	}
		
}
