package cl.utfsm.sansanoscl.controller.perfil;

import java.util.Collections;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cl.utfsm.aexa.modulos.ModuloUsuarioAexa;
import cl.utfsm.aexa.redsocial.PaginaExAlumno;
import cl.utfsm.sansanoscl.utils.ModuloUtilidades;
import cl.utfsm.sansanoscl.validators.PaginaExAlumnoValidator;

@Controller
@RequestMapping(value = "/perfil/eliminarWeb")
public class EliminarPaginaExAlumnoController {
	@Autowired ModuloUsuarioAexa moduloUsuario;
	@Autowired PaginaExAlumnoValidator validator;
	
	private static final Logger logger = Logger.getLogger(EliminarPaginaExAlumnoController.class);

	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody Map<String, ? extends Object> delete(@RequestBody String paginaId, 
			HttpSession session) {

		Long userId = (Long) session.getAttribute("userID");
		BindingResult result = new BeanPropertyBindingResult(new PaginaExAlumno(), "");
		validator.validate(paginaId, userId, result);
		Map<String, String> errores = ModuloUtilidades.parseErrors(result);

		if(!errores.isEmpty()){
			return errores;
		} else {
			try {
				PaginaExAlumno eliminar = moduloUsuario.getPaginaExAlumno(Long.parseLong(paginaId), userId);
				moduloUsuario.eliminarPaginaExAlumno(eliminar);
			} catch (Exception e) {
				logger.warn(e);
				return Collections.singletonMap("paginaExAlumno.error", "true");
			}

			// se retorna un flag que indica que todo resulto correctamente
			return Collections.singletonMap("flag", "true");
		}
	}
}
