package cl.utfsm.sansanoscl.controller.perfil;

import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cl.utfsm.aexa.modulos.ModuloUsuarioAexa;
import cl.utfsm.aexa.redsocial.PaginaExAlumno;
import cl.utfsm.aexa.tipos.TipoPagina;
import cl.utfsm.sansanoscl.utils.JsonDeserializer;
import cl.utfsm.sansanoscl.utils.ModuloUtilidades;
import cl.utfsm.sansanoscl.validators.PaginaExAlumnoValidator;

@Controller
@RequestMapping(value = "/perfil/anadirWeb")
public class AnadirPaginaWebController {
	@Autowired ModuloUsuarioAexa moduloUsuario;
	@Autowired PaginaExAlumnoValidator validator;
	
	private static final Logger logger = Logger.getLogger(AnadirPaginaWebController.class);
	final String VISTA_FORM = "/perfil/agregarPaginaWeb";

	@RequestMapping(method = RequestMethod.GET)
	public String showForm(Model model, HttpSession session) {
		model.addAttribute("paginaExAlumno", new PaginaExAlumno());
		model.addAttribute("tipos_pagina", moduloUsuario.getListaTipoPagina());
		return VISTA_FORM;
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody Map<String, ? extends Object> create(@RequestBody Object requested, 
			HttpSession session) {

		PaginaExAlumno paginaExAlumno = new PaginaExAlumno();
		paginaExAlumno = (PaginaExAlumno) JsonDeserializer.parsingClass(paginaExAlumno, requested);

		BindingResult result = new BeanPropertyBindingResult(paginaExAlumno, "");
		validator.validate(paginaExAlumno, result);
		Map<String, String> errores = ModuloUtilidades.parseErrors(result);

		// Se envian los errores si es que existen
		if (!errores.isEmpty()){
			return errores;
		}			
		// sino, se procesa la data
		else {
			paginaExAlumno.setUsuarioAexa(moduloUsuario.loadUsuarioAexaById((Long) session.getAttribute("userID")));
			paginaExAlumno.setFechaModificacion(new Date());

			try {
				moduloUsuario.savePaginaWeb(paginaExAlumno);
			} catch (Exception e) {
				logger.warn(e);
				return Collections.singletonMap("paginaWeb.error", "true");
			}

			return Collections.singletonMap("flag", "true");
		}
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(method = RequestMethod.POST, value="/mensajeroInstantaneo")
	public @ResponseBody Map<String, ? extends Object> createMensajero(@RequestBody Object requested, 
			HttpSession session) {

		LinkedHashMap<String, String> data = (LinkedHashMap<String, String>) requested;
		
		String tipo = data.get("tipos");
		String username = data.get("username");
		
		String mensajero = tipo+ ":" + username;
		PaginaExAlumno paginaExAlumno = new PaginaExAlumno();
		paginaExAlumno.setDescripcion("Mensajero Instantaneo");
		paginaExAlumno.setUrl(
				(tipo.compareTo("")==0 || tipo == null || username == null || username.compareTo("")==0) 
				  ? null : mensajero);
		TipoPagina tipoPag = new TipoPagina();
		tipoPag.setCodigo(1);
		paginaExAlumno.setTipoPagina(tipoPag);

		BindingResult result = new BeanPropertyBindingResult(paginaExAlumno, "");
		validator.validate(paginaExAlumno, result);
		Map<String, String> errores = ModuloUtilidades.parseErrors(result);

		// Se envian los errores si es que existen
		if (!errores.isEmpty()){
			return errores;
		}			
		// sino, se procesa la data
		else {
			paginaExAlumno.setUsuarioAexa(moduloUsuario.loadUsuarioAexaById((Long) session.getAttribute("userID")));
			paginaExAlumno.setFechaModificacion(new Date());

			try {
				moduloUsuario.savePaginaWeb(paginaExAlumno);
			} catch (Exception e) {
				logger.warn(e);
				return Collections.singletonMap("paginaWeb.error", "true");
			}

			return Collections.singletonMap("flag", "true");
		}
	}

}
