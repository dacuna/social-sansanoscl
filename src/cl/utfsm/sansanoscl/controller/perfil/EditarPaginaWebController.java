package cl.utfsm.sansanoscl.controller.perfil;

import java.util.Collections;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cl.utfsm.aexa.modulos.ModuloUsuarioAexa;
import cl.utfsm.aexa.personas.UsuarioAexa;
import cl.utfsm.aexa.redsocial.PaginaExAlumno;
import cl.utfsm.sansanoscl.service.UsuarioAexaService;
import cl.utfsm.sansanoscl.utils.JsonDeserializer;
import cl.utfsm.sansanoscl.utils.ModuloUtilidades;
import cl.utfsm.sansanoscl.validators.PaginaExAlumnoValidator;

@Controller
@RequestMapping(value = "/perfil/editarPaginaWeb")
public class EditarPaginaWebController {
	@Autowired ModuloUsuarioAexa moduloUsuario;
	@Autowired UsuarioAexaService userService;
	@Autowired PaginaExAlumnoValidator validator;
	
	private static final Logger logger = Logger.getLogger(EditarPaginaWebController.class);
	final String VISTA_FORM = "/perfil/formularios/editarPaginaWeb";
	
	@RequestMapping(method = RequestMethod.GET)
	public String showForm(@RequestParam(value="pid",defaultValue="none") String paginaId, 
			Model model, HttpSession session){
		//validar la id de pagina
		if(!ModuloUtilidades.checkIfNumber(paginaId)){
			return "redirect:perfil";
		}
		
		//cargo la pagina con esa id
		PaginaExAlumno web = moduloUsuario.getPaginaExAlumno(Integer.parseInt(paginaId), (Long) session.getAttribute("userID"));
		model.addAttribute("paginaWeb",web);
		model.addAttribute("tiposPagina",moduloUsuario.getListaTipoPagina());
		
		return VISTA_FORM;
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody Map<String, ? extends Object> update(@RequestBody Object requested, 
			HttpSession session) {

		PaginaExAlumno paginaExAlumno = new PaginaExAlumno();
		paginaExAlumno = (PaginaExAlumno) JsonDeserializer.parsingClass(paginaExAlumno, requested);

		UsuarioAexa user = moduloUsuario.loadUsuarioAexaById((Long) session.getAttribute("userID"));
		
		BindingResult result = new BeanPropertyBindingResult(paginaExAlumno, "");
		validator.validateEditarWeb(paginaExAlumno, user.getId(), result);
		Map<String, String> errores = ModuloUtilidades.parseErrors(result);

		// Se envian los errores si es que existen
		if (!errores.isEmpty()){
			return errores;
		}	
		// sino, se procesa la data
		else {
			paginaExAlumno.setUsuarioAexa(user);
			paginaExAlumno.setFechaModificacion(new Date());

			try {
				moduloUsuario.updatePaginaWeb(paginaExAlumno);
			} catch (Exception e) {
				logger.warn(e);
				return Collections.singletonMap("paginaWeb.error", "true");
			}

			return Collections.singletonMap("flag", "true");
		}
	}
	
}
