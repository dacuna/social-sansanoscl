package cl.utfsm.sansanoscl.controller.perfil.testing;

import static org.junit.Assert.assertEquals;

import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import cl.utfsm.sansanoscl.controller.perfil.EliminarPaginaExAlumnoController;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-context.xml" })
@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional
public class TestEliminarPaginaExAlumnoController {
	@Autowired
	EliminarPaginaExAlumnoController controller;

	@Test
	public void todoOK() {
		// Se debe inicializar una sesion, el controlador necesita conocer el
		// userID
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1); // se usa el usuario con id 1 de prueba

		Map<String, ? extends Object> result = controller.delete("50010", session);
		// si es que todo funciono correctamente retorno un key flag con valor
		// true
		assertEquals(true, result.containsKey("flag"));
	}
	
	@Test
	public void paginaInexistente() {
		// Se debe inicializar una sesion, el controlador necesita conocer el
		// userID
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		Map<String, ? extends Object> result = controller.delete("99999", session);
		assertEquals(true, result.containsKey("errores.exist"));
	}
	
	@Test
	public void paginaAjena() {
		// Se debe inicializar una sesion, el controlador necesita conocer el
		// userID
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 11795);

		Map<String, ? extends Object> result = controller.delete("50010", session);
		assertEquals(true, result.containsKey("errores.exist"));
	}
}
