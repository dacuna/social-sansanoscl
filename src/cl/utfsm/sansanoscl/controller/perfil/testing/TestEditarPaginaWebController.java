package cl.utfsm.sansanoscl.controller.perfil.testing;

import static org.junit.Assert.assertEquals;

import java.util.LinkedHashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import cl.utfsm.sansanoscl.controller.perfil.EditarPaginaWebController;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-context.xml" })
@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional
public class TestEditarPaginaWebController {
	@Autowired EditarPaginaWebController controller;

	@Test
	public void submitTest() {
		// Se debe inicializar una sesion, el controlador necesita conocer el
		// userID
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1); // se usa el usuario con id 1 de prueba

		// como los objectos recibidos por JSON son LinkedHashMap, se deben
		// settear de esa manera los
		// parametros para el test:
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("paginaExAlumnoId", "50006");
		data.put("descripcion", "Esta es una pagina editada");
		data.put("url", "http://www.12312.cl");
		data.put("tipoPagina.codigo", "3");

		// ahora testeo el resultado:
		Map<String, ? extends Object> result = controller.update((Object) data, session);
		// recordar que en el controlador se esta devolviendo un map con la id
		// si es que todo funciono correctamente
		assertEquals(true, result.containsKey("flag"));
	}

	@Test
	public void faltanDatosTest() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1); // se usa el usuario con id 1 de prueba

		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		// parametros: paginaExAlumnoId, descripcion, url y tipoPagina.codigo
		data.put("paginaExAlumnoId", "7");
		data.put("descripcion", "Testeando las transacciones");
		// no se envian los parametros url y tipoPagina.codigo

		// el controlador retorna un map con la id si es que todo funciono
		// correctamente
		// si hay algun error, se retorna null despues de enviar los errores a
		// la vista.
		Map<String, ? extends Object> result = controller.update((Object) data, session);
		assertEquals(true, result.containsKey("errores.exist"));
	}
}
