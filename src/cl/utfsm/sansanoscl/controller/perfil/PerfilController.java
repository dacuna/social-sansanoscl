package cl.utfsm.sansanoscl.controller.perfil;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.log4j.lf5.util.StreamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import cl.utfsm.aexa.AntecedenteEducacional;
import cl.utfsm.aexa.AntecedenteLaboral;
import cl.utfsm.aexa.FileService;
import cl.utfsm.aexa.modulos.ModuloAexa;
import cl.utfsm.aexa.modulos.ModuloUsuarioAexa;
import cl.utfsm.aexa.personas.UsuarioAexa;
import cl.utfsm.aexa.redsocial.PaginaExAlumno;
import cl.utfsm.aexa.tipos.TipoPagina;
import cl.utfsm.sansanoscl.models.ActividadExalumno;
import cl.utfsm.sansanoscl.service.UsuarioAexaService;
import cl.utfsm.sansanoscl.utils.ModuloUtilidades;
import cl.utfsm.sansanoscl.utils.UsuarioSocial;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Element;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.Font;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Image;
import com.itextpdf.text.BaseColor;

@Controller
@RequestMapping(value = "/perfil")
public class PerfilController {
	@Resource(name="fileService") FileService fileService;
	@Autowired ModuloUsuarioAexa moduloUsuario;
	@Autowired ModuloAexa moduloAexa;
	@Autowired UsuarioAexaService usuarioService;
	
	private static final Logger logger = Logger.getLogger(PerfilController.class);
	
	//este metodo carga el perfil de cualquier usuario
	@RequestMapping(method = RequestMethod.GET)
	public String verPerfil(@RequestParam(value = "uid", defaultValue = "null") String uid, Model model, HttpSession session) {
		boolean externo = true;
		//comportamiento por defecto, si no hay "uid" entonces se muestra el perfil del usuario logeado
		if(uid.compareTo("null") == 0){
			externo = false;
			uid = ((Long)session.getAttribute("userID")).toString();
		}
		
		if(!ModuloUtilidades.checkIfNumber(uid)){
			model.addAttribute("error","true");
			return "perfil/perfil";
		}
		
		String uSessionId = ((Long)session.getAttribute("userID")).toString();
		Long userId = Long.parseLong(uid);
		Long userSessionId = Long.parseLong(uSessionId);
		UsuarioAexa user = usuarioService.getUsuarioAexaPorId(userId);
		UsuarioAexa userSession = usuarioService.getUsuarioAexaPorId(userSessionId);
		
		logger.info("OBTENIENDO PERFIL DE USUARIO: "+user.getId());
		
		//cargar laboral actual, el segundo parametro permite obtener una cantidad especifica de actividad
		List<ActividadExalumno> actividadActual=usuarioService.antecedentesLaboralesActualesUsuario(user.getId());
		model.addAttribute("antecedentesLaboralActual",actividadActual);
		//cargar laboral anterior, el segundo parametro permite obtener una cantidad especifica de actividad
		List<ActividadExalumno> actividadAnterior=usuarioService.antecedentesLaboralesPasadosUsuario(user.getId());
		model.addAttribute("antecedentesLaboralAnterior",actividadAnterior);
		//cargar el cargo actual del usuario
		model.addAttribute("cargoActual",usuarioService.antecedentesLaboralMasActualUsuario(user.getId()));
		
		model.addAttribute("ModuloUtilidades", new ModuloUtilidades());
		
		//se cargan las paginas web del usuario (obtiene las ultimas 5, para no sobrecargar la vista)
		List<PaginaExAlumno> paginas = usuarioService.getPaginasWebUsuario(user.getId(), 4);
		model.addAttribute("paginasWeb",paginas);
		
		//revisa si son amigos actualmente o no		
		List <UsuarioAexa> contactos = moduloUsuario.getContactosByIdUser(user.getId());
		
		Boolean usuarioAgregado = false;
		
		for (UsuarioAexa contacto: contactos){
			if ((contacto.getId().compareTo(userSessionId) == 0) || (userId.compareTo(userSessionId) == 0)){
				usuarioAgregado = true;
				break;
			}
		}
		
		//se carga el numero de contactos que posee el usuario
		Number totalContactos = usuarioService.getTotalContactosDeUsuario(user.getId());
		model.addAttribute("totalContactos",totalContactos);
		model.addAttribute("contactos", moduloUsuario.getContactosByIdUser(user.getId()) );
		model.addAttribute("usuarioAgregado", usuarioAgregado);
		
		//se cargan las carreras usm que curso el usuario
		List<AntecedenteEducacional> carrerasUSM = usuarioService. getCarrerasUSM(user);
		model.addAttribute("carrerasUSM",carrerasUSM);
		
		//probar las carreras nuevas
		List<String> nuevasCarreras = usuarioService.getCarreraAlumno(user);
		model.addAttribute("carrerasUSMNuevas",nuevasCarreras);
		for(String carrera:nuevasCarreras)
		{
			logger.info("CARRERA OBTENIDA: "+carrera);
		}
		
		//cargo el usuario social
		UsuarioSocial usuario = ModuloUtilidades.cargarUsuarioSocial(user);
		usuario.setLaboralActual(usuarioService.getAntecedenteLaboralMasReciente(usuario.getId()));
		
		//logger.info("EL ANTECEDENTE LABORAL ACTUAL ES: "+usuario.getLaboralActual().getSucursalEmpresa().getEmpresa());
		//logger.info("EL ANTECEDENTE LABORAL ACTUAL ES: "+usuario.getLaboralActual().getSucursalEmpresa().getEmpresa().getNombre());
		
		usuario.setTitulo(usuarioService.getCarrerasUSMUltimo(user));
		model.addAttribute("user", usuario);
		model.addAttribute("usuario", user);
				
		//actividad reciente
		model.addAttribute("contactosRecientes",usuarioService.getContactosRecientes(user.getId(), 5));
		model.addAttribute("educacionalReciente", usuarioService.getAntecedenteEducacionalReciente(user.getId(), 2));
		model.addAttribute("actualizacionPerfil", usuarioService.obtenerActividadPerfilReciente(user));
		model.addAttribute("actividadReciente", usuarioService.getActividadRecienteUsuario(userId));
	
		//contactos comunes (usuario externo)
		if(externo){
			model.addAttribute("externo", externo);
			uid = ((Long)session.getAttribute("userID")).toString();
			List<UsuarioAexa> comunes = moduloUsuario.getContactosComunes(user, userSession);
			Integer numComunes = comunes.size();
			if (numComunes > 0){
					model.addAttribute("numComunes",numComunes );
					model.addAttribute("comunes", comunes);
			}
			else model.addAttribute("numComunes","Ninguno");
			
			//codigo para saber si el usuario logeado ha enviado una solicitud que aun no ha sido aceptada al
			//usuario al que se le esta viendo el perfil:
			Integer codVigencia = usuarioService.getCodigoVigenciaSolicitud(userSessionId, userId);
			if(codVigencia != null && codVigencia.compareTo(2) == 0)
				model.addAttribute("codigoVigencia", codVigencia);
		}
		
		//tambien se carga el modulo de utilidades
		model.addAttribute("Utilidades", new ModuloUtilidades());
		
		List<PaginaExAlumno> mensajeros = usuarioService.getMensajerosDeUsuario(userId, 4);

		int i = 0;
		for(PaginaExAlumno pag:mensajeros){
			String[] datosMensajero = pag.getUrl().split(":");
			logger.info("datosMensajero");
			logger.info(datosMensajero.length);
			if(i == 0){
				model.addAttribute("nombreMensajero", datosMensajero[0]);
				//TODO:modificar para correcto funcionamiento
				//model.addAttribute("urlMensajero", datosMensajero[1]);
			}
			TipoPagina tipo = new TipoPagina();
			tipo.setNombre(datosMensajero[0]);
			pag.setTipoPagina(tipo);
			//TODO: modificar para funcionar
			//pag.setUrl(datosMensajero[1]);
			i += 1;
		}
		
		model.addAttribute("mensajeros", mensajeros);
		
		return "perfil/perfil";
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/perfilMini")
	public String perfilMini(@RequestParam(value = "uid", defaultValue = "null") String uid, 
			Model model, HttpSession session) {
		UsuarioAexa us = usuarioService.getUsuarioAexaPorId((Long)session.getAttribute("userID"));
		//comportamiento por defecto, si no hay "uid" entonces se muestra el perfil del usuario logeado
		if(uid.compareTo("null") == 0)
			uid = ((Long)session.getAttribute("userID")).toString();
		
		if(!ModuloUtilidades.checkIfNumber(uid)){
			model.addAttribute("error","true");
			return "perfil/miniPerfil";
		}
		
		Long userId = Long.parseLong(uid);

		UsuarioAexa user = usuarioService.getUsuarioAexaPorId(userId);
		
		AntecedenteLaboral laboralActual = usuarioService.getAntecedenteLaboralMasReciente(user.getId());
		
		//se carga el numero de contactos que posee el usuario
		Number totalContactos = usuarioService.getTotalContactosDeUsuario(user.getId());
		model.addAttribute("totalContactos",totalContactos);
		
		//cargo el usuario social
		UsuarioSocial usuario = new UsuarioSocial();
		usuario.setId(user.getId());
		usuario.setNombreCompleto(user.getNombreCompleto());
		usuario.setLaboralActual(laboralActual);
		usuario.setTitulo(usuarioService.getCarrerasUSMUltimo(user));
		usuario.setComuna(user.getComuna());
		usuario.setMensajeroInstantaneo(usuarioService.getMensajeroInstantaneo(user.getId()));
		model.addAttribute("usuario", usuario);
		model.addAttribute("user", user);
		//no se porque si no se hace esto la imagen no se muestra =/
		model.addAttribute("foto", (user.getFoto() != null)?user.getFoto().toLowerCase():null);
		model.addAttribute("Utilidades", new ModuloUtilidades());
		System.out.println("id1 "+ user.getId() + " id2 "+ us.getId() );
		Integer numComunes = usuarioService.getContactosComunes(user, us).size();
		if (numComunes >0)model.addAttribute("numComunes",numComunes);
		else model.addAttribute("numComunes","Ninguno");
		
		
		return "perfil/miniPerfil";
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/enviarMensaje")
	public String enviarMensaje(@RequestParam("uid") String uid, Model model, HttpSession session) {
		//se debe pasar a la vista el usuario al que se le enviara el mensaje validacion del id de usuario
		Long userId;
		try{
			userId = Long.parseLong(uid);
		}catch (NumberFormatException e) {
			logger.warn(e);
			model.addAttribute("error","Usuario inexistente");
			return "error";
		}
		UsuarioAexa user = usuarioService.getUsuarioAexaPorId(userId);
		model.addAttribute("userId", user.getId());
		model.addAttribute("nombreUsuario",ModuloUtilidades.getNombreFormateado(user.getNombreCompleto()));
		return "/buzon/enviarMensaje";
	}
	
	
	
	
	@RequestMapping(method = RequestMethod.GET, value="/crearPDF")
	public String crearPDF(@RequestParam(value="uid") String uid, Model model, 
			HttpSession session, HttpServletRequest request, HttpServletResponse response) throws BadElementException, MalformedURLException, IOException {
		
		//se debe pasar a la vista el usuario al que se le enviara el mensaje validacion del id de usuario
		Long userId;
		
		try{
			userId = Long.parseLong(uid);
		}catch (NumberFormatException e) {
			logger.warn(e);
			model.addAttribute("error","Usuario inexistente");
			return "error";
		}
		
		UsuarioAexa user = usuarioService.getUsuarioAexaPorId(userId);
		
		//INFORMACION PERSONAL
		
		//Nombre
		String nombreCompleto = user.getNombreCompleto();
		Font fontNombre = new Font(Font.getFamily("ARIAL"),14,Font.BOLD);
		fontNombre.setColor(16, 97, 161);
		Phrase nombre = new Phrase("			" + nombreCompleto,fontNombre);
		
		//Estudio mas reciente, si no existe es Sin informacion y no debe ir en el PDF
		String educacionalMasReciente = usuarioService.getCarrerasUSMUltimo(user);
		if(educacionalMasReciente == null || educacionalMasReciente.equals("")){
			educacionalMasReciente = "Sin informacion";
		}
		Font fontEducacionalMasReciente = new Font(Font.getFamily("ARIAL"),12,Font.BOLD);
		fontEducacionalMasReciente.setColor(102,102,102);
		Phrase ultimaEducacion = new Phrase("			 " + educacionalMasReciente,fontEducacionalMasReciente);
		
		//Trabajo mas reciente (usa la misma font que Estudio mas reciente)
		AntecedenteLaboral laboralMasReciente = usuarioService.getAntecedenteLaboralMasReciente(user.getId());
		String cargoMasReciente = "";
		String empresaMasReciente = "";
		if(laboralMasReciente == null){
			cargoMasReciente = "Sin informacion";
			empresaMasReciente = "Sin Informacion";
		}
		else{
			cargoMasReciente = laboralMasReciente.getCargo();
			empresaMasReciente = laboralMasReciente.getSucursalEmpresa().getEmpresa().getNombre();
			if(cargoMasReciente == null || cargoMasReciente.equals("")){
				cargoMasReciente = "Sin informacion";
			}
			if(empresaMasReciente == null || empresaMasReciente.equals("")){
				empresaMasReciente = "Sin Informacion";
			}
		}
		Phrase ultimoTrabajo = new Phrase("			 " + cargoMasReciente + " en " + empresaMasReciente,fontEducacionalMasReciente);
		
		//Ciudad actual
		String ciudad = user.getComuna().getNombre();
		if(ciudad == null|| ciudad.equals("")){
			ciudad = "Sin Informacion";
		}
		String pais = user.getComuna().getPais().getNombre();
		if(pais == null || pais.equals("")){
			pais = "Sin Informacion";
		}
		Font fontCiudad = new Font(Font.getFamily("ARIAL"),10,Font.BOLD);
		fontCiudad.setColor(16, 97, 161);
		Phrase ciudadActual = new Phrase("			  " + ciudad + ", " + pais,fontCiudad);
		if(ciudad.equals("Sin Informacion") && !pais.equals("Sin Informacion")){
			ciudadActual = new Phrase("			  " + pais,fontCiudad);
		}
		if(!ciudad.equals("Sin Informacion") && pais.equals("Sin Informacion")){
			ciudadActual = new Phrase("			  " + ciudad,fontCiudad);
		}
				
		
		//INFORMACION GENERAL
		
		
		Font fontInfoGeneral = new Font(Font.getFamily("ARIAL"),13,Font.BOLDITALIC);
		fontInfoGeneral.setColor(16,97,161);
		Font fontDato = new Font(Font.getFamily("ARIAL"),10,Font.BOLD);
		fontDato.setColor(102,102,112);
		Font fontDatoItalic = new Font(Font.getFamily("ARIAL"),10,Font.BOLDITALIC);
		fontDatoItalic.setColor(102,102,112);
		Font fontValor = new Font(Font.getFamily("ARIAL"),10,Font.NORMAL);
		fontValor.setColor(16,97,161);
		Font parentesis = new Font(Font.getFamily("ARIAL"),10,Font.NORMAL);
		parentesis.setColor(153,153,167);
		Phrase infoGeneral = new Phrase("Informaci�n General____________________________________________________",fontInfoGeneral);
		
		//Fecha de nacimiento
		String fechaNac = user.getFechaNacimientoToString();
		if(fechaNac == null || fechaNac.equals("")){
			fechaNac = "Sin Informacion";
		}
		Phrase datoNacimiento = new Phrase("Fecha de Nacimiento: ",fontDato);
		Phrase valorNacimiento = new Phrase("				" + fechaNac,fontValor);
		
		//Correo electronico
		String mail = user.getEmail();
		if(mail == null|| mail.equals("")){
			mail = "Sin Informacion";
		}
		//Correo elecronico laboral
		String mailLaboral = user.getEmailLaboral();
		if(mailLaboral == null || mailLaboral.equals("")){
			mailLaboral = "Sin Informacion";
		}
		Phrase datoCorreo = new Phrase("Correo electr�nico: ",fontDato);
		Phrase valorCorreo = new Phrase("								" + mail,fontValor);
		Phrase valorCorreo2 = new Phrase(" (personal)",parentesis);
		Phrase valorCorreoLaboral = new Phrase(" - " + mailLaboral,fontValor);
		Phrase valorCorreoLaboral2 = new Phrase(" (laboral)",parentesis);
				
		//Telefono fijo y movil
		Integer telefono = user.getTelefono();
		if(telefono == null || telefono.equals("")){
			telefono = 0;
		}
		String celular = user.getTelefonoMovil();
		Integer auxCel = -1;
		if(celular.equals(" ")){
			auxCel = 0;
		}
		
		//VER
		if(celular == null || celular.equals("")  || auxCel == 0){
			celular = "Sin Informacion";
		}
		Phrase datoTelefono = new Phrase("Tel�fono: ",fontDato);
		Phrase valorTelefono = new Phrase("																									" + telefono.toString(),fontValor);
		Phrase valorTelefono2 = new Phrase(" (fijo)",parentesis);
		Phrase valorCelular = new Phrase(" - " + celular,fontValor);
		Phrase valorCelular2 = new Phrase(" (m�vil)",parentesis);
		
		//Mensajers instantaneos
		List<PaginaExAlumno> mensajerosInstantaneos = usuarioService.getMensajerosDeUsuario(userId, 4);
		String flagMensajero = "mensajero";
		if(mensajerosInstantaneos.size() == 0){
			flagMensajero = "Sin Informacion";
		}
		Phrase datoMensajeros = new Phrase("Mensj. Instant�neos: ",fontDato);
		
		
		//ANTECEDENTES LABORALES
		
		
		Phrase antecedentesLaborales = new Phrase("Antecedentes Laborales_________________________________________________",fontInfoGeneral);
		List<AntecedenteLaboral> listaLaboralActual = usuarioService.getAntecedenteLaboralActual(user.getId(), 3);
		List<AntecedenteLaboral> listaLaboralAnterior = usuarioService.getAntecedenteLaboralAnterior(user.getId(), 3);
		Font fontLaboral = new Font(Font.getFamily("ARIAL"),10,Font.ITALIC,BaseColor.GRAY);
		Font fontDatoLaboral = new Font(Font.getFamily("ARIAL"),10,Font.BOLD);
		fontDatoLaboral.setColor(87,144,189);
		Phrase laboralActual = new Phrase("Actual",fontDatoLaboral);
		Phrase laboralAnterior = new Phrase("Anterior",fontDatoLaboral);
		
		
		//ANTECEDENTES EDUCACIONALES
		
		
		Phrase antecedentesEducacionales = new Phrase("Antecedentes Educacionales ____________________________________________",fontInfoGeneral);
		List<AntecedenteEducacional> listaCarrerasUSM = usuarioService.getCarrerasUSM(user);
		List<AntecedenteEducacional> listaCarrerasReciente = usuarioService.getAntecedenteEducacionalReciente(userId, 2);
		Phrase educacion = new Phrase("Educaci�n",fontDatoLaboral);
		Phrase USM = new Phrase("Universidad T�cnica Federico Santa Mar�a",fontDato);
		
		//AQUI SE CREARAN POSIBLES HEADERS, FOOTERS E IMAGENES A USAR (LOGO AEXA)
		String pie1 = "____________________________";
		String pie2 = "Red de Ex Alumnos, Universidad T�cnica Federico Santa Mar�a";
		String pie3 = "Av. Espa�a 1680 - 6to. Piso, Valpara�so - CHILE";
		String pie4 = "http://social.sansanos.cl";
		Font fontFooter = new Font(Font.getFamily("ARIAL"),6,Font.NORMAL,BaseColor.BLACK);
		Chunk footer1 = new Chunk(pie1);
		Chunk footer2 = new Chunk(pie2,fontFooter);
		Chunk footer3 = new Chunk(pie3,fontFooter);
		Chunk footer4 = new Chunk(pie4,fontFooter);
		
		//OBTENCION DE LA FOTO
		Map<String,byte[]> respuestaPhoto = fileService.getPhoto(userId.toString(), "##D$$F//X1J44LA9%%ASJ()");
		byte[] bytes = null;
		String nombrePhoto = null;
		
		if(respuestaPhoto != null){
			Set<String> nombreArchivo = respuestaPhoto.keySet();
			if(nombreArchivo.size() > 0){
				nombrePhoto = nombreArchivo.iterator().next();
				bytes = respuestaPhoto.get(nombrePhoto);
			}
		}
		logger.info(nombrePhoto);
		logger.info(bytes.length);
		
		Image foto = Image.getInstance(bytes);
		Image fotoRedSocial = Image.getInstance("web/resources/img/red_social.png");
		Image fotoAexa = Image.getInstance("web/resources/img/aexa.jpg");
		fotoRedSocial.setAbsolutePosition(535, 960);
		fotoRedSocial.scaleAbsolute(60, 30);
		fotoAexa.setAbsolutePosition(15, 950);
		fotoAexa.scaleAbsolute(70, 40);
		//foto.setAbsolutePosition(50, 825);
		foto.scaleAbsoluteHeight(70);
		foto.scaleAbsoluteWidth(70);
		foto.setAlignment(Image.LEFT | Image.TEXTWRAP);
		
		//GENERACION DEL DOCUMENTO
		
		
		try{
			Document document = new Document(PageSize.LEGAL,40,40,40,40);//int marginLeft,   int marginRight,   int marginTop,   int marginBottom  
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			PdfWriter.getInstance(document,baos);
			
			//ABRIR DOCUMENTO Y SETEAR AUTOR Y TITULO
			document.open();
			document.addAuthor("Red de Ex Alumnos USM");
			document.addTitle("Ficha Usuario Sansanos.cl");
			
			logger.info("Generando PDF...");
			
			//SE CREA EL FOOTER
			footer1.setTextRise(-900.0f);
			document.add(footer1);
			document.add(Chunk.NEWLINE);
			footer2.setTextRise(-910.0f);
			document.add(footer2);
			document.add(Chunk.NEWLINE);
			footer3.setTextRise(-920.0f);
			document.add(footer3);
			document.add(Chunk.NEWLINE);
			footer4.setTextRise(-930.0f);
			document.add(footer4);
			
			//SE GENERA EL TITULO Y SU RESPECTIVA FONT
			Font fontTitulo = new Font(Font.getFamily("ARIAL"),16,Font.BOLD);
			fontTitulo.setColor(102,102,102);
			Paragraph titulo = new Paragraph("Ficha Usuario",fontTitulo);
			titulo.setAlignment(Element.ALIGN_CENTER);
			
			//AGREGAR FOTO DEL HEADER (LOGO RED DE EX ALUMNOS Y UNO MOMENTANEO DE LA RED QUE HICE CON PAINT)
			//AGREGAR EL TITULO AL PDF
			document.add(fotoAexa);
			document.add(fotoRedSocial);
			document.add(new Paragraph(" "));
			document.add(titulo);
			document.add(new Paragraph(" "));
			
			//AGREGAR INFORMACION PRINCIPAL DEL USUARIO, NOMBRE, ULTIMO TRABAJO Y EDUCACION, CIUDAD Y FOTO
			document.add(foto);
			document.add(Chunk.NEWLINE);
			document.add(new Paragraph(nombre));
			document.add(new Paragraph(ultimaEducacion));
			if(cargoMasReciente.equals("Sin Informacion") && empresaMasReciente.equals("Sin Informacion")){
				document.add(new Paragraph(ultimoTrabajo));
			}	
			document.add(new Paragraph(ciudadActual));
			
			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEWLINE);
			
			//INFORMACION GENERAL, FECHA NAC., CORREO (PERSONAL Y/O LABORAL), TELEFONO Y/O CELULAR,
			//MENSAJEROS INSTANTANEOS (ULTIMOS 4 AGREGADOS)
			document.add(new Paragraph(infoGeneral));
			document.add(Chunk.NEWLINE);
			if(!fechaNac.equals("Sin Informacion")){
				document.add(new Phrase(datoNacimiento));
				document.add(new Phrase(valorNacimiento));
			}
			if(!mail.equals("Sin Informacion")){
				document.add(Chunk.NEWLINE);
				document.add(new Phrase(datoCorreo));
				document.add(new Phrase(valorCorreo));
				document.add(new Phrase(valorCorreo2));
				if(!mailLaboral.equals("Sin Informacion")){
					document.add(new Phrase(valorCorreoLaboral));
					document.add(new Phrase(valorCorreoLaboral2));
				}
			}
			if(!telefono.equals(0)){
				document.add(Chunk.NEWLINE);
				document.add(new Phrase(datoTelefono));
				document.add(new Phrase(valorTelefono));
				document.add(new Phrase(valorTelefono2));
				if(!celular.equals("Sin Informacion")){
					document.add(new Phrase(valorCelular));
					document.add(new Phrase(valorCelular2));
				}
			}
			if(!flagMensajero.equals("Sin Informacion")){
				document.add(Chunk.NEWLINE);
				document.add(new Phrase(datoMensajeros));
				document.add(new Phrase("					"));
				int auxMensajero = 0;
				for(PaginaExAlumno pag:mensajerosInstantaneos){
					auxMensajero++;
					String[] datosMensajero = pag.getUrl().split(":");
					Phrase nombreMensajero = new Phrase(datosMensajero[0] + ": ",fontDatoItalic);
					Phrase urlMensajero = new Phrase(datosMensajero[1],fontValor);
					if(auxMensajero != 1){
						document.add(new Phrase("																																			"));
					}	
					document.add(new Phrase(nombreMensajero));
					document.add(new Phrase(urlMensajero));
					document.add(Chunk.NEWLINE);
				}
			}	
			
			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEWLINE);
			
			//ANTECEDENTES LABORALES
			//ACTUALES: SE IMPRIMEN LOS ULTIMOS 3
			//ANTERIORES: SE IMPRIMEN LOS ULTIMOS 3
			document.add(new Paragraph(antecedentesLaborales));
			document.add(Chunk.NEWLINE);
			document.add(new Phrase(laboralActual));
			document.add(new Phrase("               "));
			if(listaLaboralActual.size() == 0){
				document.add(new Phrase(" Sin informaci�n",fontLaboral));
				document.add(Chunk.NEWLINE);
			}
			else{
				for(AntecedenteLaboral labAct:listaLaboralActual){
					String trabajoActual = " " + labAct.getCargo() + " en " + labAct.getSucursalEmpresa().getEmpresa().getNombre() + " (" + labAct.getFechaIngreso() + " - " + labAct.getFechaEgreso() + ")";
					Phrase trabajo = new Phrase(trabajoActual,fontLaboral);
					document.add(new Phrase(trabajo));
					document.add(Chunk.NEWLINE);
				}
			}
			document.add(Chunk.NEWLINE);
			document.add(new Phrase(laboralAnterior));
			document.add(new Phrase("            "));
			if(listaLaboralAnterior.size() == 0){
				document.add(new Phrase(" Sin informaci�n",fontLaboral));
				document.add(Chunk.NEWLINE);
			}
			else{
				for(AntecedenteLaboral labAnt:listaLaboralAnterior){
					SimpleDateFormat fecha = new SimpleDateFormat("dd/MM/yyyy");
					String trabajoAnterior = " " + labAnt.getCargo() + " en " + labAnt.getSucursalEmpresa().getEmpresa().getNombre() + " (" + fecha.format(labAnt.getFechaIngreso())+ " - " + fecha.format(labAnt.getFechaEgreso()) + ")";
					Phrase trabajo = new Phrase(trabajoAnterior,fontLaboral);
					document.add(new Phrase(trabajo));
					document.add(Chunk.NEWLINE);
				}
			}
			
			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEWLINE);
			
			//ANTECEDENTES EDUCACIONALES
			//USM: SE IMPRIMEN TODOS
			//OTROS: LOS �LTIMOS 2
			document.add(new Paragraph(antecedentesEducacionales));
			document.add(Chunk.NEWLINE);
			document.add(new Phrase(educacion));
			document.add(new Phrase("            "));
			if(listaCarrerasUSM.size() == 0){
				document.add(new Phrase("Sin informaci�n",fontLaboral));
				document.add(Chunk.NEWLINE);
			}
			else{
				document.add(new Phrase(USM));
				document.add(Chunk.NEWLINE);
				for(AntecedenteEducacional antEd:listaCarrerasUSM){
					String carreraUSM = "																																					- " + antEd.getSedeCarrera().getNombre();
					Phrase carrUSM = new Phrase(carreraUSM,fontLaboral);
					document.add(new Phrase(carrUSM));
					document.add(Chunk.NEWLINE);
				}
				document.add(Chunk.NEWLINE);
				if(listaCarrerasReciente.size() != 0){
					for(AntecedenteEducacional antEdRec:listaCarrerasReciente){
						if(antEdRec.getUniversidad() == null){
							Phrase otraU = new Phrase("");
							document.add(new Phrase(otraU));
							document.add(Chunk.NEWLINE);
						}
						else{
							Phrase otraU = new Phrase("																															" + antEdRec.getUniversidad().getNombre(),fontDato);
							document.add(new Phrase(otraU));
							document.add(Chunk.NEWLINE);
							Phrase otraCarr = new Phrase("																																					- " + antEdRec.getSedeCarrera().getNombre(),fontLaboral);
							document.add(new Phrase(otraCarr));
							document.add(Chunk.NEWLINE);
						}
					}
				}	
			}
			document.close();
			
			// setting some response headers
			OutputStream os = response.getOutputStream();
			response.setContentType("application/pdf");
            response.setHeader("Content-Disposition","attachment;filename=fichaUsuario.pdf");
            
            // the contentlength
            response.setContentLength(baos.size());
            // write ByteArrayOutputStream to the ServletOutputStream
            
            baos.writeTo(os);
            os.write(baos.toByteArray());
            os.flush();
            os.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/foto")
	public String showPhoto(@RequestParam(value = "uid", defaultValue = "null") String uid, Model model, 
			HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		OutputStream outStream;
		try {
			outStream = response.getOutputStream();
			response.reset();
			response.setHeader("Content-Type:", "image/jpeg");
			Map<String, byte[]> mapa = fileService.getPhoto(uid, "##D$$F//X1J44LA9%%ASJ()");
			if(mapa != null){
				Set<String> nombreArchivo = mapa.keySet();
				if(nombreArchivo.size() > 0){
					String nombre = nombreArchivo.iterator().next();
					response.setHeader("Content-Disposition", "attachment; filename="+nombre);
					StreamUtils.copy(new ByteArrayInputStream(mapa.get(nombre)), outStream);
					response.flushBuffer();
				}
			}
		} catch (IOException e) {
			model.addAttribute("mensaje","Ha ocurrido un problema al enviar el archivo");
			e.printStackTrace();
		}
		
		return null;
	}

}
