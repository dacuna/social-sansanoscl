package cl.utfsm.sansanoscl.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cl.utfsm.aexa.data.UsuarioAexaDao;
import cl.utfsm.aexa.personas.UsuarioAexa;

//CONTROLADOR CREADO PARA MOTIVOS DE TESTING, CONTIENE FUNCIONES QUE CARGAN INFORMACION QUE ES 
//UTIL PARA FORMULARIOS DE PRUEBA
@Controller
@RequestMapping(value = "/util")
public class UtilController {
	@Autowired
	UsuarioAexaDao userService;
	
	final String ELIMINAR_WEB_FORM = "formularios/eliminarPaginaWeb";
	
	@RequestMapping(method = RequestMethod.GET, value="/eliminarWeb")
	public String eliminarPaginaWeb(Model model, HttpSession session){
		UsuarioAexa user = userService.loadUsuarioAexaById((Long) session.getAttribute("userID"));
		
		model.addAttribute("paginasWeb", user.getPaginas());
		
		return ELIMINAR_WEB_FORM;
	}

}
