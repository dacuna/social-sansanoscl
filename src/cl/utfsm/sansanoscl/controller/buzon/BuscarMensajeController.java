package cl.utfsm.sansanoscl.controller.buzon;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import cl.utfsm.aexa.personas.UsuarioAexa;
import cl.utfsm.aexa.redsocial.MensajeExAlumnos;
import cl.utfsm.sansanoscl.service.UsuarioAexaService;
import cl.utfsm.sansanoscl.utils.ModuloUtilidades;

@Controller
@RequestMapping(value = "/buzon/buscarMensaje")
public class BuscarMensajeController {
	@Autowired UsuarioAexaService userService;

	private final String VISTA_BUSQUEDA = "/buzon/buzonBusqueda";
	private static final Logger logger = Logger.getLogger(BuscarMensajeController.class);

	@RequestMapping(method = RequestMethod.GET)
	public String simple(@RequestParam(value = "busqueda", defaultValue = "") String mensaje, 
						 @RequestParam(value = "p", defaultValue = "1") String page,
						 HttpSession session, Model model) {

		UsuarioAexa user = new UsuarioAexa();
		user.setId((Long)session.getAttribute("userID"));
		
		if(mensaje == null || mensaje.equals("")){
			//si no ingreso nada en el formulario de busqueda se lleva al buzon
			return "redirect:buzon";
		}
		
		Integer cantidad = 15;
		Integer pagina = !ModuloUtilidades.checkIfNumber(page) ? 1 : Integer.parseInt(page);
		Number totalMensajes = userService.getTotalMensajesEncontrados(mensaje, user.getId());
		Integer offset = (cantidad * (pagina-1) > totalMensajes.intValue()) ? 0 : cantidad * (pagina-1);
		List<MensajeExAlumnos> encontrados = userService.buscarMensajes(mensaje, user.getId(), cantidad, offset);
		
		for(MensajeExAlumnos m:encontrados){
			logger.info("Mensaje: "+ m.getAsunto() + " autor "+ m.getUsuarioAexa().getNombreIncompleto() + " receptor "+ m.getUsuarioAexaReceptor().getNombreIncompleto());
		}

		Double cantidadPaginas = Math.ceil(totalMensajes.doubleValue()/cantidad.doubleValue());
		model.addAttribute("cantidadPaginas", cantidadPaginas.intValue());
		model.addAttribute("paginaActual", pagina);
		model.addAttribute("totalMensajes", totalMensajes);
		model.addAttribute("inicioPagina", (pagina-1)*cantidad + 1);
		model.addAttribute("finalPagina", (pagina*cantidad > totalMensajes.intValue()) ? totalMensajes : pagina*cantidad);
		model.addAttribute("mensajes",encontrados);
		model.addAttribute("termino", mensaje);
		model.addAttribute("ModuloUtilidades", new ModuloUtilidades());
		return VISTA_BUSQUEDA;
	}
}
