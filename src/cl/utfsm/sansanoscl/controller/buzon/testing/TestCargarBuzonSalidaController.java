package cl.utfsm.sansanoscl.controller.buzon.testing;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ExtendedModelMap;

import cl.utfsm.sansanoscl.controller.buzon.CargarBuzonSalidaController;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-context.xml" })
@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional
public class TestCargarBuzonSalidaController {
	@Autowired
	CargarBuzonSalidaController controller;

	@Test
	public void showFormTest() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		Assert.assertEquals("/buzon/buzonSalida", controller.showForm(
				"1", new ExtendedModelMap(), session));
	}

}
