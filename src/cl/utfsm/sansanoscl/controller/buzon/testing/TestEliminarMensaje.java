package cl.utfsm.sansanoscl.controller.buzon.testing;

import static org.junit.Assert.assertEquals;

import java.util.LinkedHashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import cl.utfsm.sansanoscl.controller.buzon.EliminarMensajeController;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-context.xml" })
@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional
public class TestEliminarMensaje {
	@Autowired EliminarMensajeController controller;

	@Test
	public void todoOK() {
		// Se debe inicializar una sesion, el controlador necesita conocer el
		// userID
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("usuarioAexaReceptor.id", "1");
		data.put("usuarioAexa.id", "22429");
		data.put("fechaEnvioMensaje", "2011-07-06 16:37:46.89");

		Map<String, ? extends Object> result = controller.delete((Object) data, session);
		// recordar que en el controlador devuelve un flag si todo anduvo bien
		assertEquals(true, result.containsKey("flag"));
	}
	
	@Test
	public void noEmisor() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("usuarioAexaReceptor.id", "1");
		
		data.put("fechaEnvioMensaje", "2010-12-09 15:53:47");

		Map<String, ? extends Object> result = controller.delete((Object) data, session);
		
		assertEquals(true, result.containsKey("error"));
	}
	
	@Test
	public void noReceptor() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("usuarioAexa.id", "22429");
		data.put("fechaEnvioMensaje", "2010-12-09 15:53:47");

		Map<String, ? extends Object> result = controller.delete((Object) data, session);
		
		assertEquals(true, result.containsKey("error"));
	}
	
	@Test
	public void emisorFalso() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("usuarioAexaReceptor.id", "1");
		data.put("usuarioAexa.id", "2");
		data.put("fechaEnvioMensaje", "2010-12-09 15:53:47");

		Map<String, ? extends Object> result = controller.delete((Object) data, session);
		
		assertEquals(true, result.containsKey("error"));
	}
	
	@Test
	public void ReceptorFalso() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("usuarioAexaReceptor.id", "2");
		data.put("usuarioAexa.id", "22429");
		data.put("fechaEnvioMensaje", "2010-12-09 15:53:47");

		Map<String, ? extends Object> result = controller.delete((Object) data, session);
		
		assertEquals(true, result.containsKey("error"));
	}
	
	
	@Test
	public void mensajeAjeno() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("usuarioAexaReceptor.id", "11795");
		data.put("usuarioAexa.id", "1");
		data.put("fechaEnvioMensaje", "2011-09-05 14:38:17");

		Map<String, ? extends Object> result = controller.delete((Object) data, session);
		
		assertEquals(true, result.containsKey("error"));
	}
	
	@Test
	public void fechaFalsa() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("usuarioAexaReceptor.id", "1");
		data.put("usuarioAexa.id", "22429");
		data.put("fechaEnvioMensaje", "2011-01-01 15:48:27.343");

		Map<String, ? extends Object> result = controller.delete((Object) data, session);
		
		assertEquals(true, result.containsKey("fecha.invalid"));
	}
}


