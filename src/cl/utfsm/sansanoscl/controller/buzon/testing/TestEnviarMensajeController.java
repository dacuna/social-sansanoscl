package cl.utfsm.sansanoscl.controller.buzon.testing;

import static org.junit.Assert.assertEquals;

import java.util.LinkedHashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import cl.utfsm.sansanoscl.controller.buzon.EnviarMensajeController;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-context.xml" })
@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional
public class TestEnviarMensajeController {
	@Autowired
	EnviarMensajeController controller;

	@Test
	public void todoOK() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 2989);

		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("receptores", "2989");
		data.put("asunto", "Envio de prueba");
		data.put("mensaje", "Este es un mensaje para testing");

		Map<String, ? extends Object> result = controller.create((Object) data, session);
		// recordar que en el controlador devuelve un flag si todo anduvo bien
		assertEquals(true, result.containsKey("flag"));
	}
	
	@Test
	public void noAsunto() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 22438);

		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("receptores", "1");
		
		data.put("mensaje", "Este es un mensaje para testing");

		Map<String, ? extends Object> result = controller.create((Object) data, session);
		// recordar que en el controlador devuelve un flag si todo anduvo bien
		assertEquals(true, result.containsKey("errores.exist"));
	}
	
	@Test
	public void noMensaje() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 22438);

		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("receptores", "1");
		data.put("asunto", "Envio de prueba");
		

		Map<String, ? extends Object> result = controller.create((Object) data, session);
		// recordar que en el controlador devuelve un flag si todo anduvo bien
		assertEquals(true, result.containsKey("errores.exist"));
	}
	
	@Test
	public void noReceptor() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 22438);

		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		
		data.put("asunto", "Envio de prueba");
		data.put("mensaje", "Este es un mensaje para testing");

		Map<String, ? extends Object> result = controller.create((Object) data, session);
		// recordar que en el controlador devuelve un flag si todo anduvo bien
		assertEquals(true, result.containsKey("error"));
	}	
	
	@Test
	public void asuntoVacio() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 22438);

		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("receptores", "1");
		data.put("asunto", "");
		data.put("mensaje", "Este es un mensaje para testing");

		Map<String, ? extends Object> result = controller.create((Object) data, session);
		// recordar que en el controlador devuelve un flag si todo anduvo bien
		assertEquals(true, result.containsKey("asunto.required"));
	}
	
	@Test
	public void mensajeVacio() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 22438);

		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("receptores", "1");
		data.put("asunto", "Envio de prueba");
		data.put("mensaje", "");

		Map<String, ? extends Object> result = controller.create((Object) data, session);
		// recordar que en el controlador devuelve un flag si todo anduvo bien
		assertEquals(true, result.containsKey("mensaje.required"));
	}
	
	@Test
	public void receptorVacio() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 22438);

		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("receptores", "");
		data.put("asunto", "Envio de prueba");
		data.put("mensaje", "Este es un mensaje para testing");

		Map<String, ? extends Object> result = controller.create((Object) data, session);
		// recordar que en el controlador devuelve un flag si todo anduvo bien
		assertEquals(true, result.containsKey("error"));
	}
	
	
	@Test
	public void receptorNAN() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 22438);

		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("receptores", "A");
		data.put("asunto", "Envio de prueba");
		data.put("mensaje", "Este es un mensaje para testing");

		Map<String, ? extends Object> result = controller.create((Object) data, session);
		// recordar que en el controlador devuelve un flag si todo anduvo bien
		assertEquals(true, result.containsKey("error"));
	}
}
