package cl.utfsm.sansanoscl.controller.buzon.testing;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ExtendedModelMap;

import cl.utfsm.sansanoscl.controller.buzon.BuscarMensajeController;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-context.xml" })
@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional
public class TestBuscarMensajeController {
	@Autowired BuscarMensajeController controller;
	
	@Test
	public void buscar(){
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 18102);
		
		String result = controller.simple("mensaje", "1", session, new ExtendedModelMap());
		
		Assert.assertEquals("/buzon/buzonBusqueda", result);
	}
	
	@Test
	public void mensajeVacio(){
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 18102);
		
		String result = controller.simple("", "1", session, new ExtendedModelMap());
		
		Assert.assertEquals("redirect:buzon", result);
	}
	
	@Test
	public void noMensaje(){
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 18102);
		
		String result = controller.simple(null, "1", session, new ExtendedModelMap());
		
		Assert.assertEquals("redirect:buzon", result);
	}
}
