package cl.utfsm.sansanoscl.controller.buzon;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang.WordUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cl.utfsm.aexa.modulos.ModuloUsuarioAexa;
import cl.utfsm.aexa.redsocial.MensajeExAlumnos;
import cl.utfsm.sansanoscl.utils.JsonDeserializer;
import cl.utfsm.sansanoscl.utils.ModuloUtilidades;
import cl.utfsm.sansanoscl.validators.MensajeExAlumnoValidator;

@Controller
@RequestMapping(value = "/buzon/cargarMensaje")
public class CargarMensajeController {
	@Autowired ModuloUsuarioAexa moduloUsuario;
	@Autowired MensajeExAlumnoValidator validator;
	
	private static final Logger logger = Logger.getLogger(CargarMensajeController.class);

	@SuppressWarnings("unchecked")
	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody Map<String, ? extends Object> load(@RequestBody Object requested, 
			HttpSession session) {

		MensajeExAlumnos mensaje = new MensajeExAlumnos();
		mensaje = (MensajeExAlumnos) JsonDeserializer.parsingClass(mensaje,requested);

		logger.info("SENDER: " + mensaje.getUsuarioAexa().getId());
		logger.info("RECEIVER " + mensaje.getUsuarioAexaReceptor().getId());

		Long idLogged = (Long) session.getAttribute("userID");
		// validacion
		BindingResult result = new BeanPropertyBindingResult(mensaje, "");
		validator.validate(mensaje, result, idLogged);
		Map<String, String> errores = ModuloUtilidades.parseErrors(result);

		// TODO: se podria modificar el jsonDeserealizer para que parseara
		// fechas, mientras tanto se hace manualmente
		LinkedHashMap<String, String> data = (LinkedHashMap<String, String>) requested;
		String fechaToParse = data.get("fechaEnvioMensaje");
		logger.info(fechaToParse);

		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SS");
		Date fechaEnvio = new Date();
		try {
			fechaEnvio = (Date) formatter.parse(fechaToParse);
		} catch (ParseException e1) {
			logger.error("Imposible convertir String " + fechaToParse + " a util.Date");
			errores.put("fecha.invalid", "Se debe especificar una fecha correcta");
			e1.printStackTrace();
		}

		if (!errores.isEmpty()){
			return errores;
		} else {
			// setteo la fecha del mensaje, en este punto ya se que la fecha es
			// correcta
			mensaje.setFechaEnvio(fechaEnvio);
			logger.info("fecha envio: " + mensaje.getFechaEnvio());

			MensajeExAlumnos mensajeLeer = moduloUsuario.getMensajeExAlumno(mensaje);
			if (mensajeLeer == null)
				return Collections.singletonMap("mensaje.false", "El mensaje solicitado no existe");
			else {
				// se actualiza el campo leido del mensaje
				try {
					mensajeLeer.setLeido(true);
					moduloUsuario.updateMensajeExAlumnos(mensajeLeer);
				} catch (Exception e) {
					e.printStackTrace();
					logger.error("El mensaje a leer no ha podido ser marcado como leido");
				}

				// se cargan los datos del mensaje
				LinkedHashMap<String, String> devolver = new LinkedHashMap<String, String>();
				devolver.put("asunto", mensajeLeer.getAsunto());
				devolver.put("fechaEnvio", ModuloUtilidades.fechaEventos(mensajeLeer.getFechaEnvio()));
				devolver.put("usuarioReceptor", WordUtils.capitalize(WordUtils.swapCase(mensajeLeer.getUsuarioAexaReceptor().getNombreCompleto())));
				devolver.put("usuarioReceptorId", mensajeLeer.getUsuarioAexaReceptor().getId().toString());
				devolver.put("usuario", WordUtils.capitalize(WordUtils.swapCase(mensajeLeer.getUsuarioAexa().getNombreCompleto())));
				devolver.put("usuarioId", mensajeLeer.getUsuarioAexa().getId().toString());
				devolver.put("mensaje", mensajeLeer.getMensaje());

				// retorno el mensaje
				return devolver;
			}
		}
	}
}
