package cl.utfsm.sansanoscl.controller.buzon;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang.WordUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import cl.utfsm.aexa.modulos.ModuloUsuarioAexa;
import cl.utfsm.aexa.personas.UsuarioAexa;
import cl.utfsm.aexa.redsocial.MensajeExAlumnos;
import cl.utfsm.sansanoscl.service.UsuarioAexaService;
import cl.utfsm.sansanoscl.utils.ModuloUtilidades;

@Controller
@RequestMapping(value = "/buzon/buzonSalida")
public class CargarBuzonSalidaController {
	@Autowired ModuloUsuarioAexa moduloUsuario;
	@Autowired UsuarioAexaService userService;
	
	private static final Logger logger = Logger.getLogger(CargarBuzonSalidaController.class);
	final String VISTA_FORM = "/buzon/buzonSalida";

	@RequestMapping(method = RequestMethod.GET)
	public String showForm(@RequestParam(value = "p", defaultValue = "1") String page,
			Model model, HttpSession session) {
		UsuarioAexa user = moduloUsuario.loadUsuarioAexaById((Long) session.getAttribute("userID"));
		// se deben obtener los mensajes de este usuario
		//por defecto se muestran 15 mensajes, quizas mas adelante se le da la opcion al usuario de elegir
		Integer cantidad = 15;
		Integer pagina = !ModuloUtilidades.checkIfNumber(page) ? 1 : Integer.parseInt(page);
		Number totalMensajes = userService.getTotalMensajesEnviadosExAlumno(user.getId());
		Integer offset = (cantidad * (pagina-1) > totalMensajes.intValue()) ? 0 : cantidad * (pagina-1);
		List<MensajeExAlumnos> mensajes = userService.getMensajesEnviadosExAlumno(user.getId(), cantidad, offset);

		logger.info("El usuario " + WordUtils.capitalize(WordUtils.swapCase(user.getNombreCompleto())) + " ha enviado " + mensajes.size() + " mensajes.");

		model.addAttribute("mensajes", mensajes);
		//se pasa al usuario la cantidad de pagina que existen de mensajes
		Double cantidadPaginas = Math.ceil(totalMensajes.doubleValue()/cantidad.doubleValue());
		model.addAttribute("cantidadPaginas", cantidadPaginas.intValue());
		model.addAttribute("paginaActual", pagina);
		model.addAttribute("totalMensajes", totalMensajes);
		model.addAttribute("inicioPagina", (pagina-1)*cantidad + 1);
		model.addAttribute("finalPagina", (pagina*cantidad > totalMensajes.intValue()) ? totalMensajes : pagina*cantidad);
		// ademas se pasa el usuario, para el fin que se estime conveniente
		model.addAttribute("usuario", user);
		model.addAttribute("user_name", ModuloUtilidades.getNombreFormateado((user.getNombreCompleto())));
		model.addAttribute("ModuloUtilidades", new ModuloUtilidades());

		return VISTA_FORM;
	}
}
