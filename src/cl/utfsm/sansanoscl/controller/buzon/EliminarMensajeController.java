package cl.utfsm.sansanoscl.controller.buzon;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cl.utfsm.aexa.modulos.ModuloUsuarioAexa;
import cl.utfsm.aexa.redsocial.MensajeExAlumnos;
import cl.utfsm.sansanoscl.utils.JsonDeserializer;
import cl.utfsm.sansanoscl.utils.ModuloUtilidades;
import cl.utfsm.sansanoscl.validators.MensajeExAlumnoValidator;

@Controller
@RequestMapping(value = "/buzon/eliminarMensaje")
public class EliminarMensajeController {
	@Autowired ModuloUsuarioAexa moduloUsuario;
	@Autowired MensajeExAlumnoValidator validator;
	
	private static final Logger logger = Logger.getLogger(EliminarMensajeController.class);

	@SuppressWarnings("unchecked")
	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody Map<String, ? extends Object> delete(@RequestBody
			Object requested, HttpSession session) {

		MensajeExAlumnos mensaje = new MensajeExAlumnos();
		mensaje = (MensajeExAlumnos) JsonDeserializer.parsingClass(mensaje,requested);

		Long idLogged = (Long) session.getAttribute("userID");
		// validacion
		BindingResult result = new BeanPropertyBindingResult(mensaje, "");
		validator.validate(mensaje, result, idLogged);
		Map<String, String> errores = ModuloUtilidades.parseErrors(result);

		// TODO: se podria modificar el jsonDeserealizer para que parseara
		// fechas, mientras tanto se hace manualmente
		LinkedHashMap<String, String> data = (LinkedHashMap<String, String>) requested;
		String fechaToParse = data.get("fechaEnvioMensaje");
		logger.info(fechaToParse);

		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SS");
		Date fechaEnvio = new Date();
		try {
			fechaEnvio = (Date) formatter.parse(fechaToParse);
		} catch (ParseException e) {
			logger.error(e);
			errores.put("fecha.invalid", "Se debe especificar una fecha correcta");
		} catch(NullPointerException e){
			logger.error(e);
			errores.put("fecha.invalid", "Se debe especificar una fecha correcta");
		}

		if (!errores.isEmpty()){
			return errores;
		} else {
			// setteo la fecha del mensaje, en este punto ya se que la fecha es correcta
			mensaje.setFechaEnvio(fechaEnvio);
			logger.info("fecha envio: " + mensaje.getFechaEnvio());
			// proceso la eliminacion del mensaje
			try {
				//para evitar el info: "handling transient entity in delete processing" que lanza hibernate
				MensajeExAlumnos eliminar = moduloUsuario.getMensajeExAlumno(mensaje);
				moduloUsuario.deleteMensajes(eliminar);
			} catch (Exception e) {
				e.printStackTrace();
				return Collections.singletonMap("error", "true");
			}
			// si todo funciono bien, retorno un flag
			return Collections.singletonMap("flag", "true");
		}
	}
}
