package cl.utfsm.sansanoscl.controller.buzon;

import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cl.utfsm.aexa.modulos.ModuloUsuarioAexa;
import cl.utfsm.aexa.personas.UsuarioAexa;
import cl.utfsm.aexa.redsocial.MensajeExAlumnos;
import cl.utfsm.sansanoscl.utils.JsonDeserializer;
import cl.utfsm.sansanoscl.utils.ModuloUtilidades;
import cl.utfsm.sansanoscl.validators.MensajeExAlumnoValidator;

@Controller
@RequestMapping(value = "/buzon/enviarMensaje")
public class EnviarMensajeController {
	@Autowired ModuloUsuarioAexa moduloUsuario;
	@Autowired MensajeExAlumnoValidator validator;
	
	private static final Logger logger = Logger.getLogger(EnviarMensajeController.class);

	final String VISTA_FORM = "/buzon/formularios/enviarMensaje";
	
	@RequestMapping(method = RequestMethod.GET)
	public String showForm(Model model, HttpSession session) {
		//RECORDAR: se debe agregar en la vista "enviarMensaje" el codigo que soporte el envio de mensajes
		//a un usuario especificado (ver funcion xxx en clase PerfilController)
		return VISTA_FORM;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody Map<String, ? extends Object> create(@RequestBody Object requested, 
			HttpSession session) {
		MensajeExAlumnos mensaje = new MensajeExAlumnos();
		mensaje = (MensajeExAlumnos) JsonDeserializer.parsingClass(mensaje,requested);

		//para aceptar multiples destinatarios, se verifica que sean varios receptores (separados por ',')
		LinkedHashMap<String, String> data = (LinkedHashMap<String, String>) requested;
		String receptores = data.get("receptores");
		if(receptores == null){
			return Collections.singletonMap("error", "true");
		}
		logger.info(receptores);
		String[] idReceptores = null;

		// validacion
		BindingResult result = new BeanPropertyBindingResult(mensaje, "");
		validator.validate(mensaje, result);
		Map<String, String> errores = ModuloUtilidades.parseErrors(result);
		
		if(receptores.compareTo("") != 0){
			idReceptores = receptores.split(",");
		}else{
			errores.put("receptor.required", "Debes seleccionar al menos un receptor para el mensaje");
		}

		// devuelvo los errores si es que existen
		if (!errores.isEmpty()){
			return errores;
		}
		else {
			mensaje.setUsuarioAexa(moduloUsuario.loadUsuarioAexaById((Long) session.getAttribute("userID")));
			mensaje.setFechaEnvio(new Date());
			mensaje.setFechaModificacion(new Date());
			mensaje.setLeido(false);

			//envio el mensaje a todos los receptores
			Boolean error = false;
			for(String idReceptor:idReceptores){
				try{
					UsuarioAexa receptor = moduloUsuario.loadUsuarioAexaById(Long.parseLong(idReceptor));
					if(receptor.getId().compareTo((Long) session.getAttribute("userID")) != 0){
						mensaje.setUsuarioAexaReceptor(receptor);
						moduloUsuario.saveMensajeUsuarioEx(mensaje);
						logger.info("se ha enviado mensaje a "+ModuloUtilidades.getNombreFormateado(receptor.getNombreCompleto()));
					}
				}catch (Exception e) {
					e.printStackTrace();
					error = true;
				}
			}
			if(error)
				return Collections.singletonMap("error", "true");

			return Collections.singletonMap("flag", "true");
		}
	}
}
