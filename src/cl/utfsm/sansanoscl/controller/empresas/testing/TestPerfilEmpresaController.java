package cl.utfsm.sansanoscl.controller.empresas.testing;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;

import cl.utfsm.sansanoscl.controller.empresas.PerfilEmpresaController;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-context.xml" })
@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional
public class TestPerfilEmpresaController {
	@Autowired
	PerfilEmpresaController controller;

	@Test
	public void todoOK() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		MockHttpServletRequest request = new MockHttpServletRequest();
		request.setAttribute("rut", "330544");
		Model model = new ExtendedModelMap();;
		controller.verPerfil(model, request, session);
		assertEquals(false, model.containsAttribute("error"));
	}

	@Test
	public void noRut() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		MockHttpServletRequest request = new MockHttpServletRequest();
		
		Model model = new ExtendedModelMap();
		controller.verPerfil(model, request, session);
		assertEquals(true, model.containsAttribute("error"));
	}
	
	@Test
	public void rutVacio() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		MockHttpServletRequest request = new MockHttpServletRequest();
		request.setAttribute("rut", "");
		Model model = new ExtendedModelMap();;
		controller.verPerfil(model, request, session);
		assertEquals(true, model.containsAttribute("error"));
	}
	
	@Test
	public void rutFalso() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		MockHttpServletRequest request = new MockHttpServletRequest();
		request.setAttribute("rut", "1");
		Model model = new ExtendedModelMap();;
		controller.verPerfil(model, request, session);
		assertEquals(true, model.containsAttribute("error"));
	}	
}
