package cl.utfsm.sansanoscl.controller.empresas.testing;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;

import cl.utfsm.sansanoscl.controller.empresas.BuscarEmpresaController;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-context.xml" })
@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional
public class TestBuscarEmpresaController {
	@Autowired
	BuscarEmpresaController controller;

	/* Busqueda Simple */
	
	@Test
	public void todoOK() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		MockHttpServletRequest request = new MockHttpServletRequest();
		request.setAttribute("rut", "330544");
		Model model = new ExtendedModelMap();
		String texto = "INDUSTRIAS";
		String offset = "0";
		controller.busquedaSimple(texto, offset, session, model);
		assertEquals(false, model.containsAttribute("error"));
	}
	
	@Test
	public void noTexto() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		MockHttpServletRequest request = new MockHttpServletRequest();
		request.setAttribute("rut", "330544");
		Model model = new ExtendedModelMap();
		String texto = null;
		String offset = "0";
		controller.busquedaSimple(texto, offset, session, model);
		assertEquals(true, model.containsAttribute("error"));
	}
	@Test
	public void noOffset() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		MockHttpServletRequest request = new MockHttpServletRequest();
		request.setAttribute("rut", "330544");
		Model model = new ExtendedModelMap();
		String texto = "enap";
		String offset = null;
		controller.busquedaSimple(texto, offset, session, model);
		assertEquals(true, model.containsAttribute("error"));
	}
	
	@Test
	public void textoVacio() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		MockHttpServletRequest request = new MockHttpServletRequest();
		request.setAttribute("rut", "330544");
		Model model = new ExtendedModelMap();
		String texto = "";
		String offset = "0";
		controller.busquedaSimple(texto, offset, session, model);
		assertEquals(false, model.containsAttribute("error"));
	}		
	
	@Test
	public void offsetVacio() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		MockHttpServletRequest request = new MockHttpServletRequest();
		request.setAttribute("rut", "330544");
		Model model = new ExtendedModelMap();
		String texto = "enap";
		String offset = "";
		controller.busquedaSimple(texto, offset, session, model);
		assertEquals(true, model.containsAttribute("error"));
	}
	
	/* Busqueda Avanzada*/
	@Test
	public void todoOKAvanzada() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		MockHttpServletRequest request = new MockHttpServletRequest();
		request.setAttribute("rut", "330544");
		Model model = new ExtendedModelMap();
		String texto = "enap";
		String comuna = "1";
		String offset = "0";
		controller.busquedaAvanzada(texto, comuna, offset, session, model);
		assertEquals(false, model.containsAttribute("error"));
	}
	
	@Test
	public void noTextoAvanzada() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		MockHttpServletRequest request = new MockHttpServletRequest();
		request.setAttribute("rut", "330544");
		Model model = new ExtendedModelMap();
		String texto = null;
		String comuna = "1";
		String offset = "0";
		controller.busquedaAvanzada(texto, comuna, offset, session, model);
		assertEquals(true, model.containsAttribute("error"));	
	}
	@Test
	public void noComuna() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		MockHttpServletRequest request = new MockHttpServletRequest();
		request.setAttribute("rut", "330544");
		Model model = new ExtendedModelMap();
		String texto = "enap";
		String comuna = null;
		String offset = "0";
		controller.busquedaAvanzada(texto, comuna, offset, session, model);
		assertEquals(true, model.containsAttribute("error"));
	}
	@Test
	public void noOffsetAvanzada() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		MockHttpServletRequest request = new MockHttpServletRequest();
		request.setAttribute("rut", "330544");
		Model model = new ExtendedModelMap();
		String texto = "enap";
		String comuna = "1";
		String offset = null;
		controller.busquedaAvanzada(texto, comuna, offset, session, model);
		assertEquals(true, model.containsAttribute("error"));
	}
	
	@Test
	public void textoVacioAvanzada() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		MockHttpServletRequest request = new MockHttpServletRequest();
		request.setAttribute("rut", "330544");
		Model model = new ExtendedModelMap();
		String texto = "";
		String comuna = "1";
		String offset = "0";
		controller.busquedaAvanzada(texto, comuna, offset, session, model);
		assertEquals(false, model.containsAttribute("error"));
	}		
	
	@Test
	public void offsetVacioAvanzada() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		MockHttpServletRequest request = new MockHttpServletRequest();
		request.setAttribute("rut", "330544");
		Model model = new ExtendedModelMap();
		String texto = "enap";
		String comuna = "1";
		String offset = "";
		controller.busquedaAvanzada(texto, comuna, offset, session, model);
		assertEquals(true, model.containsAttribute("error"));
	}

	@Test
	public void comunaFalsa() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		MockHttpServletRequest request = new MockHttpServletRequest();
		request.setAttribute("rut", "330544");
		Model model = new ExtendedModelMap();
		String texto = "enap";
		String comuna = "-1";
		String offset = "0";
		controller.busquedaAvanzada(texto, comuna, offset, session, model);
		assertEquals(false, model.containsAttribute("error"));
	}
}