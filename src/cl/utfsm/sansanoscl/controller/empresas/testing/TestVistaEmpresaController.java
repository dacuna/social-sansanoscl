package cl.utfsm.sansanoscl.controller.empresas.testing;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;

import cl.utfsm.sansanoscl.controller.empresas.VistaEmpresaController;
import cl.utfsm.sansanoscl.utils.EmpresaAutocomplete;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-context.xml" })
@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional
public class TestVistaEmpresaController {
	@Autowired
	VistaEmpresaController controller;

	@Test
	public void todoOK() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		MockHttpServletRequest request = new MockHttpServletRequest();
		request.setAttribute("rut", "1");
		Model model = new ExtendedModelMap();
		assertEquals("empresas", controller.verPerfil(model, session));
	}	
	
	//@Test
	public void autoCompleteEmpresa() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		MockHttpServletRequest request = new MockHttpServletRequest();
		request.setAttribute("rut", "1");
		assertEquals(new ArrayList<EmpresaAutocomplete>(), controller.getEmpresas("ENAP"));
	}
	
	@Test
	public void autoCompleteEmpresaFalsa() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		MockHttpServletRequest request = new MockHttpServletRequest();
		request.setAttribute("rut", "1");
		assertEquals(new ArrayList<EmpresaAutocomplete>(), controller.getEmpresas("sdjsaio"));
	}
}
