package cl.utfsm.sansanoscl.controller.empresas;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cl.utfsm.aexa.AntecedenteLaboral;
import cl.utfsm.aexa.EmpresaExtranjeraAexa;
import cl.utfsm.aexa.SucursalEmpresaExtranjeraAexa;
import cl.utfsm.aexa.modulos.ModuloEmpresaAexa;
import cl.utfsm.sansanoscl.utils.ModuloUtilidades;

@Controller
@RequestMapping(value = "/perfilEmpresaExtranjera")
public class PerfilEmpresaExtranjeraController {
	@Autowired
	ModuloEmpresaAexa moduloEmpresa;
	private static final Logger logger = Logger.getLogger(PerfilEmpresaExtranjeraController.class);
	
	@RequestMapping(method = RequestMethod.GET)
	public String verPerfil(Model model, HttpServletRequest request,HttpSession session){
		String id = (String)request.getAttribute("id");
		//se busca la empresa por rut
		if(id == null || id == ""){
			model.addAttribute("error","true");
			return "perfilEmpresa";
		}
		EmpresaExtranjeraAexa empresa = moduloEmpresa.getEmpresaExtranjeraAexa(Integer.parseInt(id));
		if(empresa == null){
			model.addAttribute("error","true");
			return "perfilEmpresa";
		}
		
		//se buscan las sucursales de la empresa
		List<SucursalEmpresaExtranjeraAexa> sucursales = moduloEmpresa.buscarSucursalesEmpresaExtranjeraAexa(empresa);
		
		//ademas se cargan las personas que trabajan en la empresa (sujeto a cambios)
		List<AntecedenteLaboral> antecedentes = moduloEmpresa.buscarPersonasPorEmpresaExt(empresa);
		//se carga el modulo de utilidades para formatear los nombres
		model.addAttribute("Utilidades",new ModuloUtilidades());
		
		//DEBUG
		logger.info("se tienen "+sucursales.size()+" sucursales");
		logger.info("se tienen "+antecedentes.size()+" personas en la empresa");
		//END DEBUG
		
		model.addAttribute("empresa",empresa);
		model.addAttribute("sucursales",sucursales);
		model.addAttribute("personas",antecedentes);
		
		return "perfilEmpresa";
	}
	
	

}
