package cl.utfsm.sansanoscl.controller.empresas;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cl.utfsm.academia.Empresa;
import cl.utfsm.aexa.AntecedenteLaboral;
import cl.utfsm.aexa.modulos.ModuloEmpresaAexa;
import cl.utfsm.aexa.modulos.ModuloUsuarioAexa;
import cl.utfsm.aexa.personas.UsuarioAexa;
import cl.utfsm.sansanoscl.utils.EmpresaAutocomplete;
import cl.utfsm.sansanoscl.utils.ModuloUtilidades;

@Controller
@RequestMapping(value = "/empresas")
public class VistaEmpresaController {
	@Autowired
	ModuloEmpresaAexa moduloEmpresa;
	@Autowired
	ModuloUsuarioAexa moduloUsuario;
	private static final Logger logger = Logger.getLogger(VistaEmpresaController.class);
	
	@RequestMapping(method = RequestMethod.GET)
	public String verPerfil(Model model,HttpSession session){
		//se obtienen los antecedentes laborales del usuario
		UsuarioAexa user = moduloUsuario.loadUsuarioAexaById((Long) session.getAttribute("userID"));
		List<AntecedenteLaboral> laborales = moduloEmpresa.listarAntecedentesLaborales(user);
		
		logger.info("se tienen "+laborales.size()+" antecedentes laborales");
		
		model.addAttribute("laborales",laborales);
		model.addAttribute("Utilidades",new ModuloUtilidades());
		return "empresas";
	}
	
	//este metodo se encarga de pasar por ajax las empresas (para el autocomplete)
	@RequestMapping(value="/autocomplete", method=RequestMethod.GET)
	public @ResponseBody List<EmpresaAutocomplete> getEmpresas(@RequestParam(value="term") String term) {
		logger.info("PETICION JSON...");
		List<Empresa> empresas = moduloEmpresa.getEmpresasAutocompleteV2(term);
		List<EmpresaAutocomplete> resultados = new ArrayList<EmpresaAutocomplete>();
		for(Empresa e:empresas){
			EmpresaAutocomplete agregar = new EmpresaAutocomplete();
			agregar.setNombre(e.getNombre());
			agregar.setRut(e.getRut());
			resultados.add(agregar);
		}
		logger.info("se tiene un total de "+resultados.size()+" resultados");
		return resultados;
	}
	
	
}
