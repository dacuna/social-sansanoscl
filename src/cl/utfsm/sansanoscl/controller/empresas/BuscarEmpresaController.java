package cl.utfsm.sansanoscl.controller.empresas;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import cl.utfsm.academia.Empresa;
import cl.utfsm.academia.SucursalEmpresa;
import cl.utfsm.aexa.modulos.ModuloEmpresaAexa;

@Controller
@RequestMapping(value = "/buscarEmpresa")
public class BuscarEmpresaController {
	@Autowired
	ModuloEmpresaAexa moduloEmpresa;
	private static final Logger logger = Logger.getLogger(BuscarEmpresaController.class); 
	
	@RequestMapping(method = RequestMethod.GET, value="/busquedaAvanzada")
	public String showForm(Model model, HttpSession session) {
		return "busquedaAvanzada";
	}
	
	//en esta busqueda solo se busca por el nombre de la empresa
	@RequestMapping(method = RequestMethod.POST)
	public String busquedaSimple(@RequestParam("busqueda") String buscar, @RequestParam("offset") String offset, HttpSession session, Model model) {
		//validacion del offset
		Integer nOffset;
		try{
			nOffset = Integer.parseInt(offset);
		}catch (NumberFormatException e) {
			e.printStackTrace();
			model.addAttribute("error","true");
			return "resultadosBusqueda";
		}
		if(buscar == null){
			model.addAttribute("error","true");
			return "resultadosBusqueda";
		}
		List<Empresa> resultados = moduloEmpresa.buscarEmpresaPorNombre(buscar, 10, nOffset);
		if(resultados != null){
			List<SucursalEmpresa> resultadosFinales = new ArrayList<SucursalEmpresa>();
			for(Empresa empresa : resultados){
				logger.info("nombre empresa: "+empresa.getNombre());
				//se obtienen las sucursales
				List<SucursalEmpresa> suc = new ArrayList<SucursalEmpresa>();
				suc = moduloEmpresa.buscarSucursalesEmpresasByRut(empresa);
				
				if(suc != null)
					resultadosFinales.addAll(suc);
			}
			
			model.addAttribute("resultados",resultadosFinales);
			return "resultadosBusqueda";
		}
		
		model.addAttribute("resultados",false);
		return "resultadosBusqueda";
	}
	
	//en esta busqueda se busca por comuna y nombre
	@RequestMapping(method = RequestMethod.POST, value="/avanzadaResultados")
	public String busquedaAvanzada(@RequestParam("busqueda") String buscar, @RequestParam("comuna") String comuna,
			@RequestParam("offset") String offset, HttpSession session, Model model) {
		//validacion del offset
		Integer nOffset;
		try{
			nOffset = Integer.parseInt(offset);
		}catch (NumberFormatException e) {
			e.printStackTrace();
			model.addAttribute("error","true");
			return "resultadosBusqueda";
		}
		//validacion de la comuna
		Integer idComuna;
		try{
			idComuna = Integer.parseInt(comuna);
		}catch (NumberFormatException e) {
			e.printStackTrace();
			model.addAttribute("error","true");
			return "resultadosBusqueda";
		}
		
		//busqueda de resultados
		List<SucursalEmpresa> resultados = moduloEmpresa.buscarSucursalesEmpresaDeComunaPorNombre(buscar, idComuna, 10, nOffset);
		if(resultados != null){
			//se retornan las sucursales de las empresas correspondientes a la comuna seleccionada y al nombre
			model.addAttribute("resultados",resultados);
			return "resultadosBusquedaAvanzada";
		}
		
		model.addAttribute("resultados",false);
		return "resultadosBusquedaAvanzada";
	}

}
