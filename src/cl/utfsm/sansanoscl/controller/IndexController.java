package cl.utfsm.sansanoscl.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cl.utfsm.aexa.modulos.ModuloGrupoAexa;
import cl.utfsm.aexa.modulos.ModuloUsuarioAexa;
import cl.utfsm.aexa.personas.UsuarioAexa;
import cl.utfsm.aexa.redsocial.ContactoExAlumnos;
import cl.utfsm.sansanoscl.models.BuscarCuenta;
import cl.utfsm.sansanoscl.service.UsuarioAexaService;
import cl.utfsm.sansanoscl.utils.ModuloUtilidades;
import cl.utfsm.aexa.redsocial.GrupoExAlumnos;

@Controller
public class IndexController {
	@Autowired UsuarioAexaService userService;
	@Autowired ModuloGrupoAexa moduloGrupo;
	@Autowired ModuloUsuarioAexa moduloUsuario;

	private static final Logger logger = Logger
			.getLogger(IndexController.class);

	@RequestMapping(value = { "", "/", "index" })
	public String home(Model model, HttpSession session) {
		/* Datos escenciales para la vista inicio */
		Long id = (Long) session.getAttribute("userID");
		UsuarioAexa usuario = userService.getUsuarioAexaPorId(id);

		// int userPercentageGood = usuario.getPorcentajeTrayectoria();
		// int userPercentageBad = 100 - userPercentageGood;

		List<GrupoExAlumnos> grupos = null;

		/* Datos agregados a la vista */

		model.addAttribute("user", usuario);
		model.addAttribute("user_id", usuario.getId());
		model.addAttribute("user_name", ModuloUtilidades
				.getNombreFormateado((usuario.getNombreCompleto())));
		model.addAttribute("user_first_name",
				ModuloUtilidades.getNombreFormateado((usuario
						.getNombreCompleto().split(" ")[0])));
		model.addAttribute("mensajes", usuario.getMensajesRecibidos());
		// model.addAttribute("user_percentage_good",
		// Integer.toString(userPercentageGood));
		// model.addAttribute("user_percentage_bad",
		// Integer.toString(userPercentageBad));

		List<UsuarioAexa> contactos = userService.getContactosRecientes(id, 20);
		List<UsuarioAexa> sugeridos = userService.getContactosSugeridos(id);
		sugeridos.addAll(contactos);

		model.addAttribute("sugeridos", sugeridos);
		model.addAttribute("contactos", contactos);

		try {
			grupos = moduloGrupo.obtenerGruposDeUsuario(usuario, 5, 0);
			System.out.println("size grupos: " + grupos.size());
			model.addAttribute("grupos", grupos);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error.exist", "true");
		}

		model.addAttribute("Utilidades", new ModuloUtilidades());

		List<ContactoExAlumnos> solicitudes = userService
				.getSolicitudesContacto((Long) session.getAttribute("userID"));
		logger.info("Agregados un total de " + solicitudes.size()
				+ " solicitudes.");
		for (ContactoExAlumnos solicitud : solicitudes) {
			logger.info("Mensaje:" + solicitud.getMensaje());
		}
		model.addAttribute("solicitudes", solicitudes);

		// testing: ignorar esto
		// List<GrupoExAlumnos> u = grupos;
		/*
		 * for(GrupoExAlumnos us:u){ Set<DiscusionExAlumnos> a =
		 * us.getDiscusiones(); for(DiscusionExAlumnos as:a){ //inception para
		 * rescatar a un miembro random de un grupo x as.getTitulo();
		 * 
		 * } }
		 */

		return "index";
	}

	// //buscar amigos por antecedentes laborales, ocupacion anterior
	// // List<AntecedenteLaboral> laboralUsuario =
	// moduloUsuario.getOcupacionAnterior(user);
	// // if (laboralUsuario.size() != 0) {
	// // for (AntecedenteLaboral antecedente : laboralUsuario) {
	// // if(antecedente.getSucursalEmpresa() != null){
	// // List<UsuarioAexa> ocupacionAnterior =
	// usuarioDao.getUsuariosPorAntecedenteLaboral(user.getId(), antecedente,
	// 2);
	// // if(ocupacionAnterior != null)
	// // recomendaciones.addAll(ocupacionAnterior);
	// // }
	// // }
	// // }
	//
	// if(((Integer)laboralUsuario.size()).compareTo((Integer)antUsuario.size())
	// > 0){
	// int i = 0;
	// for (AntecedenteLaboral antecedente : laboralUsuario) {
	// if(antecedente.getSucursalEmpresa() != null){
	// List<UsuarioAexa> ocupacionAnterior =
	// userService.superConsulta(user.getId(), antecedente, antUsuario.get(i),
	// 1);
	// if(ocupacionAnterior != null)
	// recomendaciones.addAll(ocupacionAnterior);
	// }
	// i++;
	// }
	// }else{
	// int i = 0;
	// for (AntecedenteEducacional antecedente : antUsuario) {
	// AntecedenteLaboral antla = new AntecedenteLaboral();
	// if(laboralUsuario.size() <= i)
	// antla = null;
	// else
	// antla = laboralUsuario.get(i);
	// List<UsuarioAexa> educacionales = userService.superConsulta(user.getId(),
	// antla, antecedente, 1);
	// if(educacionales != null)
	// recomendaciones.addAll(educacionales);
	// i++;
	// }
	// }
	//
	// //buscar amigos por antecedentes laborales, ocupacion actual
	// // List<AntecedenteLaboral> laboralUsuarioActual =
	// moduloUsuario.getOcupacionActual(user);
	// // if (laboralUsuarioActual.size() != 0) {
	// // for (AntecedenteLaboral antecedente : laboralUsuarioActual) {
	// // if(antecedente.getSucursalEmpresa() != null){
	// // List<UsuarioAexa> ocupacionActual =
	// userService.getUsuariosPorAntecedenteLaboral(user.getId(), antecedente,
	// 1);
	// // if(ocupacionActual != null)
	// // recomendaciones.addAll(ocupacionActual);
	// // }
	// // }
	// // }
	//
	// Set<UsuarioAexa> resultados = new HashSet<UsuarioAexa>();
	//
	// if(recomendaciones != null)
	// resultados.addAll(recomendaciones);
	//
	// //ahora tengo que sacar a los que ya sean contactos
	// List<UsuarioAexa> contactosActuales =
	// moduloUsuario.getContactosByIdUser(user.getId());
	// resultados.removeAll(contactosActuales);
	//
	// return resultados;
	// }

	@RequestMapping(value = "/buscar-mi-cuenta")
	public String showForm(Model model, HttpSession session) {
		return "buscar-cuenta";
	}
	
	@RequestMapping(method = RequestMethod.POST,value = "/buscar-mi-cuenta")
	public String buscarCuenta(@ModelAttribute("buscarCuenta") BuscarCuenta buscarCuenta, BindingResult result,Model model,HttpSession session) {
		
		if(buscarCuenta.getEmail()!=null && buscarCuenta.getEmail().compareTo("")!=0){
			//buscar por email
			UsuarioAexa usuario=userService.getUsuarioAexaPorEmail(buscarCuenta.getEmail());
			if(usuario!=null){
				model.addAttribute("usuarioEmail", usuario);
				model.addAttribute("showMail",true);
				logger.info("ENCONTRADO USUARIO "+usuario.getNombreCompleto());
			}
		}
		
		if(buscarCuenta.getRut()!=null && buscarCuenta.getRut().compareTo("")!=0){
			//buscar por email
			if(ModuloUtilidades.checkIfNumber(buscarCuenta.getRut())){
				UsuarioAexa usuario=moduloUsuario.buscarUsuariosAexa(buscarCuenta.getRut());
				if(usuario!=null){
					model.addAttribute("usuarioRut", usuario);
					model.addAttribute("showRut",true);
					logger.info("ENCONTRADO USUARIO "+usuario.getNombreCompleto());
				}
			}
		}
		
		return "buscar-cuenta-result";
	}

}
