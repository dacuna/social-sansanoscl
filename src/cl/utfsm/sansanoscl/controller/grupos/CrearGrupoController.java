package cl.utfsm.sansanoscl.controller.grupos;

import java.util.Collections;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cl.utfsm.aexa.modulos.ModuloGrupoAexa;
import cl.utfsm.aexa.modulos.ModuloUsuarioAexa;
import cl.utfsm.aexa.personas.UsuarioAexa;
import cl.utfsm.aexa.redsocial.GrupoExAlumnos;
import cl.utfsm.aexa.redsocial.MiembroGrupoExalumnos;
import cl.utfsm.aexa.tipos.TipoRolMiembroExAlumnos;
import cl.utfsm.sansanoscl.utils.JsonDeserializer;
import cl.utfsm.sansanoscl.utils.ModuloUtilidades;
import cl.utfsm.sansanoscl.validators.GrupoExAlumnoValidator;

@Controller
@RequestMapping(value = "/grupos/crearGrupo")
public class CrearGrupoController {
	@Autowired ModuloGrupoAexa moduloGrupo;
	@Autowired ModuloUsuarioAexa moduloUsuario;
	@Autowired GrupoExAlumnoValidator grupoExAlumnoValidator;

	private final String VISTA_FORM = "/grupos/crearGrupo";

	@RequestMapping(method = RequestMethod.GET)
	public String showForm(Model model, HttpSession session) {
		UsuarioAexa user = moduloUsuario.loadUsuarioAexaById((Long) session.getAttribute("userID"));

		model.addAttribute("tiposGrupo", moduloGrupo.getTiposGrupoExAlumno());
		model.addAttribute("user", user);

		return VISTA_FORM;
	}

	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody
	Map<String, ? extends Object> create(@RequestBody Object requested,
			HttpServletResponse response, HttpSession session) {
		GrupoExAlumnos grupo = new GrupoExAlumnos();
		grupo = (GrupoExAlumnos) JsonDeserializer.parsingClass(grupo, requested);
		BindingResult result = new BeanPropertyBindingResult(grupo, "");
		Map<String, String> errores = null;
		UsuarioAexa user = moduloUsuario.loadUsuarioAexaById((Long) session.getAttribute("userID"));

		grupoExAlumnoValidator.validate(grupo, result);
		errores = ModuloUtilidades.parseErrors(result);
		if (!errores.isEmpty()) {
			errores.put("errores.exist", "true");
			return errores;
		}

		try {
			grupo.setUsuarioEncargado(user);
			grupo.setFechaModificacion(new Date());
			grupo.setIntegrantes(0);
			moduloGrupo.insertarGrupo(grupo);
			// agrego un miembro al grupo
			MiembroGrupoExalumnos nuevoMiembro = new MiembroGrupoExalumnos();
			nuevoMiembro.setUsuarioAexa(user);
			nuevoMiembro.setFechaModificacion(new Date());
			nuevoMiembro.setGrupoExAlumnos(grupo);
			nuevoMiembro.setRecibirMensajesDiscusiones(true);
			nuevoMiembro.setRecibirMensajesEventos(true);
			TipoRolMiembroExAlumnos tipoRol = moduloGrupo.getTipoRolesMiembrosExAlumnos("ADMIN");
			nuevoMiembro.setTipoRolMiembroExAlumnos(tipoRol);
			moduloGrupo.insertarMiembroGrupoExAlumnos(nuevoMiembro);
		} catch (Exception e) {
			e.printStackTrace();
			return Collections.singletonMap("error.exist", "true");
		}

		// se retorna un flag que indica que todo resulto correctamente
		return Collections.singletonMap("flag", String.valueOf(grupo.getId()));
	}

}
