package cl.utfsm.sansanoscl.controller.grupos;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cl.utfsm.aexa.modulos.ModuloGrupoAexa;
import cl.utfsm.aexa.personas.UsuarioAexa;
import cl.utfsm.aexa.redsocial.DiscusionExAlumnos;
import cl.utfsm.aexa.redsocial.GrupoExAlumnos;
import cl.utfsm.aexa.redsocial.PostExAlumnos;
import cl.utfsm.sansanoscl.utils.JsonDeserializer;
import cl.utfsm.sansanoscl.utils.ModuloUtilidades;
import cl.utfsm.sansanoscl.validators.GrupoExAlumnoValidator;
import cl.utfsm.sansanoscl.validators.PostExAlumnoValidator;

@Controller
@RequestMapping(value = "/grupos/discusiones/participarDiscusion")
public class EnviarMensajeDiscusionController {
	@Autowired ModuloGrupoAexa moduloGrupo;
	@Autowired PostExAlumnoValidator postExAlumnoValidator;
	@Autowired GrupoExAlumnoValidator grupoExAlumnoValidator;

	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody Map<String, ? extends Object> create(@RequestBody Object requested,
			HttpSession session) {
		PostExAlumnos post = new PostExAlumnos();
		UsuarioAexa user = new UsuarioAexa();
		user.setId((Long) session.getAttribute("userID"));
		BindingResult result = new BeanPropertyBindingResult(post, "");
		Map<String, String> errores = new HashMap<String, String>();

		post = (PostExAlumnos) JsonDeserializer.parsingClass(post, requested);
		postExAlumnoValidator.validate(post, result);
		errores = ModuloUtilidades.parseErrors(result);
		
		if (!errores.isEmpty()) {
			errores.put("errores.exist", "true");
			return Collections.singletonMap("error.exist", "true");
		}	
		post.setFechaModificacion(new Date());
		post.setUsuarioAexa(user);

		try {
			//verificar que el usuario pertenece al grupo
			GrupoExAlumnos grupo = moduloGrupo.loadGrupo(moduloGrupo.getDiscusionById(post.getDiscusionExAlumnos().getId()).getGrupoExAlumnos().getId());
			grupoExAlumnoValidator.validateUsuarioPerteneceGrupo(grupo, user, result);
			if (!errores.isEmpty()) {
				errores.put("errores.exist", "true");
				return Collections.singletonMap("error.exist", "true");
			}
			moduloGrupo.savePostEx(post);
			//sumo 1 al total de mensajes de la discusion
			DiscusionExAlumnos discusion = moduloGrupo.getDiscusionById(post.getDiscusionExAlumnos().getId());
			discusion.setMensajes(discusion.getMensajes() + 1);
			moduloGrupo.updateDiscusionGrupo(discusion);
		} catch (Exception e) {
			e.printStackTrace();
			return Collections.singletonMap("error.exist", "true");
		}

		return Collections.singletonMap("flag", "true");
	}

}
