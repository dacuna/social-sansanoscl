package cl.utfsm.sansanoscl.controller.grupos;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cl.utfsm.aexa.modulos.ModuloGrupoAexa;
import cl.utfsm.aexa.modulos.ModuloUsuarioAexa;
import cl.utfsm.aexa.personas.UsuarioAexa;

import cl.utfsm.aexa.redsocial.GrupoExAlumnos;
import cl.utfsm.aexa.redsocial.MiembroGrupoExalumnos;
import cl.utfsm.sansanoscl.utils.ModuloUtilidades;
import cl.utfsm.sansanoscl.validators.GrupoExAlumnoValidator;

@Controller
@RequestMapping(value = "/grupos/invitacionGrupo")
public class AceptarInvitacionGrupoController {
	@Autowired ModuloGrupoAexa moduloGrupo;
	@Autowired ModuloUsuarioAexa userService;
	@Autowired GrupoExAlumnoValidator grupoExAlumnoValidator;

	@SuppressWarnings("unused")
	private static final Logger logger = Logger.getLogger(AceptarInvitacionGrupoController.class);

	@RequestMapping(method = RequestMethod.POST, value = "/aceptar")
	public @ResponseBody Map<String, ? extends Object> aceptar(@RequestBody String idGrupo,
			HttpSession session) {

		// Map<String, String> errors = moduloGrupo.validaGrupoPorId(idGrupo);
		UsuarioAexa user = userService.loadUsuarioAexaById((Long) session.getAttribute("userID"));
		GrupoExAlumnos grupo = new GrupoExAlumnos();
		MiembroGrupoExalumnos membresia = null;
		BindingResult result = new BeanPropertyBindingResult(grupo, "");
		Map<String, String> errores = new HashMap<String, String>();

		if (!ModuloUtilidades.checkIfNumber(idGrupo))
			return Collections.singletonMap("error.exist", "true");

		try {
			grupo = moduloGrupo.loadGrupo(Integer.parseInt(idGrupo));
		} catch (Exception e1) {
			e1.printStackTrace();
			errores.put("errores.exist", "true");
			return Collections.singletonMap("error.exist", "true");
		}
		if (grupo == null)
			return Collections.singletonMap("error.exist", "true");

		try {
			membresia = moduloGrupo.usuarioPerteneceGrupo(user, grupo);
		} catch (Exception e1) {
			e1.printStackTrace();
			errores.put("error.exist", "true");
			return Collections.singletonMap("error.exist", "true");
		}
		if (membresia == null)
			return Collections.singletonMap("errorMembresiaInvitadoNoExiste", "true");
		grupoExAlumnoValidator.validateUsuarioPerteneceGrupo(grupo, user, result);
		grupoExAlumnoValidator.validateUsuarioInvitadoGrupo(grupo, membresia, result);
		errores = ModuloUtilidades.parseErrors(result);
		if (!errores.isEmpty()) 
			return errores;

		try {
			membresia.setTipoRolMiembroExAlumnos(moduloGrupo.getTipoRolesMiembrosExAlumnos("USUARIO"));
			moduloGrupo.updateMembresiaGrupo(membresia);
		} catch (Exception e) {
			e.printStackTrace();
			errores.put("error.exist", "true");
			return Collections.singletonMap("error.exist", "true");
		}

		return Collections.singletonMap("flag", "true");

	}

}
