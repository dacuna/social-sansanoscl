package cl.utfsm.sansanoscl.controller.grupos;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;

import java.util.List;

import java.util.Map;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cl.utfsm.aexa.modulos.ModuloGrupoAexa;
import cl.utfsm.aexa.personas.UsuarioAexa;
import cl.utfsm.aexa.redsocial.GrupoExAlumnos;
import cl.utfsm.aexa.redsocial.MiembroGrupoExalumnos;
import cl.utfsm.aexa.tipos.TipoRolMiembroExAlumnos;
import cl.utfsm.sansanoscl.service.UsuarioAexaService;
import cl.utfsm.sansanoscl.utils.ModuloUtilidades;
import cl.utfsm.sansanoscl.validators.GrupoExAlumnoValidator;

@Controller
@RequestMapping(value = "/grupos/abandonar")
public class AbandonarGrupoController {
	@Autowired ModuloGrupoAexa moduloGrupo;
	@Autowired UsuarioAexaService userService;
	@Autowired GrupoExAlumnoValidator grupoExAlumnoValidator;

	private static final Logger logger = Logger.getLogger(AbandonarGrupoController.class);
	private final static String VISTA_FORM = "grupos/abandonar";

	
	@RequestMapping(method = RequestMethod.GET)
	public String showForm(@RequestParam(value="id") String id, Model model, HttpSession session,
			HttpServletRequest request) {
		UsuarioAexa user = userService.getUsuarioAexaPorId((Long) session.getAttribute("userID"));
		GrupoExAlumnos grupo = null;
		Map<String, String> errores = null;
		List<MiembroGrupoExalumnos> listaMiembroGrupoExalumnos = new ArrayList<MiembroGrupoExalumnos>();
		
		if (!ModuloUtilidades.checkIfNumber(id)) {
			model.addAttribute("grupoInexistente", "true");
			return VISTA_FORM;
		}

		grupo = moduloGrupo.loadGrupo(Integer.parseInt(id));

		if (grupo == null) {
			model.addAttribute("grupoInexistente", "true");
			return VISTA_FORM;
		}
		
		model.addAttribute("grupo", grupo);
		BindingResult result = new BeanPropertyBindingResult(grupo, "");
		grupoExAlumnoValidator.validateUsuarioPerteneceGrupo(grupo, user, result);
		errores = ModuloUtilidades.parseErrors(result);
		if (!errores.isEmpty()) {
			errores.put("errores.exist", "true");
			return VISTA_FORM;
		}

		// si el grupo tiene solo un integrante, el sistema advierte
		// que el grupo se eliminara completamente
		if (grupo.getIntegrantes() == 1) {
			model.addAttribute("abandonarConEliminacionGrupo", 1);
			return VISTA_FORM;
		}

		// si es el usuario encargado quien abandona el grupo, entonces este
		// usuario debe seleccionar a un integrante del grupo como administrador
		// del grupo
		
		
		else{ 
			
			if (grupo.getUsuarioEncargado() != null && grupo.getUsuarioEncargado().getId() == user.getId()) {
				for(MiembroGrupoExalumnos m : grupo.getMiembros()){
					if(m.getUsuarioAexa().getId() != grupo.getUsuarioEncargado().getId() )
						listaMiembroGrupoExalumnos.add(m);
				}
												
			
		
				model.addAttribute("integrantes", listaMiembroGrupoExalumnos);
				return "/grupos/abandonar/nuevoAdmin";
			}
		}
		
		// esta seguro que quiere abandonar el grupo?

		return VISTA_FORM;
	}

	
	@RequestMapping(value="/nuevoAdmin", method = RequestMethod.POST)
	public @ResponseBody Map<String, ? extends Object> abandonarConNuevoAdmin(@RequestBody Object requested, HttpServletResponse response,
				HttpSession session) {

		UsuarioAexa user = userService.getUsuarioAexaPorId((Long) session.getAttribute("userID"));
		GrupoExAlumnos grupo = null;
		BindingResult result = new BeanPropertyBindingResult(grupo, "");
		Map<String, String> errores = new HashMap<String, String>();
		@SuppressWarnings("unchecked")
		LinkedHashMap<String, String> data = (LinkedHashMap<String, String>) requested;
		System.out.println(data.get("grupoExAlumnos.id"));
		
		String idGrupo = data.get("grupoExAlumnos.id");
		String idUsuarioSeleccionado = data.get("seleccionado");
		System.out.println(idUsuarioSeleccionado);
		UsuarioAexa usuarioSeleccionado = userService.getUsuarioAexaPorId(Long.parseLong(idUsuarioSeleccionado));
		if (!ModuloUtilidades.checkIfNumber(idGrupo)) {
			return Collections.singletonMap("error.exist", "true");
		}

		try {
			grupo = moduloGrupo.loadGrupo(Integer.parseInt(idGrupo));
		} catch (Exception e1) {
			e1.printStackTrace();
			return Collections.singletonMap("error.exist", "true");
		}
		if (grupo == null)
			return Collections.singletonMap("error.exist", "true");

		grupoExAlumnoValidator.validateUsuarioPerteneceGrupo(grupo, usuarioSeleccionado, result);
		grupoExAlumnoValidator.validateUsuarioEncargadoGrupo(grupo, user, result);

		errores = ModuloUtilidades.parseErrors(result);
		if (!errores.isEmpty()) {
			errores.put("errores.exist", "true");
			return Collections.singletonMap("error.exist", "true");
		}

		try {
			logger.info("idNuevoAdmin: " + idUsuarioSeleccionado);
			
			//setea nuevo usuario como encargado de grupo
			grupo.setUsuarioEncargado(usuarioSeleccionado); 
			grupo.setFechaModificacion(new Date());

			// rescato objeto membresia de nuevo usuario encargado
			MiembroGrupoExalumnos membresia = moduloGrupo.usuarioPerteneceGrupo(usuarioSeleccionado, grupo);
			membresia.setFechaModificacion(new Date());

			TipoRolMiembroExAlumnos tipoRolMiembroExAlumnos = moduloGrupo.getTipoRolesMiembrosExAlumnos("ADMIN");

			membresia.setRecibirMensajesDiscusiones(true);
			membresia.setRecibirMensajesEventos(true);
			// setea la calidad de ADMIN de grupo
			membresia.setTipoRolMiembroExAlumnos(tipoRolMiembroExAlumnos);
			moduloGrupo.updateMembresiaGrupo(membresia); // actualiza membresia de nuevo admin
			moduloGrupo.updateInformacionGrupo(grupo);

			// recata membresia de admin antiguo
			membresia = moduloGrupo.usuarioPerteneceGrupo(user, grupo);
			// elimino membresia
			moduloGrupo.eliminarMiembroGrupoExalumnos(membresia);

		} catch (Exception e) {
			e.printStackTrace();
			return Collections.singletonMap("error.exist", "true");
		}

		return Collections.singletonMap("flag", "true");

	}

	@RequestMapping("/eliminacionGrupo")
	public @ResponseBody
	Map<String, ? extends Object> abandonarGrupo(@RequestBody String idGrupo, 
			HttpServletResponse response, HttpSession session) {

		UsuarioAexa user = new UsuarioAexa();
		user.setId((Long) session.getAttribute("userID"));
		GrupoExAlumnos grupo = new GrupoExAlumnos();
		BindingResult result = new BeanPropertyBindingResult(grupo, "");
		Map<String, String> errores = new HashMap<String, String>();
		

		if (!ModuloUtilidades.checkIfNumber(idGrupo))
			return Collections.singletonMap("error.exist", "true");
		try {
			grupo = moduloGrupo.loadGrupo(Integer.parseInt(idGrupo));
		} catch (Exception e1) {
			e1.printStackTrace();
			return Collections.singletonMap("error.exist", "true");
		}
		if (grupo == null)
			return Collections.singletonMap("error.exist", "true");

		grupoExAlumnoValidator.validateUsuarioPerteneceGrupo(grupo, user, result);

		errores = ModuloUtilidades.parseErrors(result);
		if (!errores.isEmpty()) {
			return errores;
		}
		
		try {
			MiembroGrupoExalumnos miembro = moduloGrupo.getMiembroGrupoPorUsuario(user.getId(), grupo.getId());
			moduloGrupo.eliminarMiembroGrupoExalumnos(miembro);
			//si el grupo contiene solo 1 miembro, se elimina, sino solo se elimina el miembro
			if(grupo.getIntegrantes() == 1)
				moduloGrupo.eliminarGrupo(grupo);
		} catch (Exception e) {
			e.printStackTrace();
			return Collections.singletonMap("eliminar.error", "true");
		}

		return Collections.singletonMap("flag", "true");

	}

}
