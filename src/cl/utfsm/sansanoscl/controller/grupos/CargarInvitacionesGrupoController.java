package cl.utfsm.sansanoscl.controller.grupos;

import java.util.List;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cl.utfsm.aexa.modulos.ModuloGrupoAexa;
import cl.utfsm.aexa.modulos.ModuloUsuarioAexa;
import cl.utfsm.aexa.personas.UsuarioAexa;

import cl.utfsm.aexa.redsocial.GrupoExAlumnos;
import cl.utfsm.aexa.tipos.TipoRolMiembroExAlumnos;

@Controller
@RequestMapping(value = "/grupos/invitacionGrupo")
public class CargarInvitacionesGrupoController {
	@Autowired
	ModuloGrupoAexa moduloGrupo;
	@Autowired
	ModuloUsuarioAexa userService;

	private final String VISTA_FORM = "/grupos/formularios/verInvitacionesGrupo";

	@RequestMapping(method = RequestMethod.GET)
	public String showForm(Model model, HttpSession session) {
		UsuarioAexa user = userService.loadUsuarioAexaById((Long) session
				.getAttribute("userID"));
		TipoRolMiembroExAlumnos rolInvitado = null;
		List<GrupoExAlumnos> invitaciones = null;

		try {
			rolInvitado = moduloGrupo.getTipoRolesMiembrosExAlumnos("INVITADO");
			invitaciones = moduloGrupo.obtenerInvitacionesUsuario(user.getId(),
					rolInvitado.getId());
			model.addAttribute("invitaciones", invitaciones);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error.exist", "true");

		}

		return VISTA_FORM;
	}
}
