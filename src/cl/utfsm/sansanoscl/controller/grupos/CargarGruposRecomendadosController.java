package cl.utfsm.sansanoscl.controller.grupos;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cl.utfsm.aexa.modulos.ModuloGrupoAexa;
import cl.utfsm.aexa.modulos.ModuloUsuarioAexa;
import cl.utfsm.aexa.personas.UsuarioAexa;
import cl.utfsm.aexa.redsocial.GrupoExAlumnos;
import cl.utfsm.sansanoscl.service.UsuarioAexaService;
import cl.utfsm.sansanoscl.utils.ModuloUtilidades;

@Controller
@RequestMapping(value = "/grupos/gruposRecomendados")
public class CargarGruposRecomendadosController {
	@Autowired
	ModuloGrupoAexa moduloGrupo;
	@Autowired
	ModuloUsuarioAexa moduloUsuarioAexa;
	@Autowired
	UsuarioAexaService usuarioAexaSevice;
	int cantidad = 4;
	
	final String VISTA_FORM = "/grupos/gruposRecomendados";
	private static final Logger logger = Logger.getLogger(CargarGruposRecomendadosController.class);

	@RequestMapping(method = RequestMethod.GET)
	public String cargar(String page, Model model, HttpSession session) {
		UsuarioAexa user = moduloUsuarioAexa.loadUsuarioAexaById((Long)session.getAttribute("userID"));
		List<GrupoExAlumnos> grupos = null;

		try {
			grupos = moduloGrupo.getGruposRelacionadoAId(user.getId());
			//
			
			if(grupos == null){	
				grupos = new ArrayList<GrupoExAlumnos>();
				List<UsuarioAexa> lista = usuarioAexaSevice.getContactosSugeridos(user.getId());
				for(UsuarioAexa u:lista){
					List<GrupoExAlumnos> gruposUsuarioRelac = moduloGrupo.getGruposRelacionadoAId(u.getId());
					if(gruposUsuarioRelac != null)grupos.addAll(gruposUsuarioRelac);
					
					Set<GrupoExAlumnos> gs = u.getGrupos();
					if(gs != null)grupos.addAll(gs);
					
				}
			}
			if(grupos.size() == 0){
				Integer pagina = !ModuloUtilidades.checkIfNumber(page) ? 1 : Integer.parseInt(page);
				Number totalMensajes = usuarioAexaSevice.getTotalGrupos();
				Integer offset = (cantidad * (pagina-1) > totalMensajes.intValue()) ? 0 : cantidad * (pagina-1);
				
				grupos = usuarioAexaSevice.directorioGrupos(cantidad, offset);
			}
			logger.info("gruposRecomendados: "+grupos.size());
			model.addAttribute("gruposRecomendados", grupos);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error.exist", "true");
		}
		
		model.addAttribute("usuario", user);
		model.addAttribute("user_name", ModuloUtilidades.getNombreFormateado((user.getNombreCompleto())));
		model.addAttribute("Utilidades", new ModuloUtilidades());
		return VISTA_FORM;
	}
}
