package cl.utfsm.sansanoscl.controller.grupos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import cl.utfsm.aexa.AntecedenteLaboral;
import cl.utfsm.aexa.modulos.ModuloGrupoAexa;
import cl.utfsm.aexa.modulos.ModuloUsuarioAexa;
import cl.utfsm.aexa.personas.UsuarioAexa;
import cl.utfsm.aexa.redsocial.GrupoExAlumnos;
import cl.utfsm.aexa.redsocial.MiembroGrupoExalumnos;
import cl.utfsm.sansanoscl.dao.UsuarioDao;
import cl.utfsm.sansanoscl.utils.ModuloUtilidades;
import cl.utfsm.sansanoscl.validators.GrupoExAlumnoValidator;

@Controller
@RequestMapping(value = "/grupos/perfilgrupo")
public class CargarPerfilGrupoController {
	@Autowired ModuloGrupoAexa moduloGrupo;
	@Autowired ModuloUsuarioAexa moduloUsuario;
	@Autowired UsuarioDao usuarioDao;
	@Autowired GrupoExAlumnoValidator grupoExAlumnoValidator;

	private final String VISTA_PERFIL = "/grupos/perfilGrupo";
	private final String VISTA_MIEMBROS = "/grupos/miembrosGrupo";
	private static final Logger logger = Logger.getLogger(CargarPerfilGrupoController.class);

	@RequestMapping(method = RequestMethod.GET)
	public String perfilGrupo(@RequestParam(value= "id", defaultValue = "null") String id,
			Model model, HttpSession session) {
		UsuarioAexa user = moduloUsuario.loadUsuarioAexaById((Long) session.getAttribute("userID"));
		GrupoExAlumnos ver = null;
		MiembroGrupoExalumnos miembro = null;
		if (!ModuloUtilidades.checkIfNumber(id)) {
			model.addAttribute("grupo.invalid", "true");
			return VISTA_PERFIL;
		}

		try {
			ver = moduloGrupo.loadGrupo(Integer.parseInt(id));
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error.exist", "true");
			return VISTA_PERFIL;
		}
		if (ver == null) {
			model.addAttribute("error.exist", "true");
			return VISTA_PERFIL;
		}

		model.addAttribute("grupo", ver);
		// si es que el usuario no pertenece al grupo y ademas el grupo es cerrado, 
		// entonces el usuario no puede ver la infomacion del grupo, se debe notificar esto
		try {
			miembro = moduloGrupo.getMiembroGrupoPorUsuario(user.getId(), ver.getId());
			} catch (Exception e1) {
			e1.printStackTrace();
		}
		if(miembro != null){
			model.addAttribute("membresia", true);

			System.out.println("miembro: "+ miembro.getTipoRolMiembroExAlumnos().getNombreUsuario().toString());
			
			if(miembro.getTipoRolMiembroExAlumnos().getNombreUsuario().toString().equals("SOLICITANTE")){
				model.addAttribute("solicitante", "true");
				model.addAttribute("permisos", "false");
				return VISTA_PERFIL;
			}
			else{
				if(miembro.getTipoRolMiembroExAlumnos().getNombreUsuario().toString().equals("ADMIN")){
					List<MiembroGrupoExalumnos> listaSolicitudes = moduloGrupo.getSolicitudesMembresiaGrupo(ver.getId());
					System.out.println("cant solicitudes: "+listaSolicitudes.size() );
					model.addAttribute("solicitudes", listaSolicitudes.size());
					model.addAttribute("listaSolicitudes", listaSolicitudes);
				}
						
			}
		}
		System.out.println("permisos: " + ver.getPermisos());
		if (miembro == null && ver.getPermisos() == 0 ) {
			model.addAttribute("grupoId", ver.getId());
			model.addAttribute("permisos", "false");
			return VISTA_PERFIL;
		} 
		
		model.addAttribute("user", user);
		return VISTA_PERFIL;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/miembros")
	public String verMiembros(@RequestParam(value= "id", defaultValue = "null") String id,
			Model model, HttpSession session) {
		GrupoExAlumnos ver = new GrupoExAlumnos() ;
		List<String> actuales = new ArrayList<String>();
		List<MiembroGrupoExalumnos> miembros = null;
		BindingResult result = new BeanPropertyBindingResult(ver, "");
		Map<String, String> errores = new HashMap<String, String>();
		UsuarioAexa user = new UsuarioAexa();
		user.setId((Long) session.getAttribute("userID"));

		// obtener la id del grupo a crear
		if (!ModuloUtilidades.checkIfNumber(id)) {
			model.addAttribute("grupo.invalid", "true");
			return VISTA_MIEMBROS;
		}

		try {
			ver = moduloGrupo.loadGrupo(Integer.parseInt(id));
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error.exist", "true");
		System.out.println("grupo id: "+ ver.getId());
		}
		if (ver == null) {
			model.addAttribute("grupoNoExiste", "true");
			model.addAttribute("error.exist", "true");
			return VISTA_MIEMBROS;
		}

		// si es que el usuario no pertenece al grupo y ademas el grupo es
		// cerrado, entonces el usuario no
		// puede ver la infomacion del grupo, se debe notificar esto
		MiembroGrupoExalumnos pertenece = moduloGrupo.usuarioPerteneceGrupo(user, ver);
		if (ver.getPermisos() == 0 && pertenece == null) {
			model.addAttribute("permisos", "false");
			model.addAttribute("error.exist", "true");
			return VISTA_MIEMBROS;
		}
		// se verifica si el usuario logeado pertenece a este grupo
		grupoExAlumnoValidator.validateUsuarioPerteneceGrupo(ver, user, result);
		errores = ModuloUtilidades.parseErrors(result);
		if (!errores.isEmpty()) {
			errores.put("errores.exist", "true");
			return VISTA_PERFIL;
		}

		try {
			miembros = moduloGrupo.getIntegrantesGrupo(Integer.parseInt(id));
			model.addAttribute("miembros", miembros);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error.exist", "true");
			return VISTA_MIEMBROS;
		}
		if (miembros != null) {
			model.addAttribute("miembros", miembros);
			// se agrega tambien a cada miembro su antecedente laboral actual,
			// de esta forma se puede presentar
			// el nombre de cada integrante del grupo y bajo este se coloca la
			// ocupacion actual

			for (MiembroGrupoExalumnos m : miembros) {
				AntecedenteLaboral antecedentes = usuarioDao
						.getAntecedenteLaboralMasReciente(m.getUsuarioAexa()
								.getId());
				if (antecedentes != null) {
					if (antecedentes.getSucursalEmpresa() != null)
						actuales.add(antecedentes.getCargo()
								+ " en "
								+ ModuloUtilidades
										.getNombreFormateado(antecedentes
												.getSucursalEmpresa()
												.getEmpresa().getNombre()));
					else
						actuales.add(antecedentes.getCargo()
								+ " en "
								+ ModuloUtilidades
										.getNombreFormateado(antecedentes
												.getSucursalEmpresaExtranjeraAexa()
												.getEmpresa().getNombre()));
				} else
					// se agrega un valor vacio a la lista, esto permite
					// flexibilidad en la vista de este metodo
					actuales.add(" ");
			}
			MiembroGrupoExalumnos miembro = moduloGrupo.usuarioPerteneceGrupo(user,
					ver);
			model.addAttribute("miembro", miembro);
			model.addAttribute("ModuloUtilidades", new ModuloUtilidades());
			if(miembro.getTipoRolMiembroExAlumnos().getNombreUsuario().toString().equals("SOLICITANTE"))
				model.addAttribute("solicitante", "true");
			model.addAttribute("antecedentes", actuales);
			model.addAttribute("grupo", ver);
		}
		return VISTA_MIEMBROS;
	}

	// carga las ids de los miembros de un grupo, para enviarle un mensaje a
	// todos
	@RequestMapping(value = "/mensaje", method = RequestMethod.GET)
	public String getIdsIntegrantes(@RequestParam(value="id",defaultValue = "null") String id, 
			Model model, HttpSession session) {
		String resultado = "";
		List<MiembroGrupoExalumnos> miembros = null;
		try {
			miembros = moduloGrupo.getIntegrantesGrupo(Integer.parseInt(id));
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
		if (miembros == null) {
			return "error";
		}
		for (MiembroGrupoExalumnos m : miembros) {
			UsuarioAexa user = m.getUsuarioAexa();
			resultado += user.getId() + ",";
		}
		// quito la ultima coma del string
		resultado = StringUtils.chop(resultado);
		logger.info("mensaje enviado a las siguientes ids "+resultado);
		GrupoExAlumnos grupo = moduloGrupo.loadGrupo(Integer.parseInt(id));
		model.addAttribute("nombreUsuario", "Grupo "+grupo.getNombre());
		model.addAttribute("userId", resultado);
		return "buzon/enviarMensaje";
	}

}
