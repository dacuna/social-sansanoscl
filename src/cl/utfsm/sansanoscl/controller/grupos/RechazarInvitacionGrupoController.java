package cl.utfsm.sansanoscl.controller.grupos;

import java.util.Collections;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cl.utfsm.aexa.modulos.ModuloGrupoAexa;
import cl.utfsm.aexa.modulos.ModuloUsuarioAexa;
import cl.utfsm.aexa.personas.UsuarioAexa;

import cl.utfsm.aexa.redsocial.GrupoExAlumnos;
import cl.utfsm.aexa.redsocial.MiembroGrupoExalumnos;

@Controller
@RequestMapping(value = "/grupos/rechazarInvitacionGrupo")
public class RechazarInvitacionGrupoController {
	@Autowired
	ModuloGrupoAexa moduloGrupo;
	@Autowired
	ModuloUsuarioAexa moduloUsuario;

	private static final Logger logger = Logger
			.getLogger(RechazarInvitacionGrupoController.class);

	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody
	Map<String, ? extends Object> rechazar(@RequestBody String idGrupo,
			HttpSession session) {
		UsuarioAexa user = moduloUsuario.loadUsuarioAexaById((Long) session
				.getAttribute("userID"));
		GrupoExAlumnos grupo = new GrupoExAlumnos();
		MiembroGrupoExalumnos membresia = null;

		try {
			grupo = moduloGrupo.loadGrupo(Integer.parseInt(idGrupo));
		} catch (NumberFormatException e1) {
			e1.printStackTrace();
		}
		if (grupo == null)
			return Collections.singletonMap("error.exist", "true");

		try {
			membresia = moduloGrupo.usuarioPerteneceGrupo(user, grupo);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		if (membresia == null)
			return Collections.singletonMap("error.exist", "true");

		String nombreRolUsuario = membresia.getTipoRolMiembroExAlumnos()
				.getNombreUsuario();
		if (!nombreRolUsuario.equals("INVITADO"))
			return Collections.singletonMap("invitado.invalid",
					"Ya eres parte de este grupo.");
		else {
			try {
				moduloGrupo.eliminarMiembroGrupoExalumnos(membresia);
			} catch (Exception e) {
				logger.warn(e);
				return Collections.singletonMap("invitacion.error",
						"Ha ocurrido un error, intentalo mas tarde.");
			}
		}

		return Collections.singletonMap("flag", "true");

	}

}
