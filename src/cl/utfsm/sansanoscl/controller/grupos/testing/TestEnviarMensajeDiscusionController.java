package cl.utfsm.sansanoscl.controller.grupos.testing;

import static org.junit.Assert.assertEquals;

import java.util.LinkedHashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import cl.utfsm.sansanoscl.controller.grupos.EnviarMensajeDiscusionController;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-context.xml" })
@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional
public class TestEnviarMensajeDiscusionController {
	@Autowired
	EnviarMensajeDiscusionController controller;

	@Test
	public void todoOK() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("respuesta", "respuesta a discusion de prueba");
		data.put("discusionExAlumnos.id", "2");

		Map<String, ? extends Object> result = controller.create((Object) data, session);
		assertEquals(result.containsKey("flag"), true);
	}
	
	@Test
	public void noRespuesta() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		
		data.put("discusionExAlumnos.id", "2");

		Map<String, ? extends Object> result = controller.create((Object) data, session);
		assertEquals(result.containsKey("error.exist"), true);
	}	
	@Test
	public void noDiscusionId() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("respuesta", "respuesta a discusion de prueba");
		

		Map<String, ? extends Object> result = controller.create((Object) data, session);
		assertEquals(result.containsKey("error.exist"), true);
	}
	
	@Test
	public void respuestaVacia() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("respuesta", "");
		data.put("discusionExAlumnos.id", "2");

		Map<String, ? extends Object> result = controller.create((Object) data, session);
		assertEquals(result.containsKey("error.exist"), true);
	}
	@Test
	public void discusionIdVacia() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("respuesta", "respuesta a discusion de preuba");
		data.put("discusionExAlumnos.id", "");

		Map<String, ? extends Object> result = controller.create((Object) data, session);
		assertEquals(result.containsKey("error.exist"), true);
	}
	
	@Test
	public void discusionIdFalsa() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("respuesta", "respuesta a discusion de preuba");
		data.put("discusionExAlumnos.id", "3123");

		Map<String, ? extends Object> result = controller.create((Object) data, session);
		assertEquals(result.containsKey("error.exist"), true);
	}
	
	@Test
	public void discusionAjena() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 11795);

		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("respuesta", "respuesta a discusion ajena de preuba");
		data.put("discusionExAlumnos.id", "2");

		Map<String, ? extends Object> result = controller.create((Object) data, session);
		assertEquals(result.containsKey("error.exist"), true);
	}
}
