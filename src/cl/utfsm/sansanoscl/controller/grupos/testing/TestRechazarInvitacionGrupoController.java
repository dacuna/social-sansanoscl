package cl.utfsm.sansanoscl.controller.grupos.testing;

import static org.junit.Assert.assertEquals;

import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import cl.utfsm.sansanoscl.controller.grupos.RechazarInvitacionGrupoController;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-context.xml" })
@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional
public class TestRechazarInvitacionGrupoController {
	@Autowired
	RechazarInvitacionGrupoController controller;


	@Test
	public void RechazarGrupoExistenteTest() {
		//test para rechazar grupo que existe con invitacion existente (caso bonito :) )
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 11795);
		
		String idGrupo = "150034";
					
		Map<String, ? extends Object> result = controller.rechazar(idGrupo,session);
		assertEquals(result.containsKey("flag"), true);
	}
	
	@Test
	public void grupoInexistente() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID",1 );
		
		String idGrupo = "1302309312";
	
		Map<String, ? extends Object> result = controller.rechazar(idGrupo,session);
		assertEquals(true, result.containsKey("error.exist"));
	}
	
	@Test
	public void grupoNull() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID",1 );
		
		String idGrupo = null;
	
		Map<String, ? extends Object> result = controller.rechazar(idGrupo,session);
		assertEquals(true, result.containsKey("error.exist"));
	}	
	@Test
	public void grupoVacio() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID",1 );
		
		String idGrupo = "";
	
		Map<String, ? extends Object> result = controller.rechazar(idGrupo,session);
		assertEquals(true, result.containsKey("error.exist"));
	}
	
	@Test
	public void RechazarGrupoMiembroTest() {
		//test para rechazar solicitud donde el usuario ya es miembro
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID",18131 );
		
		String idGrupo = "150029";
	
		Map<String, ? extends Object> result = controller.rechazar(idGrupo,session);
		assertEquals(result.containsKey("invitado.invalid"), true);
	}

	
}
