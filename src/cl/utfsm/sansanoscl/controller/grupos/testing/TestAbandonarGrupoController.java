package cl.utfsm.sansanoscl.controller.grupos.testing;

import static org.junit.Assert.assertEquals;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ExtendedModelMap;

import cl.utfsm.sansanoscl.controller.grupos.AbandonarGrupoController;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-context.xml" })

@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional
public class TestAbandonarGrupoController {
	@Autowired
	AbandonarGrupoController controller;

	@Test
	public void todoOK() {
		MockHttpSession session = new MockHttpSession();
		MockHttpServletRequest request = new MockHttpServletRequest();

		request.addParameter("idgrupo", "4");

		session.putValue("userID", 1);

		//Assert.assertEquals("abandonar", controller.showForm(new ExtendedModelMap(), session, request));
	}

	@Test
	public void abandonarConNuevoAdminTest() {
		
		HttpServletResponse response = new MockHttpServletResponse();
		
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
	
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();
		data.put("idgrupo", "200005");
		data.put("idUsuarioNuevoAdmin", "11795");
		
		
	
		Map<String, ? extends Object> result = controller.abandonarConNuevoAdmin((Object)data, response, session);
		assertEquals(true, result.containsKey("flag"));
	}
	@Test
	public void ErrorUsuarioNuevoAdminabandonarConNuevoAdminTest() {
		
		HttpServletResponse response = new MockHttpServletResponse();
		
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();
		data.put("idgrupo", "4");
		data.put("idUsuarioNuevoAdmin", "22438");
	
		Map<String, ? extends Object> result = controller.abandonarConNuevoAdmin((Object)data,
				response, session);
		assertEquals(result.containsKey("error.exist"), true);
	}
	
	
	
	@Test
	public void abandonarConEliminacionGrupoTest() {
		
		HttpServletResponse response = new MockHttpServletResponse();
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		
		Map<String, ? extends Object> result = controller.abandonarGrupo("50004",
				response, session);
		assertEquals(result.containsKey("error.exist"), true);
	}
	@Test
	public void Error1abandonarConEliminacionGrupoTest() {
		//usuario no es admin de grupo
		HttpServletResponse response = new MockHttpServletResponse();
		
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 22429);

		Map<String, ? extends Object> result = controller.abandonarGrupo("50004",
				response, session);
		assertEquals(result.containsKey("error.exist"), true);
	}
	@Test
	public void Error2abandonarConEliminacionGrupoTest() {
		//usuario no es admin de grupo
		HttpServletResponse response = new MockHttpServletResponse();
		
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);		
	
		Map<String, ? extends Object> result = controller.abandonarGrupo("50004",
				response, session);
		assertEquals(result.containsKey("error.exist"), true);
	}
	
	
	
}
