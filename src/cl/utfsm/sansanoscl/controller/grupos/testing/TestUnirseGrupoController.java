package cl.utfsm.sansanoscl.controller.grupos.testing;

import static org.junit.Assert.assertEquals;

import java.util.LinkedHashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import cl.utfsm.sansanoscl.controller.grupos.UnirseGrupoController;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-context.xml" })
@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional
public class TestUnirseGrupoController {
	@Autowired
	UnirseGrupoController controller;

	@Test
	public void todoOK() {

		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 11795);

		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("grupoExAlumnos.id", "150029");
		data.put("recibirMensajesDiscusiones", "true");
		data.put("recibirMensajesEventos", "true");

		Map<String, ? extends Object> result = controller.create((Object) data, session);
		// recordar que en el controlador devuelve un flag si todo anduvo bien
		assertEquals(true, result.containsKey("flag"));
	}
	
	/** mandar un grupo falso no tiene sentido.
	 * esta permitido no enviar los otros parametros **/
}
