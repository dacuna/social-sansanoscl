package cl.utfsm.sansanoscl.controller.grupos.testing;

import static org.junit.Assert.assertEquals;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import cl.utfsm.sansanoscl.controller.grupos.CrearGrupoController;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-context.xml" })
@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional
public class TestCrearGrupoController {
	@Autowired
	CrearGrupoController controller;

	@Test
	public void todoOK() {
		// se intenta testear el requestpost del controlador (recordar que es
		// con json)
		HttpServletResponse response = new MockHttpServletResponse();
		// Se debe inicializar una sesion, el controlador necesita conocer el
		// userID
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1); // se usa el usuario con id 1 de prueba

		// los objectos recibidos por JSON son LinkedHashMap
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("nombre", "Mi Grupo de prueba");
		data.put("descripcion", "Este es un grupo de prueba");
		data.put("foto", "img.jpg");
		data.put("tipoGrupoExAlumnos.codigo", "1");
		data.put("permisos", "1");

		// ahora testeo el resultado:
		Map<String, ? extends Object> result = controller.create((Object) data,
				response, session);
		// recordar que en el controlador se esta devolviendo un map con la id
		// si es que todo funciono correctamente
		assertEquals(result.containsKey("flag"), true);
	}
	
	@Test
	public void noNombre() {
		HttpServletResponse response = new MockHttpServletResponse();
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1); // se usa el usuario con id 1 de prueba

		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		
		data.put("descripcion", "Este es un grupo de prueba");
		data.put("foto", "img.jpg");
		data.put("tipoGrupoExAlumnos.codigo", "1");
		data.put("permisos", "1");

		Map<String, ? extends Object> result = controller.create((Object) data,
				response, session);

		assertEquals(true, result.containsKey("error.exist"));
	}
	@Test
	public void noDescripcion() {
		HttpServletResponse response = new MockHttpServletResponse();
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1); // se usa el usuario con id 1 de prueba

		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("nombre", "Mi Grupo de prueba");
		
		data.put("foto", "img.jpg");
		data.put("tipoGrupoExAlumnos.codigo", "1");
		data.put("permisos", "1");

		Map<String, ? extends Object> result = controller.create((Object) data,
				response, session);

		assertEquals(true, result.containsKey("flag"));
	}
	@Test
	public void noFoto() {
		HttpServletResponse response = new MockHttpServletResponse();
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1); // se usa el usuario con id 1 de prueba

		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("nombre", "Mi Grupo de prueba");
		data.put("descripcion", "Este es un grupo de prueba");
		data.put("tipoGrupoExAlumnos.codigo", "1");
		data.put("permisos", "1");

		Map<String, ? extends Object> result = controller.create((Object) data,
				response, session);

		assertEquals(true, result.containsKey("flag"));
	}
	@Test
	public void noTipoGrupo() {
		HttpServletResponse response = new MockHttpServletResponse();
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1); // se usa el usuario con id 1 de prueba

		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("nombre", "Mi Grupo de prueba");
		data.put("descripcion", "Este es un grupo de prueba");
		data.put("foto", "img.jpg");
		data.put("permisos", "1");

		Map<String, ? extends Object> result = controller.create((Object) data,
				response, session);

		assertEquals(true, result.containsKey("error.exist"));
	}
	@Test
	public void noPermisos() {
		HttpServletResponse response = new MockHttpServletResponse();
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1); // se usa el usuario con id 1 de prueba

		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("nombre", "Mi Grupo de prueba");
		data.put("descripcion", "Este es un grupo de prueba");
		data.put("foto", "img.jpg");
		data.put("tipoGrupoExAlumnos.codigo", "1");
		

		Map<String, ? extends Object> result = controller.create((Object) data,
				response, session);

		assertEquals(true, result.containsKey("error.exist"));
	}
	
	@Test
	public void nombreVacio() {
		HttpServletResponse response = new MockHttpServletResponse();
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1); // se usa el usuario con id 1 de prueba

		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("nombre", "");
		data.put("descripcion", "Este es un grupo de prueba");
		data.put("foto", "img.jpg");
		data.put("tipoGrupoExAlumnos.codigo", "1");
		data.put("permisos", "1");

		Map<String, ? extends Object> result = controller.create((Object) data,
				response, session);

		assertEquals(true, result.containsKey("error.exist"));
	}
	@Test
	public void descripcionVacio() {
		HttpServletResponse response = new MockHttpServletResponse();
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1); // se usa el usuario con id 1 de prueba

		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("nombre", "Mi Grupo de prueba");
		data.put("descripcion", "");
		data.put("foto", "img.jpg");
		data.put("tipoGrupoExAlumnos.codigo", "1");
		data.put("permisos", "1");

		Map<String, ? extends Object> result = controller.create((Object) data,
				response, session);

		assertEquals(result.containsKey("flag"), true);
	}
	@Test
	public void fotoVacio() {
		HttpServletResponse response = new MockHttpServletResponse();
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1); // se usa el usuario con id 1 de prueba

		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("nombre", "Mi Grupo de prueba");
		data.put("descripcion", "Este es un grupo de prueba");
		data.put("foto", "");
		data.put("tipoGrupoExAlumnos.codigo", "1");
		data.put("permisos", "1");

		Map<String, ? extends Object> result = controller.create((Object) data,
				response, session);

		assertEquals(result.containsKey("flag"), true);
	}
	@Test
	public void tipoGrupoVacio() {
		HttpServletResponse response = new MockHttpServletResponse();
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1); // se usa el usuario con id 1 de prueba

		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("nombre", "Mi Grupo de prueba");
		data.put("descripcion", "Este es un grupo de prueba");
		data.put("foto", "img.jpg");
		data.put("tipoGrupoExAlumnos.codigo", "");
		data.put("permisos", "1");

		Map<String, ? extends Object> result = controller.create((Object) data,
				response, session);

		assertEquals(true, result.containsKey("error.exist"));
	}
	@Test
	public void permisosVacio() {
		HttpServletResponse response = new MockHttpServletResponse();
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1); // se usa el usuario con id 1 de prueba

		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("nombre", "Mi Grupo de prueba");
		data.put("descripcion", "Este es un grupo de prueba");
		data.put("foto", "img.jpg");
		data.put("tipoGrupoExAlumnos.codigo", "1");
		data.put("permisos", "");

		Map<String, ? extends Object> result = controller.create((Object) data,
				response, session);

		assertEquals(true, result.containsKey("error.exist"));
	}
}
