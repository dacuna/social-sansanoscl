package cl.utfsm.sansanoscl.controller.grupos.testing;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;

import cl.utfsm.sansanoscl.controller.grupos.CargarPerfilGrupoController;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-context.xml" })
@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional
public class TestCargarPerfilGrupoController {
	@Autowired
	CargarPerfilGrupoController controller;

	@Test
	public void createTest() {
		// Se debe inicializar una sesion, el controlador necesita conocer el
		// userID
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1); // se usa el usuario con id 1 de prueba

		// ahora testeo el resultado:
		String result = controller.perfilGrupo("6",new ExtendedModelMap(), session);
		// recordar que en el controlador se esta devolviendo un map con la id
		// si es que todo funciono correctamente
		assertEquals("/grupos/perfilGrupo", result);
	}
	
	@Test
	public void grupoNull() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1); 
		Model model =  new ExtendedModelMap();
		
		controller.perfilGrupo(null, model, session);
		assertEquals(true, model.containsAttribute("grupo.invalid"));
	}
	@Test
	public void grupoVacio() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1); 
		Model model =  new ExtendedModelMap();
		
		controller.perfilGrupo("", model, session);
		assertEquals(true, model.containsAttribute("grupo.invalid"));
	}
	@Test
	public void grupoNotNumber() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1); 
		Model model =  new ExtendedModelMap();
		
		controller.perfilGrupo("Deportes", model, session);
		assertEquals(true, model.containsAttribute("grupo.invalid"));
	}
	
	
	@Test
	public void verMiebrosTest() {
		// Se debe inicializar una sesion, el controlador necesita conocer el
		// userID
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1); // se usa el usuario con id 1 de prueba

		// ahora testeo el resultado:
		String result = controller.verMiembros("6",new ExtendedModelMap(), session);
		// recordar que en el controlador se esta devolviendo un map con la id
		// si es que todo funciono correctamente
		assertEquals(result, "/grupos/miembrosGrupo");
	}
}
