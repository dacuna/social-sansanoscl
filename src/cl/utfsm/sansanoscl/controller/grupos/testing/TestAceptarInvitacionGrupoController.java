package cl.utfsm.sansanoscl.controller.grupos.testing;

import static org.junit.Assert.assertEquals;

import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import cl.utfsm.sansanoscl.controller.grupos.AceptarInvitacionGrupoController;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-context.xml" })
@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional
public class TestAceptarInvitacionGrupoController {
	@Autowired
	AceptarInvitacionGrupoController controller;


	@Test
	public void AceptarGrupoExistenteTest() {
		//test para aceptar grupo que existe con invitacion existente (caso bonito :) )
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 11795);
		
		String idGrupo = "150031";
					
		Map<String, ? extends Object> result = controller.aceptar(idGrupo,session);
		assertEquals(true, result.containsKey("flag"));
	}
	
	@Test
	public void AceptarGrupoMiembroTest() {
		//test para aceptar solicitud donde el usuario ya es miembro
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID",1 );
		
		String idGrupo = "150030";
	
		Map<String, ? extends Object> result = controller.aceptar(idGrupo,session);
		assertEquals(result.containsKey("flag"), true);
	}
	
	@Test
	public void AceptarGrupoMiembroNoInvitadoTest() {
		//test para aceptar solicitud donde no existe membresia de ipo invitado en bd
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID",12583 );
		
		String idGrupo = "150030";
	
		Map<String, ? extends Object> result = controller.aceptar(idGrupo,session);
		assertEquals(result.containsKey("errorMembresiaInvitadoNoExiste"), true);
	}
	
}
