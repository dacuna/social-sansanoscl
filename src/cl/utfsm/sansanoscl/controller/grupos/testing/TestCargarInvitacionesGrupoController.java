package cl.utfsm.sansanoscl.controller.grupos.testing;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ExtendedModelMap;

import cl.utfsm.sansanoscl.controller.grupos.CargarInvitacionesGrupoController;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-context.xml" })
@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional
public class TestCargarInvitacionesGrupoController {
	@Autowired
	CargarInvitacionesGrupoController controller;

	@Test
	public void todoOK() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		String result = controller.showForm(new ExtendedModelMap(), session);
		assertEquals(result, "/grupos/formularios/verInvitacionesGrupo");
	}
}
