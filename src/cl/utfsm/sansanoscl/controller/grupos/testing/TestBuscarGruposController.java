package cl.utfsm.sansanoscl.controller.grupos.testing;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ExtendedModelMap;

import cl.utfsm.sansanoscl.controller.grupos.BuscarGruposController;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-context.xml" })
@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional
public class TestBuscarGruposController {
	@Autowired
	BuscarGruposController controller;

	@Test
	public void buscarTest() {

		MockHttpServletRequest request = new MockHttpServletRequest();
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 11795);

		request.addParameter("cadena", "Entretencion");

		String result = controller.buscar("Deporte",new ExtendedModelMap(), request,
				session);
		assertEquals(result, "/grupos/buscar");
	}
	
	@Test
	public void noCadena() {

		MockHttpServletRequest request = new MockHttpServletRequest();
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 11795);

		request.addParameter("cadena", "Entretencion");

		String result = controller.buscar("Deporte",new ExtendedModelMap(), request,
				session);
		assertEquals(result, "/grupos/buscar");
	}
	
	@Test
	public void cadenaVacia() {

		MockHttpServletRequest request = new MockHttpServletRequest();
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 11795);

		request.addParameter("cadena", "");

		String result = controller.buscar("Deporte",new ExtendedModelMap(), request,
				session);
		assertEquals(result, "/grupos/buscar");
	}
}
