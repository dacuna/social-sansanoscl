package cl.utfsm.sansanoscl.controller.grupos.testing;

import static org.junit.Assert.assertEquals;
import java.util.LinkedHashMap;
import java.util.Map;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ExtendedModelMap;

import cl.utfsm.sansanoscl.controller.grupos.InvitarContactosGrupoController;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-context.xml" })
@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional
public class TestInvitarContactosGrupoController {
	@Autowired
	InvitarContactosGrupoController controller;
	
	
	public void invitarUsuarioNuevoTest() {
		// Se debe inicializar una sesion, el controlador necesita conocer el
		// userID
	
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1); // se usa el usuario con id 1 de prueba
	
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();
				
		String invitados = "13102,18131,18444,22438";
		data.put("idgrupo", "4");
		data.put("invitados", invitados);
		// ahora testeo el resultado:
		Map<String, ? extends Object> result = controller.invitar(data, session);
		// recordar que en el controlador se esta devolviendo un map con la id
		// si es que todo funciono correctamente
		assertEquals(true, result.containsKey("flag"));
	}
	
	@Test
	public void invitarContactos() {	
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1); 
	
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();
				
		String invitados = "13102,18131,18444,22438";
		data.put("invitados", invitados);
		String result = controller.invitarContactos("4", new ExtendedModelMap(), session);
		assertEquals("/grupos/formularios/invitarContactos", result);
	}
	
	@Test
	public void invitarContactoInexistente() {
	
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1); // se usa el usuario con id 1 de prueba
	
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();
				
		String invitados = "25153263464";
		data.put("invitados", invitados);
		String result = controller.invitarContactos("4", new ExtendedModelMap(), session);
		assertEquals("/grupos/formularios/invitarContactos", result);
	}

}
