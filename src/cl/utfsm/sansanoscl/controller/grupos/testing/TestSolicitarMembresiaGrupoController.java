package cl.utfsm.sansanoscl.controller.grupos.testing;

import static org.junit.Assert.assertEquals;

import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import cl.utfsm.sansanoscl.controller.grupos.SolicitarMembresiaGrupoController;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-context.xml" })
@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional
public class TestSolicitarMembresiaGrupoController {
	@Autowired
	SolicitarMembresiaGrupoController controller;

	@Test
	public void todoOK() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 11795);
		Map<String, ? extends Object> result = controller.solicitar("X150035X" , session);
		assertEquals(result.containsKey("flag"), true);
	}
	
	@Test
	public void noGrupoId() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 11795);
		Map<String, ? extends Object> result = controller.solicitar(null , session);
		assertEquals(true, result.containsKey("error.exist"));
	}
	@Test
	public void grupoVacio() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 11795);
		Map<String, ? extends Object> result = controller.solicitar("" , session);
		assertEquals(true, result.containsKey("error.exist"));
	}
	
	@Test
	public void grupoFalso() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 11795);
		Map<String, ? extends Object> result = controller.solicitar("2318931232131" , session);
		assertEquals(true, result.containsKey("error.exist"));
	}
	
	@Test
	public void solicitudExistente() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 11795);
		Map<String, ? extends Object> result = controller.solicitar("X200005X", session);
		assertEquals(true, result.containsKey("flag"));
	}
	@Test
	public void grupoYaPertenece() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 11795);
		Map<String, ? extends Object> result = controller.solicitar("X150030X", session);
		assertEquals(true, result.containsKey("flag"));
	}
}
