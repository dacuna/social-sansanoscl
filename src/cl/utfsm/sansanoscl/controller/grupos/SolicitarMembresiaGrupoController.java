package cl.utfsm.sansanoscl.controller.grupos;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cl.utfsm.aexa.modulos.ModuloGrupoAexa;
import cl.utfsm.aexa.modulos.ModuloUsuarioAexa;
import cl.utfsm.aexa.personas.UsuarioAexa;

import cl.utfsm.aexa.redsocial.GrupoExAlumnos;
import cl.utfsm.aexa.redsocial.MiembroGrupoExalumnos;
import cl.utfsm.sansanoscl.validators.GrupoExAlumnoValidator;

@Controller
@RequestMapping(value = "/grupos")
public class SolicitarMembresiaGrupoController {
	@Autowired
	ModuloGrupoAexa moduloGrupo;
	@Autowired
	ModuloUsuarioAexa userService;
	@Autowired
	GrupoExAlumnoValidator grupoExAlumnoValidator;

	@SuppressWarnings("unused")
	private static final Logger logger = Logger
			.getLogger(SolicitarMembresiaGrupoController.class);

	@RequestMapping(method = RequestMethod.POST, value = "/solicitar")
	public @ResponseBody
	Map<String, ? extends Object> solicitar(@RequestBody String idGrupo,
			HttpSession session) {

		UsuarioAexa user = userService.loadUsuarioAexaById((Long) session
				.getAttribute("userID"));
		GrupoExAlumnos grupo = new GrupoExAlumnos();
		MiembroGrupoExalumnos membresia = null;
		Map<String, String> errores = new HashMap<String, String>();
		
		if(idGrupo == null || idGrupo.equals(""))
			return Collections.singletonMap("error.exist", "true");
		
		idGrupo = idGrupo.substring(1, idGrupo.length()-1);
		System.out.println("HOLA "+ idGrupo);
		/*if (!ModuloUtilidades.checkIfNumber(idGrupo.substring(1, idGrupo.length()-1)))
			return Collections.singletonMap("error.exist", "true");
		System.out.println("HOLA "+ idGrupo);*/
		try {
			grupo = moduloGrupo.loadGrupo(Integer.parseInt(idGrupo));
		} catch (Exception e1) {
			e1.printStackTrace();
			errores.put("errores.exist", "true");
			return Collections.singletonMap("error.exist", "true");
		}
		if (grupo == null)
			return Collections.singletonMap("error.exist", "true");

				
		if (!errores.isEmpty()) {
			errores.put("errores.exist", "true");
			return Collections.singletonMap("error.exist", "true");
		}
		
		try {
			membresia = moduloGrupo.getMiembroGrupoPorUsuario(user.getId(), grupo.getId());
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		if(membresia == null)
		{
			membresia = new MiembroGrupoExalumnos();
			try {
				membresia.setTipoRolMiembroExAlumnos(moduloGrupo
						.getTipoRolesMiembrosExAlumnos("SOLICITANTE"));
				membresia.setUsuarioAexa(user);
				membresia.setGrupoExAlumnos(grupo);
				membresia.setFechaModificacion(new Date());
				moduloGrupo.insertarMiembroGrupoExAlumnos(membresia);
			} catch (Exception e) {
				e.printStackTrace();
				errores.put("error.exist", "true");
				return Collections.singletonMap("error.exist", "true");
			}
		
		}
		return Collections.singletonMap("flag", "true");

	}

}
