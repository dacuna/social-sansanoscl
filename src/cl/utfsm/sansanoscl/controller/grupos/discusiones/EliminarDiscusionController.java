package cl.utfsm.sansanoscl.controller.grupos.discusiones;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cl.utfsm.aexa.modulos.ModuloGrupoAexa;
import cl.utfsm.aexa.modulos.ModuloUsuarioAexa;
import cl.utfsm.aexa.personas.UsuarioAexa;
import cl.utfsm.aexa.redsocial.DiscusionExAlumnos;
import cl.utfsm.aexa.redsocial.MiembroGrupoExalumnos;
import cl.utfsm.sansanoscl.validators.DiscusionExAlumnoValidator;

@Controller
@RequestMapping(value = "/grupos/discusiones/eliminarDiscusion")
public class EliminarDiscusionController {
	@Autowired
	ModuloGrupoAexa moduloGrupo;
	@Autowired
	ModuloUsuarioAexa moduloUsuario;
	@Autowired
	DiscusionExAlumnoValidator discusionExAlumnoValidator;

	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody
	Map<String, ? extends Object> delete(@RequestBody String idDiscusion,
			HttpSession session) {
		UsuarioAexa user = moduloUsuario.loadUsuarioAexaById((Long) session.getAttribute("userID"));
		DiscusionExAlumnos discusion = null;
		Integer discusionId = Integer.parseInt(idDiscusion);
		Map<String, String> errores = new HashMap<String, String>();
		MiembroGrupoExalumnos miembro = null;

		try {
			discusion = moduloGrupo.getDiscusionById(discusionId);
		} catch (Exception e1) {
			e1.printStackTrace();
			errores.put("error.exist", "true");
		}
		if (discusion == null)
			return Collections.singletonMap("error.exist", "true");

		// se chequea que el usuario posea los permisos para eliminar la
		// discusion
		// casos: puede eliminar si es que el creo la discusion o si es
		// administrador del grupo
		try {
			miembro = moduloGrupo.getMiembroGrupoPorUsuario(user.getId(),
					discusion.getGrupoExAlumnos().getId());
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		if (miembro == null)
			return Collections.singletonMap("error.exist", "true");

		System.out.println(discusion.getAutor().getId());
		System.out.println(user.getId());
		System.out.println(discusion.getAutor().getId().compareTo(user.getId()));
		if(discusion.getAutor().getId().compareTo(user.getId()) != 0){
			if(miembro.getTipoRolMiembroExAlumnos().getNombreUsuario().compareTo("ADMIN") != 0)
				return Collections.singletonMap("permission.error", "No tienes los permisos para eliminar esta discusion.");
		}

		// todo correcto, se elimina la discusion
		try {
			moduloGrupo.eliminarDiscusion(discusion);
		} catch (Exception e) {
			e.printStackTrace();
			return Collections.singletonMap("eliminarDiscusion.error", "true");
		}

		return Collections.singletonMap("flag", "true");
	}

}
