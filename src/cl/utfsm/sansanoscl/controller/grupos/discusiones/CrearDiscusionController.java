package cl.utfsm.sansanoscl.controller.grupos.discusiones;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cl.utfsm.aexa.modulos.ModuloGrupoAexa;
import cl.utfsm.aexa.modulos.ModuloUsuarioAexa;
import cl.utfsm.aexa.personas.UsuarioAexa;
import cl.utfsm.aexa.redsocial.DiscusionExAlumnos;
import cl.utfsm.aexa.redsocial.GrupoExAlumnos;
import cl.utfsm.sansanoscl.utils.JsonDeserializer;
import cl.utfsm.sansanoscl.utils.ModuloUtilidades;
import cl.utfsm.sansanoscl.validators.DiscusionExAlumnoValidator;
import cl.utfsm.sansanoscl.validators.GrupoExAlumnoValidator;

@Controller
@RequestMapping(value = "/grupos/crearDiscusion")
public class CrearDiscusionController {
	@Autowired ModuloGrupoAexa moduloGrupo;
	@Autowired ModuloUsuarioAexa moduloUsuario;
	@Autowired GrupoExAlumnoValidator grupoExAlumnoValidator;
	@Autowired DiscusionExAlumnoValidator discusionExAlumnoValidator;

	private final String VISTA_FORM = "/grupos/discusiones/crearDiscusion";
	private static final Logger logger = Logger.getLogger(CrearDiscusionController.class);

	@RequestMapping(method = RequestMethod.GET)
	public String showForm(@RequestParam(value="id", defaultValue = "null") String id,
			Model model, HttpSession session) {
		UsuarioAexa user = moduloUsuario.loadUsuarioAexaById((Long) session.getAttribute("userID"));
		GrupoExAlumnos grupo = null;
		BindingResult result = new BeanPropertyBindingResult(grupo, "");
		Map<String, String> errores = new HashMap<String, String>();

		try {
			grupo = moduloGrupo.loadGrupo(Integer.parseInt(id));
		} catch (NumberFormatException e) {
			e.printStackTrace();
			model.addAttribute("error", "primero");
			return VISTA_FORM;

		}
		if (grupo == null) {
			model.addAttribute("error", "segundo");
			return VISTA_FORM;
		}
		grupoExAlumnoValidator.validateUsuarioPerteneceGrupo(grupo, user, result);
		errores = ModuloUtilidades.parseErrors(result);
		if (!errores.isEmpty()) {
			errores.put("errores.exist", "true");
			return VISTA_FORM;
		}
		
		model.addAttribute("grupo", grupo);

		return VISTA_FORM;
	}

	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody
	Map<String, ? extends Object> create(@RequestBody Object requested,
			HttpSession session) {
		DiscusionExAlumnos discusion = new DiscusionExAlumnos();
		discusion = (DiscusionExAlumnos) JsonDeserializer.parsingClass(
				discusion, requested);
		UsuarioAexa user = moduloUsuario.loadUsuarioAexaById((Long) session
				.getAttribute("userID"));
		BindingResult result = new BeanPropertyBindingResult(discusion, "");
		Map<String, String> errores = new HashMap<String, String>();
		
		try {
			if(moduloGrupo.usuarioPerteneceGrupo(user, discusion.getGrupoExAlumnos()) == null)	
				return Collections.singletonMap("errores.exist", "true");
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		grupoExAlumnoValidator.validateUsuarioPerteneceGrupo(discusion.getGrupoExAlumnos(), user, null);
		discusionExAlumnoValidator.validate(discusion, result);
		errores = ModuloUtilidades.parseErrors(result);
		if (!errores.isEmpty()) {
			errores.put("errores.exist", "true");
			return errores;
		}

		discusion.setAutor(user);
		discusion.setFechaModificacion(new Date());
		discusion.setMensajes(1); // <-- el mensaje inicial

		try {
			moduloGrupo.insertarDiscusion(discusion);
		} catch (Exception e) {
			e.printStackTrace();
			logger.warn(e);
			return Collections.singletonMap("errores.exist", "true");
		}

		return Collections.singletonMap("flag", "true");
	}

}
