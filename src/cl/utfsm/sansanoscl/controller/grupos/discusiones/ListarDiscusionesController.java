package cl.utfsm.sansanoscl.controller.grupos.discusiones;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import cl.utfsm.aexa.modulos.ModuloGrupoAexa;
import cl.utfsm.aexa.modulos.ModuloUsuarioAexa;
import cl.utfsm.aexa.personas.UsuarioAexa;
import cl.utfsm.aexa.redsocial.DiscusionExAlumnos;
import cl.utfsm.aexa.redsocial.GrupoExAlumnos;
import cl.utfsm.aexa.redsocial.MiembroGrupoExalumnos;
import cl.utfsm.sansanoscl.utils.ModuloUtilidades;
import cl.utfsm.sansanoscl.validators.GrupoExAlumnoValidator;

@Controller
@RequestMapping(value = "/grupos/discusiones")
public class ListarDiscusionesController {
	@Autowired
	ModuloUsuarioAexa moduloUsuario;
	@Autowired
	ModuloGrupoAexa moduloGrupo;
	@Autowired
	GrupoExAlumnoValidator grupoExAlumnoValidator;
	private final String VISTA_FORM = "/grupos/discusiones/discusiones";
	private final String VISTA_POST = "/grupos/discusiones/posts";
	private static final Logger logger = Logger
			.getLogger(ListarDiscusionesController.class);
	@RequestMapping(method = RequestMethod.GET)
	public String showDiscusiones(
			@RequestParam(value="gid", defaultValue = "null") String gid, Model model,
			HttpSession session) {
		UsuarioAexa user = moduloUsuario.loadUsuarioAexaById((Long) session
				.getAttribute("userID"));
		GrupoExAlumnos grupo = null;
		List<DiscusionExAlumnos> discusiones = null;
		BindingResult result = new BeanPropertyBindingResult(grupo, "");
		Map<String, String> errores = new HashMap<String, String>();
	
		try {
			grupo = moduloGrupo.loadGrupo(Integer.parseInt(gid));
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error.exist", "true");
		}
		if (grupo == null) {
			model.addAttribute("error.exist", "true");
			return VISTA_FORM;
		}
		MiembroGrupoExalumnos miembro = moduloGrupo.usuarioPerteneceGrupo(user,
				grupo);
		model.addAttribute("miembro", miembro);
		if(miembro.getTipoRolMiembroExAlumnos().getNombreUsuario().toString().equals("SOLICITANTE"))
			model.addAttribute("solicitante", "true");
		model.addAttribute("ModuloUtilidades", new ModuloUtilidades());
		model.addAttribute("grupo", grupo);
		
		try{
			grupoExAlumnoValidator.validateUsuarioPerteneceGrupo(grupo, user,
					result);
		}catch(IllegalStateException e){
			e.printStackTrace();
			model.addAttribute("error.exist", "true");
			return VISTA_FORM;
		}
		errores = ModuloUtilidades.parseErrors(result);
		if (!errores.isEmpty()) {
			model.addAttribute("error.exist", "true");
			return VISTA_FORM;
		}

		try {
			discusiones = moduloGrupo.getDiscusion(grupo.getId());
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (discusiones == null) {
			model.addAttribute("error.exist", "true");
			return VISTA_FORM;
		}
		System.out.println("TAMANIO DE DISCUSIONES: "+discusiones.size());
		model.addAttribute("discusiones", discusiones);
		model.addAttribute("user", user);
		model.addAttribute("rolUsuario", moduloGrupo.getRolUsuarioEnGrupo(user.getId(), grupo.getId()));
		model.addAttribute("ModuloUtilidades", new ModuloUtilidades());

		return VISTA_FORM;
	}

	@RequestMapping(method = RequestMethod.GET, value = "ver")
	public String verPostDiscusiones(
			@RequestParam(value = "id", defaultValue = "null") String id,
			Model model, HttpSession session) {
		DiscusionExAlumnos ver = new DiscusionExAlumnos();
		UsuarioAexa user = moduloUsuario.loadUsuarioAexaById((Long) session
				.getAttribute("userID"));
		BindingResult result = new BeanPropertyBindingResult(ver, "");
		Map<String, String> errores = new HashMap<String, String>();

		try {
			ver = moduloGrupo.getDiscusionById(Integer.parseInt(id));
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (ver.getId() == null) {
			model.addAttribute("discusionNoExiste", "true");
			model.addAttribute("error.exist", "true");
			return VISTA_POST;
		}
		try {
			grupoExAlumnoValidator.validateUsuarioPerteneceGrupo(
					ver.getGrupoExAlumnos(), user, result);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error.exist", "true");
		}
		errores = ModuloUtilidades.parseErrors(result);
		if (!errores.isEmpty()) {
			model.addAttribute("error.exist", "true");
			return VISTA_POST;
		}
		model.addAttribute("grupo", ver.getGrupoExAlumnos());
		model.addAttribute("ver", ver);
		model.addAttribute("posts", ver.getPosts());
		model.addAttribute("membresia", true);
		logger.info("GRUPO ID:" + ver.getGrupoExAlumnos().getId());
		model.addAttribute("ModuloUtilidades", new ModuloUtilidades());
	
		return VISTA_POST;
	}
}
