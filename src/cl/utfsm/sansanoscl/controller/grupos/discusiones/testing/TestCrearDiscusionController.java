package cl.utfsm.sansanoscl.controller.grupos.discusiones.testing;

import static org.junit.Assert.*;

import java.util.LinkedHashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import cl.utfsm.sansanoscl.controller.grupos.discusiones.CrearDiscusionController;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-context.xml" })
@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional
public class TestCrearDiscusionController {
	@Autowired
	CrearDiscusionController controller;
	
	@Test
	public void todoOK() {
		// Se debe inicializar una sesion, el controlador necesita conocer el
		// userID
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1); // se usa el usuario con id 1 de prueba

		// los objectos recibidos por JSON son LinkedHashMap
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("grupoExAlumnos.id", "150030");
		data.put("titulo", "Discusion de pruebaa");
		data.put("resumen", "Esta discusion es sobre algo interesante");
		data.put("detalle", "Hola amigos les quiero preguntar si saben de algo que sea realmente interesante?");

		// ahora testeo el resultado:
		Map<String, ? extends Object> result = controller.create((Object) data, session);
		
		assertEquals(true, result.containsKey("flag"));
		}
	
	@Test
	public void noTitulo() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("grupoExAlumnos.id", "150030");
		
		data.put("resumen", "Esta discusion es sobre algo interesante");
		data.put("detalle", "Hola amigos les quiero preguntar si saben de algo que sea realmente interesante?");
		Map<String, ? extends Object> result = controller.create((Object) data, session);
		assertEquals(true, result.containsKey("errores.exist"));
		}
	@Test
	public void noResumen() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("grupoExAlumnos.id", "150030");
		data.put("titulo", "discusion en grupo inexistente");
		
		data.put("detalle", "Hola amigos les quiero preguntar si saben de algo que sea realmente interesante?");
		Map<String, ? extends Object> result = controller.create((Object) data, session);
		assertEquals(true, result.containsKey("flag"));
		}
	@Test
	public void noDetalle() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("grupoExAlumnos.id", "150030");
		data.put("titulo", "discusion en grupo inexistente");
		data.put("resumen", "Esta discusion es sobre algo interesante");

		Map<String, ? extends Object> result = controller.create((Object) data, session);
		assertEquals(true, result.containsKey("errores.exist"));
		}
	
	@Test
	public void tituloVacio() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("grupoExAlumnos.id", "150030");
		data.put("titulo", "");
		data.put("resumen", "Esta discusion es sobre algo interesante");
		data.put("detalle", "Hola amigos les quiero preguntar si saben de algo que sea realmente interesante?");

		Map<String, ? extends Object> result = controller.create((Object) data, session);
		assertEquals(true, result.containsKey("errores.exist"));
		}
	@Test
	public void resumenVacio() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("grupoExAlumnos.id", "150030");
		data.put("titulo", "discusion en grupo inexistente");
		data.put("resumen", "");
		data.put("detalle", "Hola amigos les quiero preguntar si saben de algo que sea realmente interesante?");

		Map<String, ? extends Object> result = controller.create((Object) data, session);
		assertEquals(true, result.containsKey("flag"));
		}
	@Test
	public void detalleVacio() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("grupoExAlumnos.id", "150030");
		data.put("titulo", "discusion en grupo inexistente");
		data.put("resumen", "Esta discusion es sobre algo interesante");
		data.put("detalle", "");

		Map<String, ? extends Object> result = controller.create((Object) data, session);
		assertEquals(true, result.containsKey("errores.exist"));
		}
	
	@Test
	public void grupoFalso() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("grupoExAlumnos.id", "123812093");
		data.put("titulo", "discusion en grupo inexistente");
		data.put("resumen", "Esta discusion es sobre algo interesante");
		data.put("detalle", "Hola amigos les quiero preguntar si saben de algo que sea realmente interesante?");

		Map<String, ? extends Object> result = controller.create((Object) data, session);
		assertEquals(true, result.containsKey("errores.exist"));
		}
	
	@Test
	public void grupoAjeno() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("grupoExAlumnos.id", "150031");
		data.put("titulo", "discusion en grupo AJENO");
		data.put("resumen", "Esta discusion es sobre algo interesante");
		data.put("detalle", "Hola amigos les quiero preguntar si saben de algo que sea realmente interesante?");

		Map<String, ? extends Object> result = controller.create((Object) data, session);
		assertEquals(true, result.containsKey("errores.exist"));
		}
	
}
