package cl.utfsm.sansanoscl.controller.grupos.discusiones.testing;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;

import cl.utfsm.sansanoscl.controller.grupos.discusiones.ListarDiscusionesController;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-context.xml" })
@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional
public class TestListarDiscusionesController {
	@Autowired
	ListarDiscusionesController controller;
	
	@Test
	public void discusionesTodoOK() {
		// Se debe inicializar una sesion, el controlador necesita conocer el
		// userID
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1); // se usa el usuario con id 1 de prueba
		Model model = new ExtendedModelMap();
		// ahora testeo el resultado:
		String result = controller.showDiscusiones("3", model, session);
		assertEquals("/grupos/discusiones/discusiones", result);
	}
	@Test
	public void discusionesGrupoFalso() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1); 
		Model model = new ExtendedModelMap();
		controller.showDiscusiones("124129479", model, session);
		assertEquals(true, model.containsAttribute("error.exist"));
	}
	@Test
	public void discusionesGrupoAjeno() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 11795); // se usa el usuario con id 11795 no pertenece al grupo 150035
		Model model = new ExtendedModelMap();
		controller.showDiscusiones("150035", model, session);
		assertEquals(true, model.containsAttribute("error.exist"));
	}
	
	/**	-----------VER POST DISCUSIONES----------- **/
	@Test
	public void verPostDiscusionesTodoOK() {
		// Se debe inicializar una sesion, el controlador necesita conocer el
		// userID
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1); // se usa el usuario con id 1 de prueba
		Model model = new ExtendedModelMap();
		// ahora testeo el resultado:
		String result = controller.verPostDiscusiones("2", model, session);
		assertEquals("/grupos/discusiones/posts", result);
	}
	
	@Test
	public void verPostDiscusionesGrupoAjeno() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 11795);		// este usuario no pertenece al grupo al que pertenece la discusion con id 2 (150035)
		Model model = new ExtendedModelMap();
		controller.verPostDiscusiones("2", model, session);
		assertEquals(true, model.containsAttribute("error.exist"));
	}
	
	@Test
	public void verPostDiscusionFalsa() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 11795);		// este usuario no pertenece al grupo al que pertenece la discusion con id 2 (150035)
		Model model = new ExtendedModelMap();
		controller.verPostDiscusiones("124224124", model, session);
		assertEquals(true, model.containsAttribute("discusionNoExiste"));
	}
	
}
