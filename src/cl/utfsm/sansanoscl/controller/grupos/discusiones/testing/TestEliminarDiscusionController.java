package cl.utfsm.sansanoscl.controller.grupos.discusiones.testing;

import static org.junit.Assert.assertEquals;

import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import cl.utfsm.sansanoscl.controller.grupos.discusiones.EliminarDiscusionController;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-context.xml" })
@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional
public class TestEliminarDiscusionController {
	@Autowired
	EliminarDiscusionController controller;
	
	@Test
	public void todoOK() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		
		Map<String, ? extends Object> result = controller.delete("2",session);
		assertEquals(result.containsKey("flag"), true);
	}
	
	@Test
	public void discusionFalsa() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		
		Map<String, ? extends Object> result = controller.delete("123783123",session);
		assertEquals(true, result.containsKey("error.exist"));
	}
	
	@Test
	public void discusionAjena() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 11795);
		
		Map<String, ? extends Object> result = controller.delete("2",session);
		assertEquals(true, result.containsKey("error.exist"));
	}
}
