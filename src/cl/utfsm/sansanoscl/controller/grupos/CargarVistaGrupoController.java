package cl.utfsm.sansanoscl.controller.grupos;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cl.utfsm.aexa.modulos.ModuloGrupoAexa;
import cl.utfsm.aexa.modulos.ModuloUsuarioAexa;
import cl.utfsm.aexa.personas.UsuarioAexa;
import cl.utfsm.aexa.redsocial.GrupoExAlumnos;
import cl.utfsm.sansanoscl.utils.ModuloUtilidades;

@Controller
@RequestMapping(value = "/grupos/misgrupos")
public class CargarVistaGrupoController {
	
	@Autowired
	ModuloUsuarioAexa userService;
	@Autowired
	ModuloGrupoAexa moduloGrupo;
	
	@RequestMapping(method = RequestMethod.GET)
	public String cargar(Model model, HttpSession session) {
		UsuarioAexa user = userService.loadUsuarioAexaById((Long)session.getAttribute("userID"));
		model.addAttribute("user_name", ModuloUtilidades.getNombreFormateado((user.getNombreCompleto())));
		List<GrupoExAlumnos> grupos = null;
		
		try {
			grupos = moduloGrupo.obtenerGruposDeUsuario(user, 15, 5);
			System.out.println("size grupos: "+ grupos.size());
			model.addAttribute("grupos", grupos);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error.exist", "true");
		}
		
		
		return "/grupos/index";
	}
	
}
