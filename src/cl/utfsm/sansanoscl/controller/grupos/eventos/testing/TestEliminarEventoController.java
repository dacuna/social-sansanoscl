package cl.utfsm.sansanoscl.controller.grupos.eventos.testing;

import static org.junit.Assert.assertEquals;

import java.util.LinkedHashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import cl.utfsm.sansanoscl.controller.grupos.eventos.EliminarEventoController;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-context.xml" })
@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional
public class TestEliminarEventoController {
	@Autowired
	EliminarEventoController controller;

	@Test
	public void todoOK() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("idEvento", "1003");
		
		Map<String, ? extends Object> result = controller.delete("1003", session);
		assertEquals(result.containsKey("flag"), true);
	}
	
	@Test
	public void eventoAjenoMismoGrupo() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 11795);
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("idEvento", "1003");
		
		Map<String, ? extends Object> result = controller.delete("1003", session);
		assertEquals(true, result.containsKey("error.exist"));
	}
	
	@Test
	public void eventoAjeno() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 18131);
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("idEvento", "1003");
		
		Map<String, ? extends Object> result = controller.delete("1003", session);
		assertEquals(true,result.containsKey("error.exist"));
	}
}
