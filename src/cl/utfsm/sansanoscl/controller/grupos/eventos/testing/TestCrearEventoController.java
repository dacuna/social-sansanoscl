package cl.utfsm.sansanoscl.controller.grupos.eventos.testing;

import static org.junit.Assert.assertEquals;

import java.util.LinkedHashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ExtendedModelMap;

import cl.utfsm.sansanoscl.controller.grupos.eventos.CrearEventoController;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-context.xml" })
@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional
public class TestCrearEventoController {
	@Autowired
	CrearEventoController controller;

	@Test
	public void showFormTest() {
		MockHttpSession session = new MockHttpSession();

		session.putValue("userID", 1);

		Assert.assertEquals("/grupos/eventos/formularios/crearEvento", controller.showForm("150030", new ExtendedModelMap(), session));
	}

	@Test
	public void todoOK() {
		// Se debe inicializar una sesion, el controlador necesita conocer el
		// userID
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1); // se usa el usuario con id 1 de prueba

		// los objectos recibidos por JSON son LinkedHashMap
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("grupoExAlumnos.id", "150030");
		data.put("nombre", "Este es un evento de prueba");
		data.put("resumen", "este es un evento para coleccionar estampillas");
		data.put("fecha", "05-11-2011");
		data.put("direccion", "152 South Second Street");
		data.put("foto", "foto.jpg");
		data.put("descripcion", "esta es la descripcion");

		// ahora testeo el resultado:
		Map<String, ? extends Object> result = controller.create((Object) data, session);
		// recordar que en el controlador se esta devolviendo un map con la id
		// si es que todo funciono correctamente
		assertEquals(true, result.containsKey("flag"));
	}
	
	@Test
	public void noGrupoId() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		
		data.put("nombre", "Este es un evento de prueba");
		data.put("resumen", "este es un evento para coleccionar estampillas");
		data.put("fecha", "05-11-2011");
		data.put("direccion", "152 South Second Street");
		data.put("foto", "foto.jpg");
		data.put("descripcion", "esta es la descripcion");

		Map<String, ? extends Object> result = controller.create((Object) data, session);
		assertEquals(true, result.containsKey("error.exist"));
	}
	@Test
	public void noNombre() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("grupoExAlumnos.id", "42894809241");
		
		data.put("resumen", "este es un evento para coleccionar estampillas");
		data.put("fecha", "05-11-2011");
		data.put("direccion", "152 South Second Street");
		data.put("foto", "foto.jpg");
		data.put("descripcion", "esta es la descripcion");

		Map<String, ? extends Object> result = controller.create((Object) data, session);
		assertEquals(true, result.containsKey("error.exist"));
	}
	@Test
	public void noResumen() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("grupoExAlumnos.id", "42894809241");
		data.put("nombre", "Este es un evento de prueba");
		
		data.put("fecha", "05-11-2011");
		data.put("direccion", "152 South Second Street");
		data.put("foto", "foto.jpg");
		data.put("descripcion", "esta es la descripcion");

		Map<String, ? extends Object> result = controller.create((Object) data, session);
		assertEquals(true, result.containsKey("error.exist"));
	}
	@Test
	public void noFecha() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("grupoExAlumnos.id", "42894809241");
		data.put("nombre", "Este es un evento de prueba");
		data.put("resumen", "este es un evento para coleccionar estampillas");
		
		data.put("direccion", "152 South Second Street");
		data.put("foto", "foto.jpg");
		data.put("descripcion", "esta es la descripcion");

		Map<String, ? extends Object> result = controller.create((Object) data, session);
		assertEquals(true, result.containsKey("error.exist"));
	}
	@Test
	public void noDireccion() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("grupoExAlumnos.id", "42894809241");
		data.put("nombre", "Este es un evento de prueba");
		data.put("resumen", "este es un evento para coleccionar estampillas");
		data.put("fecha", "05-11-2011");
		
		data.put("foto", "foto.jpg");
		data.put("descripcion", "esta es la descripcion");

		Map<String, ? extends Object> result = controller.create((Object) data, session);
		assertEquals(true, result.containsKey("error.exist"));
	}
	@Test
	public void noFoto() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("grupoExAlumnos.id", "42894809241");
		data.put("nombre", "Este es un evento de prueba");
		data.put("resumen", "este es un evento para coleccionar estampillas");
		data.put("fecha", "05-11-2011");
		data.put("direccion", "152 South Second Street");
		
		data.put("descripcion", "esta es la descripcion");

		Map<String, ? extends Object> result = controller.create((Object) data, session);
		assertEquals(true, result.containsKey("error.exist"));
	}
	@Test
	public void noDescipcion() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("grupoExAlumnos.id", "42894809241");
		data.put("nombre", "Este es un evento de prueba");
		data.put("resumen", "este es un evento para coleccionar estampillas");
		data.put("fecha", "05-11-2011");
		data.put("direccion", "152 South Second Street");
		data.put("foto", "foto.jpg");
		

		Map<String, ? extends Object> result = controller.create((Object) data, session);
		assertEquals(true, result.containsKey("error.exist"));
	}
	
	@Test
	public void idGrupoVacia() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("grupoExAlumnos.id", "");
		data.put("nombre", "Este es un evento de prueba");
		data.put("resumen", "este es un evento para coleccionar estampillas");
		data.put("fecha", "05-11-2011");
		data.put("direccion", "152 South Second Street");
		data.put("foto", "foto.jpg");
		data.put("descripcion", "esta es la descripcion");

		Map<String, ? extends Object> result = controller.create((Object) data, session);
		assertEquals(true, result.containsKey("error.exist"));
	}
	@Test
	public void nombreVacia() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("grupoExAlumnos.id", "42894809241");
		data.put("nombre", "");
		data.put("resumen", "este es un evento para coleccionar estampillas");
		data.put("fecha", "05-11-2011");
		data.put("direccion", "152 South Second Street");
		data.put("foto", "foto.jpg");
		data.put("descripcion", "esta es la descripcion");

		Map<String, ? extends Object> result = controller.create((Object) data, session);
		assertEquals(true, result.containsKey("error.exist"));
	}
	@Test
	public void resumenVacia() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("grupoExAlumnos.id", "42894809241");
		data.put("nombre", "Este es un evento de prueba");
		data.put("resumen", "");
		data.put("fecha", "05-11-2011");
		data.put("direccion", "152 South Second Street");
		data.put("foto", "foto.jpg");
		data.put("descripcion", "esta es la descripcion");

		Map<String, ? extends Object> result = controller.create((Object) data, session);
		assertEquals(true, result.containsKey("error.exist"));
	}
	@Test
	public void fechaVacia() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("grupoExAlumnos.id", "42894809241");
		data.put("nombre", "Este es un evento de prueba");
		data.put("resumen", "este es un evento para coleccionar estampillas");
		data.put("fecha", "");
		data.put("direccion", "152 South Second Street");
		data.put("foto", "foto.jpg");
		data.put("descripcion", "esta es la descripcion");

		Map<String, ? extends Object> result = controller.create((Object) data, session);
		assertEquals(true, result.containsKey("error.exist"));
	}
	@Test
	public void dirVacia() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("grupoExAlumnos.id", "42894809241");
		data.put("nombre", "Este es un evento de prueba");
		data.put("resumen", "este es un evento para coleccionar estampillas");
		data.put("fecha", "05-11-2011");
		data.put("direccion", "");
		data.put("foto", "foto.jpg");
		data.put("descripcion", "esta es la descripcion");

		Map<String, ? extends Object> result = controller.create((Object) data, session);
		assertEquals(true, result.containsKey("error.exist"));
	}
	@Test
	public void fotoVacia() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("grupoExAlumnos.id", "42894809241");
		data.put("nombre", "Este es un evento de prueba");
		data.put("resumen", "este es un evento para coleccionar estampillas");
		data.put("fecha", "05-11-2011");
		data.put("direccion", "152 South Second Street");
		data.put("foto", "");
		data.put("descripcion", "esta es la descripcion");

		Map<String, ? extends Object> result = controller.create((Object) data, session);
		assertEquals(true, result.containsKey("error.exist"));
	}
	@Test
	public void descVacia() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("grupoExAlumnos.id", "42894809241");
		data.put("nombre", "Este es un evento de prueba");
		data.put("resumen", "este es un evento para coleccionar estampillas");
		data.put("fecha", "05-11-2011");
		data.put("direccion", "152 South Second Street");
		data.put("foto", "foto.jpg");
		data.put("descripcion", "");

		Map<String, ? extends Object> result = controller.create((Object) data, session);
		assertEquals(true, result.containsKey("error.exist"));
	}
	
	@Test
	public void grupoFalso() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("grupoExAlumnos.id", "42894809241");
		data.put("nombre", "Este es un evento de prueba");
		data.put("resumen", "este es un evento para coleccionar estampillas");
		data.put("fecha", "05-11-2011");
		data.put("direccion", "152 South Second Street");
		data.put("foto", "foto.jpg");
		data.put("descripcion", "esta es la descripcion");

		Map<String, ? extends Object> result = controller.create((Object) data, session);
		assertEquals(true, result.containsKey("error.exist"));
	}
	
	@Test
	public void grupoAjeno() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("grupoExAlumnos.id", "150031");
		data.put("nombre", "Este es un evento de prueba");
		data.put("resumen", "este es un evento para coleccionar estampillas");
		data.put("fecha", "05-11-2011");
		data.put("direccion", "152 South Second Street");
		data.put("foto", "foto.jpg");
		data.put("descripcion", "esta es la descripcion");

		Map<String, ? extends Object> result = controller.create((Object) data, session);
		assertEquals(true, result.containsKey("error.exist"));
	}
}
