package cl.utfsm.sansanoscl.controller.grupos.eventos.testing;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;

import cl.utfsm.sansanoscl.controller.grupos.eventos.ListarEventosGrupoController;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-context.xml" })
@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional
public class TestListarEventosGrupoController {
	@Autowired
	ListarEventosGrupoController controller;

	@Test
	public void showListadoEventosTest() {

		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1); // se usa el usuario con id 1 de prueba
	
		String idgrupo = "150030";
		String result = controller.showListadoEventos(idgrupo,new ExtendedModelMap(),session);

		assertEquals(result, "/grupos/eventos/formularios/eventos");
	}
	@Test
	public void showListadoEventosGrupoAjeno() {

		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 18131);
	
		String idgrupo = "150030";
		Model model = new ExtendedModelMap();
		controller.showListadoEventos(idgrupo, model, session);
		assertEquals(true, model.containsAttribute("error.exist"));
	}
	@Test
	public void grupoFalso() {

		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 18131);
	
		String idGrupo = "412849014";
		Model model = new ExtendedModelMap();
		controller.showListadoEventos(idGrupo, model, session);
		assertEquals(true, model.containsAttribute("error.exist"));
	}
	
	@Test
	public void showEventoTest() {

		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1); // se usa el usuario con id 1 de prueba

		String idevento = "1003";
		String result = controller.showEvento(idevento,new ExtendedModelMap(),session);

		assertEquals(result, "/grupos/eventos/formularios/evento");
	}
	@Test
	public void showEventoGrupoAjeno() {

		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 18131);
		String idEvento = "1003";
		Model model = new ExtendedModelMap();
		controller.showEvento(idEvento, model, session);
		assertEquals(true, model.containsAttribute("error.exist"));
	}
	@Test
	public void eventoFalso() {

		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		String idEvento = "13812948";
		Model model = new ExtendedModelMap();
		controller.showEvento(idEvento, model, session);
		assertEquals(true, model.containsAttribute("error.exist"));
	}
}
