package cl.utfsm.sansanoscl.controller.grupos.eventos.testing;

import static org.junit.Assert.assertEquals;

import java.util.LinkedHashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import cl.utfsm.sansanoscl.controller.grupos.eventos.EditarEventoController;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-context.xml" })
@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional
public class TestEditarEventoController {
	@Autowired
	EditarEventoController controller;

	@Test
	public void todoOK() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("grupoExAlumnos.id", "150030"); //al editar evento no se puede cambiar el grupo del evento
		data.put("id", "1003");
		data.put("nombre", "Este es el nuevo nombre del evento de prueba");
		data.put("resumen", "este es un evento editado para coleccionar estampillas");
		data.put("fecha", "06-11-2011");
		data.put("direccion", "15550 West Second Street");
		data.put("foto", "fotito_nueva.jpg");
		data.put("descripcion", "esta es la nueva descripcion");

		Map<String, ? extends Object> result = controller.edit((Object) data, session);
		assertEquals(true, result.containsKey("flag"));
	}
	
	@Test
	public void noGrupoId() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		
		data.put("id", "1003");
		data.put("nombre", "Este es el nuevo nombre del evento de prueba");
		data.put("resumen", "este es un evento editado para coleccionar estampillas");
		data.put("fecha", "06-11-2011");
		data.put("direccion", "15550 West Second Street");
		data.put("foto", "fotito_nueva.jpg");
		data.put("descripcion", "esta es la nueva descripcion");

		Map<String, ? extends Object> result = controller.edit((Object) data, session);
		assertEquals(true, result.containsKey("error.exist"));
	}
	@Test
	public void noId() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("grupoExAlumnos.id", "150030"); //al editar evento no se puede cambiar el grupo del evento
		
		data.put("nombre", "Este es el nuevo nombre del evento de prueba");
		data.put("resumen", "este es un evento editado para coleccionar estampillas");
		data.put("fecha", "06-11-2011");
		data.put("direccion", "15550 West Second Street");
		data.put("foto", "fotito_nueva.jpg");
		data.put("descripcion", "esta es la nueva descripcion");

		Map<String, ? extends Object> result = controller.edit((Object) data, session);
		assertEquals(true, result.containsKey("error.exist"));
	}
	@Test
	public void noNombre() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("grupoExAlumnos.id", "150030"); //al editar evento no se puede cambiar el grupo del evento
		data.put("id", "1003");
		
		data.put("resumen", "este es un evento editado para coleccionar estampillas");
		data.put("fecha", "06-11-2011");
		data.put("direccion", "15550 West Second Street");
		data.put("foto", "fotito_nueva.jpg");
		data.put("descripcion", "esta es la nueva descripcion");

		Map<String, ? extends Object> result = controller.edit((Object) data, session);
		assertEquals(true, result.containsKey("error.exist"));
	}
	@Test
	public void noResumen() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("grupoExAlumnos.id", "150030"); //al editar evento no se puede cambiar el grupo del evento
		data.put("id", "1003");
		data.put("nombre", "Este es el nuevo nombre del evento de prueba");
		
		data.put("fecha", "06-11-2011");
		data.put("direccion", "15550 West Second Street");
		data.put("foto", "fotito_nueva.jpg");
		data.put("descripcion", "esta es la nueva descripcion");

		Map<String, ? extends Object> result = controller.edit((Object) data, session);
		//assertEquals(true, result.containsKey("error.exist"));
		assertEquals(true, result.containsKey("flag"));
	}
	@Test
	public void noFecha() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("grupoExAlumnos.id", "150030"); //al editar evento no se puede cambiar el grupo del evento
		data.put("id", "1003");
		data.put("nombre", "Este es el nuevo nombre del evento de prueba");
		data.put("resumen", "este es un evento editado para coleccionar estampillas");
		
		data.put("direccion", "15550 West Second Street");
		data.put("foto", "fotito_nueva.jpg");
		data.put("descripcion", "esta es la nueva descripcion");

		Map<String, ? extends Object> result = controller.edit((Object) data, session);
		assertEquals(true, result.containsKey("error.exist"));
	}
	@Test
	public void noDir() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("grupoExAlumnos.id", "150030"); //al editar evento no se puede cambiar el grupo del evento
		data.put("id", "1003");
		data.put("nombre", "Este es el nuevo nombre del evento de prueba");
		data.put("resumen", "este es un evento editado para coleccionar estampillas");
		data.put("fecha", "06-11-2011");
		
		data.put("foto", "fotito_nueva.jpg");
		data.put("descripcion", "esta es la nueva descripcion");

		Map<String, ? extends Object> result = controller.edit((Object) data, session);
		//assertEquals(true, result.containsKey("error.exist"));
		assertEquals(true, result.containsKey("flag"));
	}
	@Test
	public void noFoto() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("grupoExAlumnos.id", "150030"); //al editar evento no se puede cambiar el grupo del evento
		data.put("id", "1003");
		data.put("nombre", "Este es el nuevo nombre del evento de prueba");
		data.put("resumen", "este es un evento editado para coleccionar estampillas");
		data.put("fecha", "06-11-2011");
		data.put("direccion", "15550 West Second Street");
		
		data.put("descripcion", "esta es la nueva descripcion");

		Map<String, ? extends Object> result = controller.edit((Object) data, session);
		//assertEquals(true, result.containsKey("error.exist"));
		assertEquals(true, result.containsKey("flag"));
	}
	@Test
	public void noDesc() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("grupoExAlumnos.id", "150030"); //al editar evento no se puede cambiar el grupo del evento
		data.put("id", "1003");
		data.put("nombre", "Este es el nuevo nombre del evento de prueba");
		data.put("resumen", "este es un evento editado para coleccionar estampillas");
		data.put("fecha", "06-11-2011");
		data.put("direccion", "15550 West Second Street");
		data.put("foto", "fotito_nueva.jpg");

		Map<String, ? extends Object> result = controller.edit((Object) data, session);
		assertEquals(true, result.containsKey("error.exist"));
	}
	
	@Test
	public void grupoIdVacia() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();
		data.put("grupoExAlumnos.id", "");
		data.put("id", "1003");
		data.put("nombre", "Este es el nuevo nombre del evento de prueba");
		data.put("resumen", "este es un evento editado para coleccionar estampillas");
		data.put("fecha", "06-11-2011");
		data.put("direccion", "15550 West Second Street");
		data.put("foto", "fotito_nueva.jpg");
		data.put("descripcion", "esta es la nueva descripcion");
		Map<String, ? extends Object> result = controller.edit((Object) data, session);
		assertEquals(true, result.containsKey("error.exist"));
	}
	@Test
	public void idVacia() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();
		data.put("grupoExAlumnos.id", "150030");
		data.put("id", "");
		data.put("nombre", "Este es el nuevo nombre del evento de prueba");
		data.put("resumen", "este es un evento editado para coleccionar estampillas");
		data.put("fecha", "06-11-2011");
		data.put("direccion", "15550 West Second Street");
		data.put("foto", "fotito_nueva.jpg");
		data.put("descripcion", "esta es la nueva descripcion");
		Map<String, ? extends Object> result = controller.edit((Object) data, session);
		assertEquals(true, result.containsKey("error.exist"));
	}
	@Test
	public void nombreVacia() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();
		data.put("grupoExAlumnos.id", "150030");
		data.put("id", "1003");
		data.put("nombre", "");
		data.put("resumen", "este es un evento editado para coleccionar estampillas");
		data.put("fecha", "06-11-2011");
		data.put("direccion", "15550 West Second Street");
		data.put("foto", "fotito_nueva.jpg");
		data.put("descripcion", "esta es la nueva descripcion");
		Map<String, ? extends Object> result = controller.edit((Object) data, session);
		assertEquals(true, result.containsKey("error.exist"));
	}
	@Test
	public void resumenVacia() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();
		data.put("grupoExAlumnos.id", "150030");
		data.put("id", "1003");
		data.put("nombre", "Este es el nuevo nombre del evento de prueba");
		data.put("resumen", "");
		data.put("fecha", "06-11-2011");
		data.put("direccion", "15550 West Second Street");
		data.put("foto", "fotito_nueva.jpg");
		data.put("descripcion", "esta es la nueva descripcion");
		Map<String, ? extends Object> result = controller.edit((Object) data, session);
		//assertEquals(true, result.containsKey("error.exist"));
		assertEquals(true, result.containsKey("flag"));	
	}
	@Test
	public void fechaVacia() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();
		data.put("grupoExAlumnos.id", "150030");
		data.put("id", "1003");
		data.put("nombre", "Este es el nuevo nombre del evento de prueba");
		data.put("resumen", "este es un evento editado para coleccionar estampillas");
		data.put("fecha", "");
		data.put("direccion", "15550 West Second Street");
		data.put("foto", "fotito_nueva.jpg");
		data.put("descripcion", "esta es la nueva descripcion");
		Map<String, ? extends Object> result = controller.edit((Object) data, session);
		assertEquals(true, result.containsKey("error.exist"));
	}
	@Test
	public void direccionVacia() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();
		data.put("grupoExAlumnos.id", "150030");
		data.put("id", "1003");
		data.put("nombre", "Este es el nuevo nombre del evento de prueba");
		data.put("resumen", "este es un evento editado para coleccionar estampillas");
		data.put("fecha", "06-11-2011");
		data.put("direccion", "");
		data.put("foto", "fotito_nueva.jpg");
		data.put("descripcion", "esta es la nueva descripcion");
		Map<String, ? extends Object> result = controller.edit((Object) data, session);
		//assertEquals(true, result.containsKey("error.exist"));
		assertEquals(true, result.containsKey("flag"));
	}
	@Test
	public void fotoVacia() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();
		data.put("grupoExAlumnos.id", "150030");
		data.put("id", "1003");
		data.put("nombre", "Este es el nuevo nombre del evento de prueba");
		data.put("resumen", "este es un evento editado para coleccionar estampillas");
		data.put("fecha", "06-11-2011");
		data.put("direccion", "15550 West Second Street");
		data.put("foto", "");
		data.put("descripcion", "esta es la nueva descripcion");
		Map<String, ? extends Object> result = controller.edit((Object) data, session);
		//assertEquals(true, result.containsKey("error.exist"));
		assertEquals(true, result.containsKey("flag"));
	}
	@Test
	public void descripcionVacia() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();
		data.put("grupoExAlumnos.id", "150030");
		data.put("id", "1003");
		data.put("nombre", "Este es el nuevo nombre del evento de prueba");
		data.put("resumen", "este es un evento editado para coleccionar estampillas");
		data.put("fecha", "06-11-2011");
		data.put("direccion", "15550 West Second Street");
		data.put("foto", "fotito_nueva.jpg");
		data.put("descripcion", "");
		Map<String, ? extends Object> result = controller.edit((Object) data, session);
		assertEquals(true, result.containsKey("error.exist"));
	}
	
	@Test
	public void grupoFalso() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 11795);
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("grupoExAlumnos.id", "150029");
		data.put("id", "1003");
		data.put("nombre", "Este es el nuevo nombre del evento de prueba");
		data.put("resumen", "este es un evento editado para coleccionar estampillas");
		data.put("fecha", "06-11-2011");
		data.put("direccion", "15550 West Second Street");
		data.put("foto", "fotito_nueva.jpg");
		data.put("descripcion", "esta es la nueva descripcion");

		Map<String, ? extends Object> result = controller.edit((Object) data, session);
		assertEquals(true, result.containsKey("error.exist"));
	}
	@Test
	public void eventoAjeno() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 11795);
		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("grupoExAlumnos.id", "150030");
		data.put("id", "1003");
		data.put("nombre", "Este es el nuevo nombre del evento de prueba");
		data.put("resumen", "este es un evento editado para coleccionar estampillas");
		data.put("fecha", "06-11-2011");
		data.put("direccion", "15550 West Second Street");
		data.put("foto", "fotito_nueva.jpg");
		data.put("descripcion", "esta es la nueva descripcion");

		Map<String, ? extends Object> result = controller.edit((Object) data, session);
		assertEquals(true, result.containsKey("error.exist"));
	}
}
