package cl.utfsm.sansanoscl.controller.grupos.eventos;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cl.utfsm.aexa.modulos.ModuloGrupoAexa;
import cl.utfsm.aexa.modulos.ModuloUsuarioAexa;
import cl.utfsm.aexa.personas.UsuarioAexa;
import cl.utfsm.aexa.redsocial.EventoExAlumnos;
import cl.utfsm.aexa.redsocial.GrupoExAlumnos;
import cl.utfsm.aexa.redsocial.MiembroGrupoExalumnos;
import cl.utfsm.sansanoscl.utils.JsonDeserializer;
import cl.utfsm.sansanoscl.utils.ModuloUtilidades;
import cl.utfsm.sansanoscl.validators.EventoExAlumnosValidator;
import cl.utfsm.sansanoscl.validators.GrupoExAlumnoValidator;

@Controller
@RequestMapping(value = "/grupos/eventos/editarEvento")
public class EditarEventoController {
	@Autowired ModuloGrupoAexa moduloGrupo;
	@Autowired ModuloUsuarioAexa moduloUsuario;
	@Autowired EventoExAlumnosValidator eventoExAlumnosValidator;
	@Autowired GrupoExAlumnoValidator grupoExAlumnoValidator;

	private static final Logger logger = Logger.getLogger(EditarEventoController.class);

	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody Map<String, ? extends Object> edit(@RequestBody Object requested,
			HttpSession session) {

		EventoExAlumnos eventoEditado = new EventoExAlumnos();
		eventoEditado = (EventoExAlumnos) JsonDeserializer.parsingClass(eventoEditado, requested);
		BindingResult result = new BeanPropertyBindingResult(eventoEditado, "");
		UsuarioAexa user = new UsuarioAexa();
		user.setId((Long) session.getAttribute("userID"));
		GrupoExAlumnos grupo = null;
		MiembroGrupoExalumnos membresia = null;
		Map<String, String> errores = new HashMap<String, String>();

		try {
			grupo = moduloGrupo.loadGrupo(eventoEditado.getGrupoExAlumnos().getId());
		} catch (Exception e1) {
			e1.printStackTrace();
			errores.put("error.exist", "true");
			return Collections.singletonMap("error.exist", "true");
		}
		if (grupo == null)
			return Collections.singletonMap("error.exist", "true");

		try {
			membresia = moduloGrupo.usuarioPerteneceGrupo(user, grupo);
		} catch (Exception e1) {
			e1.printStackTrace();
			errores.put("error.exist", "true");
			return errores;
		}
		if (membresia == null)
			return Collections.singletonMap("error.exist", "true");

		eventoExAlumnosValidator.validate(eventoEditado, result);
		grupoExAlumnoValidator.validateUsuarioCrearEventos(membresia, result);
		errores = ModuloUtilidades.parseErrors(result);

		if (!errores.isEmpty()) {
			return errores;
		}
		try {
			//se mantiene el creador del evento
			EventoExAlumnos old = moduloGrupo.getEventoById(eventoEditado.getId().toString());
			eventoEditado.setUsuarioAexa(old.getUsuarioAexa());
			moduloGrupo.updateEventoGrupo(eventoEditado);
		} catch (Exception e) {
			logger.warn(e);
			return Collections.singletonMap("error.exist", "true");
		}

		return Collections.singletonMap("flag", "true");

	}

}
