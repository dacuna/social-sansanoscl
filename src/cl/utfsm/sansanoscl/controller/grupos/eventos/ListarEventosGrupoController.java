package cl.utfsm.sansanoscl.controller.grupos.eventos;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import cl.utfsm.aexa.modulos.ModuloGrupoAexa;
import cl.utfsm.aexa.modulos.ModuloUsuarioAexa;
import cl.utfsm.aexa.personas.UsuarioAexa;
import cl.utfsm.aexa.redsocial.EventoExAlumnos;
import cl.utfsm.aexa.redsocial.GrupoExAlumnos;
import cl.utfsm.aexa.redsocial.MiembroGrupoExalumnos;
import cl.utfsm.sansanoscl.utils.ModuloUtilidades;
import cl.utfsm.sansanoscl.validators.GrupoExAlumnoValidator;

@Controller
@RequestMapping(value = "/grupos/eventos")
public class ListarEventosGrupoController {
	@Autowired ModuloUsuarioAexa moduloUsuario;
	@Autowired ModuloGrupoAexa moduloGrupo;
	@Autowired GrupoExAlumnoValidator grupoExAlumnoValidator;
	
	private final String VISTA_INICIO = "/grupos/eventos/formularios/eventos";
	private final String VISTA_EVENTO = "/grupos/eventos/formularios/evento";
	private static final Logger logger = Logger.getLogger(ListarEventosGrupoController.class);

	@RequestMapping(method = RequestMethod.GET)
	public String showListadoEventos(
			@RequestParam(value="id", defaultValue = "null") String id, Model model,
			HttpSession session) {
		UsuarioAexa user = moduloUsuario.loadUsuarioAexaById((Long) session.getAttribute("userID"));
		GrupoExAlumnos grupo = null;
		BindingResult result = new BeanPropertyBindingResult(grupo, "");
		Map<String, String> errores = null;
		List<EventoExAlumnos> listaEventos = null;
		
		System.out.println("id grupo "+ id);
		try {
			grupo = moduloGrupo.loadGrupo(Integer.parseInt(id));
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error.exist", "true");
			return VISTA_INICIO;
		}
		if (grupo == null) {
			model.addAttribute("error.exist", "true");
			return VISTA_INICIO;
		}
		
		MiembroGrupoExalumnos miembro = moduloGrupo.usuarioPerteneceGrupo(user,
				grupo);
		
		if(miembro.getTipoRolMiembroExAlumnos().getNombreUsuario().toString().equals("SOLICITANTE"))
			model.addAttribute("solicitante", "true");
		model.addAttribute("miembro", miembro);
		model.addAttribute("ModuloUtilidades", new ModuloUtilidades());
		model.addAttribute("grupo", grupo);

		
		try{
			grupoExAlumnoValidator.validateUsuarioPerteneceGrupo(grupo, user, result);
		}catch(IllegalStateException e){
			e.printStackTrace();
			model.addAttribute("error.exist", "true");
			return VISTA_INICIO;
		}
		errores = ModuloUtilidades.parseErrors(result);
		if (!errores.isEmpty()) {
			model.addAttribute("error.exist", "true");
			return VISTA_INICIO;
		}

		try {
			listaEventos = moduloGrupo.getEventos(Integer.parseInt(id));
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (listaEventos == null) {
			model.addAttribute("error.exist", "true");
			return VISTA_INICIO;
		}

		logger.info("eventos size: " + listaEventos.size());
		
		model.addAttribute("eventos", listaEventos);
		model.addAttribute("user", user);
	
		return VISTA_INICIO;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/evento")
	public String showEvento(@RequestParam(value="id", defaultValue = "null") String id,
			Model model, HttpSession session) {
		UsuarioAexa user = moduloUsuario.loadUsuarioAexaById((Long) session
				.getAttribute("userID"));
		EventoExAlumnos evento = null;
		GrupoExAlumnos grupo = new GrupoExAlumnos();
		BindingResult result = new BeanPropertyBindingResult(grupo, "");
		Map<String, String> errores = null;

		try {
			evento = moduloGrupo.getEventoById(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (evento == null) {
			model.addAttribute("error.exist", "true");
			return VISTA_INICIO;
		}
		grupo = evento.getGrupoExAlumnos();
		grupoExAlumnoValidator.validateUsuarioPerteneceGrupo(grupo, user,
				result);

		errores = ModuloUtilidades.parseErrors(result);
		if (!errores.isEmpty()) {
			model.addAttribute("error.exist", "true");
			return VISTA_EVENTO;
		}
		

		model.addAttribute("evento", evento);
		model.addAttribute("user", user);
		model.addAttribute("ModuloUtilidades", new ModuloUtilidades());
		return VISTA_EVENTO;

	}

}
