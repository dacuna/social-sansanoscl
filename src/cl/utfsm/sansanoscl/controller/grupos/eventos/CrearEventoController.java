package cl.utfsm.sansanoscl.controller.grupos.eventos;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cl.utfsm.aexa.modulos.ModuloGrupoAexa;
import cl.utfsm.aexa.personas.UsuarioAexa;
import cl.utfsm.aexa.redsocial.EventoExAlumnos;
import cl.utfsm.aexa.redsocial.GrupoExAlumnos;
import cl.utfsm.aexa.redsocial.MiembroGrupoExalumnos;
import cl.utfsm.sansanoscl.utils.JsonDeserializer;
import cl.utfsm.sansanoscl.utils.ModuloUtilidades;
import cl.utfsm.sansanoscl.validators.EventoExAlumnosValidator;
import cl.utfsm.sansanoscl.validators.GrupoExAlumnoValidator;

@Controller
@RequestMapping(value = "/grupos/crearEvento")
public class CrearEventoController {
	@Autowired ModuloGrupoAexa moduloGrupo;
	@Autowired GrupoExAlumnoValidator grupoExAlumnoValidator;
	@Autowired EventoExAlumnosValidator eventoExAlumnosValidator;

	private final String VISTA_FORM = "/grupos/eventos/formularios/crearEvento";
	private static final Logger logger = Logger.getLogger(CrearEventoController.class);

	@RequestMapping(method = RequestMethod.GET)
	public String showForm(@RequestParam(value="id", defaultValue = "null") String id,
			Model model, HttpSession session) {
		
		if (!ModuloUtilidades.checkIfNumber(id)) {
			model.addAttribute("error", "true");
			logger.info("ID de grupo invalida");
			return VISTA_FORM;
		}
		
		UsuarioAexa user = new UsuarioAexa();
		user.setId((Long) session.getAttribute("userID"));
		
		GrupoExAlumnos grupo = null;
		
		try {
			grupo = moduloGrupo.loadGrupo(Integer.parseInt(id));
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "true");
			return VISTA_FORM;
		}
		if (grupo == null) {
			model.addAttribute("error", "true");
			return VISTA_FORM;
		}
		
		model.addAttribute("idGrupo", grupo.getId());
		
		BindingResult result = new BeanPropertyBindingResult(grupo, "");
		Map<String, String> errores = new HashMap<String, String>();
		grupoExAlumnoValidator.validateUsuarioPerteneceGrupo(grupo, user, result);
		errores = ModuloUtilidades.parseErrors(result);
		
		if (!errores.isEmpty())
			return VISTA_FORM;

		return VISTA_FORM;
	}

	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody Map<String, ? extends Object> create(@RequestBody Object requested,
			HttpSession session) {
		
		EventoExAlumnos evento = new EventoExAlumnos();
		evento = (EventoExAlumnos) JsonDeserializer.parsingClass(evento, requested);
		
		GrupoExAlumnos grupo = null;
		try {
			grupo = moduloGrupo.loadGrupo(evento.getGrupoExAlumnos().getId());
		} catch (Exception e) {
			e.printStackTrace();
			return Collections.singletonMap("grupo.invalid", "Error al cargar el grupo");
		}
		if (grupo == null)
			return Collections.singletonMap("grupo.inexistente", "No existe el grupo solicitado");

		BindingResult result = new BeanPropertyBindingResult(evento, "");
		Map<String, String> errores = null;
		MiembroGrupoExalumnos membresia = null;
		UsuarioAexa user = new UsuarioAexa();
		user.setId((Long) session.getAttribute("userID"));
		
		membresia = moduloGrupo.usuarioPerteneceGrupo(user, grupo);
		if (membresia == null)
			return Collections.singletonMap("membresia.error", "Usuario no pertenece a grupo");

		eventoExAlumnosValidator.validateCreateEvent(evento, result);
		grupoExAlumnoValidator.validateUsuarioCrearEventos(membresia, result);
		errores = ModuloUtilidades.parseErrors(result);
		if (!errores.isEmpty())
			return errores;

		try {
			evento.setUsuarioAexa(user);
			evento.setGrupoExAlumnos(grupo);
			evento.setFechaModificacion(new Date());
			moduloGrupo.insertarEvento(evento);
		} catch (Exception e) {
			e.printStackTrace();
			return Collections.singletonMap("error.exist", "true");
		}

		return Collections.singletonMap("flag", "true");

	}

}
