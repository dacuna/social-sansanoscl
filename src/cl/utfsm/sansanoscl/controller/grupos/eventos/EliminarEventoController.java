package cl.utfsm.sansanoscl.controller.grupos.eventos;

import java.util.Collections;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cl.utfsm.aexa.modulos.ModuloGrupoAexa;
import cl.utfsm.aexa.modulos.ModuloUsuarioAexa;
import cl.utfsm.aexa.personas.UsuarioAexa;
import cl.utfsm.aexa.redsocial.EventoExAlumnos;
import cl.utfsm.aexa.redsocial.GrupoExAlumnos;
import cl.utfsm.aexa.redsocial.MiembroGrupoExalumnos;
import cl.utfsm.sansanoscl.utils.ModuloUtilidades;
import cl.utfsm.sansanoscl.validators.GrupoExAlumnoValidator;

@Controller
@RequestMapping(value = "/grupos/eventos/eliminarEvento")
public class EliminarEventoController {
	@Autowired ModuloGrupoAexa moduloGrupo;
	@Autowired ModuloUsuarioAexa moduloUsuario;
	@Autowired GrupoExAlumnoValidator grupoExAlumnoValidator;

	@SuppressWarnings("unused")
	private static final Logger logger = Logger.getLogger(EliminarEventoController.class);

	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody
	Map<String, ? extends Object> delete(@RequestBody String id,
			HttpSession session) {
		EventoExAlumnos evento = new EventoExAlumnos();
		GrupoExAlumnos grupo = new GrupoExAlumnos();
		MiembroGrupoExalumnos membresia = new MiembroGrupoExalumnos();
		BindingResult result = new BeanPropertyBindingResult(grupo, "");
		Map<String, String> errores = null;
		
		UsuarioAexa user = new UsuarioAexa();
		user.setId((Long) session.getAttribute("userID"));

		if (!ModuloUtilidades.checkIfNumber(id))
			return Collections.singletonMap("evento.invalid", "Debes seleccionar un evento valido");

		try {
			evento = moduloGrupo.getEventoById(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (evento == null)
			return Collections.singletonMap("evento.invalid", "Debes seleccionar un evento valido");

		try {
			grupo = moduloGrupo.loadGrupo(evento.getGrupoExAlumnos().getId());
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (grupo == null)
			Collections.singletonMap("error.exist", "true");

		try {
			membresia = moduloGrupo.usuarioPerteneceGrupo(user, grupo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (membresia == null)
			Collections.singletonMap("error.exist", "true");

		grupoExAlumnoValidator.validateUsuarioCrearEventos(membresia, result);
		errores = ModuloUtilidades.parseErrors(result);
		if (!errores.isEmpty())
			return errores;

		try {
			moduloGrupo.eliminarEvento(evento);
		} catch (Exception e) {
			e.printStackTrace();
			return Collections.singletonMap("eliminarEvento.error", "true");
		}

		return Collections.singletonMap("flag", "true");

	}

}
