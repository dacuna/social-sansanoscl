package cl.utfsm.sansanoscl.controller.grupos;

import java.util.Collections;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import org.apache.log4j.Logger;

import cl.utfsm.aexa.modulos.ModuloGrupoAexa;
import cl.utfsm.aexa.modulos.ModuloUsuarioAexa;
import cl.utfsm.aexa.personas.UsuarioAexa;

import cl.utfsm.aexa.redsocial.GrupoExAlumnos;
import cl.utfsm.aexa.redsocial.MiembroGrupoExalumnos;
import cl.utfsm.sansanoscl.utils.JsonDeserializer;
import cl.utfsm.sansanoscl.utils.ModuloUtilidades;

@Controller
@RequestMapping(value = "/grupos/unirseGrupo")
public class UnirseGrupoController {
	@Autowired ModuloGrupoAexa moduloGrupo;
	@Autowired ModuloUsuarioAexa moduloUsuario;

	private static final Logger logger = Logger.getLogger(CargarPerfilGrupoController.class);

	@RequestMapping(method = RequestMethod.GET)
	public String showForm(@RequestParam(value = "gid", defaultValue = "null") String id, Model model, 
			HttpSession session) {
		if(!ModuloUtilidades.checkIfNumber(id)) {
			model.addAttribute("idInvalida", "true");
			return "/grupos/unirseGrupo";
		}
		
		GrupoExAlumnos grupo = moduloGrupo.loadGrupo(Integer.parseInt(id));
		if(grupo == null){
			model.addAttribute("grupoInvalido", "true");
			return "/grupos/unirseGrupo";
		}
		
		model.addAttribute("grupo", grupo);
		
		return "/grupos/unirseGrupo";
	}

	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody Map<String, ? extends Object> create(@RequestBody Object requested, 
			HttpSession session) {
		GrupoExAlumnos ver = null;
		MiembroGrupoExalumnos miembroGrupoExalumnos = new MiembroGrupoExalumnos();
		miembroGrupoExalumnos = (MiembroGrupoExalumnos) JsonDeserializer.parsingClass(miembroGrupoExalumnos, requested);
		UsuarioAexa user = new UsuarioAexa();
		user.setId((Long) session.getAttribute("userID"));
		
		logger.info("recibirMensajeEventos: " + miembroGrupoExalumnos.getRecibirMensajesEventos());
		logger.info("recibirMensajeDiscusiones: " + miembroGrupoExalumnos.getRecibirMensajesDiscusiones());
		logger.info("usuario id: " + user.getId());
		logger.info("la id del grupo es: " + miembroGrupoExalumnos.getGrupoExAlumnos().getId());
		
		try {
			ver = moduloGrupo.loadGrupo(miembroGrupoExalumnos.getGrupoExAlumnos().getId());
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		if (ver == null)
			return Collections.singletonMap("errores.exist", "true");
		
		miembroGrupoExalumnos.setGrupoExAlumnos(ver);
		miembroGrupoExalumnos.setUsuarioAexa(user);
		if(ver.getPermisos() == 0)
			miembroGrupoExalumnos.setTipoRolMiembroExAlumnos(moduloGrupo.getTipoRolesMiembrosExAlumnos("SOLICITANTE"));
		else	
			miembroGrupoExalumnos.setTipoRolMiembroExAlumnos(moduloGrupo.getTipoRolesMiembrosExAlumnos("USUARIO"));
		
		try {
			moduloGrupo.insertarMiembroGrupoExAlumnos(miembroGrupoExalumnos);
		} catch (RuntimeException e) {
			e.printStackTrace();
			return Collections.singletonMap("errores.exist", "true");
		}

		return Collections.singletonMap("flag", "true");

	}

}
