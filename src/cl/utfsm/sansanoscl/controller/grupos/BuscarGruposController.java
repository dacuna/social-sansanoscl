package cl.utfsm.sansanoscl.controller.grupos;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import cl.utfsm.aexa.modulos.ModuloGrupoAexa;
import cl.utfsm.aexa.modulos.ModuloUsuarioAexa;
import cl.utfsm.aexa.redsocial.GrupoExAlumnos;
import cl.utfsm.sansanoscl.service.UsuarioAexaService;
import cl.utfsm.sansanoscl.utils.ModuloUtilidades;

@Controller
@RequestMapping(value = "/grupos/buscar")
public class BuscarGruposController {
	@Autowired
	ModuloGrupoAexa moduloGrupo;
	@Autowired
	ModuloUsuarioAexa moduloUsuario;
	@Autowired
	UsuarioAexaService userService;

	private static final Logger logger = Logger.getLogger(BuscarGruposController.class);

	@RequestMapping(method = RequestMethod.GET)
	public String buscar(@RequestParam(value = "cadena", defaultValue = "") String cadena, 
										Model model, HttpServletRequest request,
										HttpSession session) {

		List<GrupoExAlumnos> resultado = null;
			
		try {
		//	String cadena = (request.getParameter("cadena"));
			logger.info("cadena es: " + cadena);
			resultado = userService.buscarGrupos(cadena);
			logger.info(resultado);
			model.addAttribute("resultados", resultado);
			model.addAttribute("Utilidades", new ModuloUtilidades());

			System.out.println("resultado size : " + resultado.size());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error de busqueda");
			model.addAttribute("error.exist", "true");

		}
		
		return "/grupos/buscar";
	}

}
