package cl.utfsm.sansanoscl.controller.grupos;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import cl.utfsm.aexa.modulos.ModuloGrupoAexa;
import cl.utfsm.aexa.personas.UsuarioAexa;
import cl.utfsm.aexa.redsocial.GrupoExAlumnos;
import cl.utfsm.sansanoscl.service.UsuarioAexaService;
import cl.utfsm.sansanoscl.utils.ModuloUtilidades;

@Controller
@RequestMapping(value = "/grupos/directorio")
public class CargarDirectorioGruposController {
	@Autowired ModuloGrupoAexa moduloGrupo;
	@Autowired UsuarioAexaService userService;

	final String VISTA_FORM = "/grupos/directorio";
	@SuppressWarnings("unused")
	private static final Logger logger = Logger.getLogger(CargarDirectorioGruposController.class);

	@RequestMapping(method = RequestMethod.GET)
	public String cargar(@RequestParam(value = "p", defaultValue = "1") String page, 
			Model model, HttpSession session) {
		UsuarioAexa user = new UsuarioAexa();
		user.setId((Long)session.getAttribute("userID"));

		Integer cantidad = 10;
		Integer pagina = !ModuloUtilidades.checkIfNumber(page) ? 1 : Integer.parseInt(page);
		Number totalMensajes = userService.getTotalGrupos();
		Integer offset = (cantidad * (pagina-1) > totalMensajes.intValue()) ? 0 : cantidad * (pagina-1);
		
		List<GrupoExAlumnos> populares = userService.directorioGrupos(cantidad, offset);
		model.addAttribute("populares", populares);
		model.addAttribute("usuario", user);
		model.addAttribute("Utilidades", new ModuloUtilidades());
		Double cantidadPaginas = Math.ceil(totalMensajes.doubleValue()/cantidad.doubleValue());
		model.addAttribute("cantidadPaginas", cantidadPaginas.intValue());
		model.addAttribute("paginaActual", pagina);
		model.addAttribute("totalGrupos", totalMensajes);
		model.addAttribute("inicioPagina", (pagina-1)*cantidad + 1);
		model.addAttribute("finalPagina", (pagina*cantidad > totalMensajes.intValue()) ? totalMensajes : pagina*cantidad);
		return VISTA_FORM;
	}
}
