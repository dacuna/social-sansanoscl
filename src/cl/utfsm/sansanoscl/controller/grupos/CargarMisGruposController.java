package cl.utfsm.sansanoscl.controller.grupos;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import cl.utfsm.aexa.modulos.ModuloGrupoAexa;
import cl.utfsm.aexa.modulos.ModuloUsuarioAexa;
import cl.utfsm.aexa.personas.UsuarioAexa;
import cl.utfsm.aexa.redsocial.GrupoExAlumnos;
import cl.utfsm.sansanoscl.utils.ModuloUtilidades;

@Controller
@RequestMapping(value = "/grupos")
public class CargarMisGruposController {
	@Autowired
	ModuloGrupoAexa moduloGrupo;
	@Autowired
	ModuloUsuarioAexa userService;

	final String VISTA_FORM = "/grupos/index";
	private static final Logger logger = Logger.getLogger(CargarMisGruposController.class);

	@RequestMapping(method = RequestMethod.GET)
	public String cargar(@RequestParam(value = "p", defaultValue = "1") String page, Model model, 
			HttpSession session) {
		UsuarioAexa user = userService.loadUsuarioAexaById((Long)session.getAttribute("userID"));
		//paginacion
		Integer cantidad = 10;
		Integer pagina = !ModuloUtilidades.checkIfNumber(page) ? 1 : Integer.parseInt(page);
		Number total = moduloGrupo.getTotalGruposDeUsuario(user);
		Integer offset = (cantidad * (pagina-1) > total.intValue()) ? 0 : cantidad * (pagina-1);
		
		List<GrupoExAlumnos> grupos = null;
		try {
			grupos = moduloGrupo.obtenerGruposDeUsuario(user, cantidad, offset);
			logger.info("size grupos: "+ grupos.size());
			model.addAttribute("grupos", grupos);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error.exist", "true");
		}

		model.addAttribute("usuario", user);
		model.addAttribute("user_name", ModuloUtilidades.getNombreFormateado((user.getNombreCompleto())));
		model.addAttribute("Utilidades", new ModuloUtilidades());
		
		Double cantidadPaginas = Math.ceil(total.doubleValue()/cantidad.doubleValue());
		model.addAttribute("cantidadPaginas", cantidadPaginas.intValue());
		model.addAttribute("paginaActual", pagina);
		model.addAttribute("totalGrupos", total);
		model.addAttribute("inicioPagina", (pagina-1)*cantidad + 1);
		model.addAttribute("finalPagina", (pagina*cantidad > total.intValue()) ? total : pagina*cantidad);
		return VISTA_FORM;
	}
}
