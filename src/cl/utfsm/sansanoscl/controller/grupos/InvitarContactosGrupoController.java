package cl.utfsm.sansanoscl.controller.grupos;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cl.utfsm.aexa.modulos.ModuloGrupoAexa;
import cl.utfsm.aexa.modulos.ModuloUsuarioAexa;
import cl.utfsm.aexa.personas.UsuarioAexa;

import cl.utfsm.aexa.redsocial.GrupoExAlumnos;
import cl.utfsm.aexa.redsocial.MiembroGrupoExalumnos;
import cl.utfsm.aexa.tipos.TipoRolMiembroExAlumnos;
import cl.utfsm.sansanoscl.dao.UsuarioDao;
import cl.utfsm.sansanoscl.utils.ModuloUtilidades;
import cl.utfsm.sansanoscl.utils.UsuarioSocial;
import cl.utfsm.sansanoscl.validators.GrupoExAlumnoValidator;

@Controller
@RequestMapping(value = "/grupos/invitarContactos")
public class InvitarContactosGrupoController {
	@Autowired
	ModuloGrupoAexa moduloGrupo;
	@Autowired
	ModuloUsuarioAexa moduloUsuario;
	@Autowired
	UsuarioDao usuarioDao;
	@Autowired
	GrupoExAlumnoValidator grupoExAlumnoValidator;

	private final String VISTA_FORM = "/grupos/formularios/invitarContactos";
	private static final Logger logger = Logger
			.getLogger(InvitarContactosGrupoController.class);

	@RequestMapping(method = RequestMethod.GET)
	public String invitarContactos(
			@RequestParam(value="gid", defaultValue = "null") String gid, Model model,
			HttpSession session) {
		UsuarioAexa user = moduloUsuario.loadUsuarioAexaById((Long) session
				.getAttribute("userID"));
		GrupoExAlumnos grupo = new GrupoExAlumnos();
		BindingResult result = new BeanPropertyBindingResult(grupo, "");
		Map<String, String> errores = new HashMap<String, String>();
		List<UsuarioAexa> contactos = null;
		List<MiembroGrupoExalumnos> miembros = null;
		List<Long> idMiembros = new ArrayList<Long>();
		List<UsuarioSocial> invitar = new ArrayList<UsuarioSocial>();

		try {
			grupo = moduloGrupo.loadGrupo(Integer.parseInt(gid));
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (grupo == null) {
			model.addAttribute("error.exist", "true");
			return VISTA_FORM;
		}

		grupoExAlumnoValidator.validateUsuarioPerteneceGrupo(grupo, user,
				result);

		errores = ModuloUtilidades.parseErrors(result);
		if (!errores.isEmpty()) {
			errores.put("errores.exist", "true");
			return VISTA_FORM;
		}

		try {
			contactos = moduloUsuario.getContactosByIdUser(user.getId());
			miembros = moduloGrupo.getIntegrantesGrupo(grupo.getId());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error.exist", "true");
			return VISTA_FORM;
		}
		if (contactos == null || miembros == null) {
			model.addAttribute("error.exist", "true");
			return VISTA_FORM;
		}

		// si existen contactos, se debe comprobar que estos no pertenescan al
		// grupo
		// traspaso la informacion relevante de los miembros a una lista
		// (contiene la id de este)
		for (MiembroGrupoExalumnos m : miembros)
			idMiembros.add(m.getUsuarioAexa().getId());

		for (UsuarioAexa u : contactos) {
			// se verifica si es que existe una id de usuario de los contactos
			// en la de los grupos
			if (!idMiembros.contains(u.getId())) {
				UsuarioSocial nuevo = new UsuarioSocial();
				nuevo.setId(u.getId());
				nuevo.setNombreIncompleto(u.getNombreIncompleto());
				nuevo.setLaboralActual(usuarioDao
						.getAntecedenteLaboralMasReciente(nuevo.getId()));
				invitar.add(nuevo);
			}
		}
		// en invitar estan los usuarios que se pueden invitar al grupo
		model.addAttribute("invitar", invitar);

		model.addAttribute("id", gid);
		return VISTA_FORM;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody
	Map<String, ? extends Object> invitar(@RequestBody Object requested,
			HttpSession session) {
		UsuarioAexa user = moduloUsuario.loadUsuarioAexaById((Long) session
				.getAttribute("userID"));
		GrupoExAlumnos grupo = new GrupoExAlumnos();
		LinkedHashMap<String, ?> data = (LinkedHashMap<String, ?>) requested;
		String idGrupo = (String) data.get("idgrupo");
		MiembroGrupoExalumnos membresia = null;
		Map<String, String> errores = new HashMap<String, String>();
		BindingResult result = new BeanPropertyBindingResult(grupo, "");
		String idUsuarios[] = ((String) data.get("invitados")).split(",");
		
		//for(String id : idUsuarios)logger.info("id: "+id);
		
		
		try {
			grupo = moduloGrupo.loadGrupo(Integer.parseInt(idGrupo));
		} catch (NumberFormatException e1) {
			e1.printStackTrace();
		}

		if (grupo == null)return Collections.singletonMap("errores.exist", "true");
		
		try {
			membresia = moduloGrupo.usuarioPerteneceGrupo(user, grupo);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		if (membresia == null)
			return Collections.singletonMap("errores.exist", "true");

		grupoExAlumnoValidator.validateUsuarioPermisoInvitarGrupo(membresia,
				result);
		errores = ModuloUtilidades.parseErrors(result);
		if (!errores.isEmpty()) {
			errores.put("errores.exist", "true");
			return Collections.singletonMap("error.exist", "true");
		}
		
		for (String idInvitado : idUsuarios) {
			UsuarioAexa usuarioInvitado = new UsuarioAexa();
			usuarioInvitado = moduloUsuario.loadUsuarioAexaById(Long.parseLong(idInvitado));
			
			Long idUsuario = Long.parseLong(idInvitado);
					
			MiembroGrupoExalumnos membresiaExistente = moduloGrupo
					.getMiembroGrupoPorUsuario(idUsuario,
							grupo.getId());

			if (membresiaExistente == null) {
				logger.info("usuarioInvitado.id=" + idUsuario);
				TipoRolMiembroExAlumnos rolInvitado = moduloGrupo
						.getTipoRolesMiembrosExAlumnos("INVITADO");
				
				MiembroGrupoExalumnos membresiaInvitado = new MiembroGrupoExalumnos();
				membresiaInvitado.setTipoRolMiembroExAlumnos(rolInvitado);
				membresiaInvitado.setUsuarioAexa(usuarioInvitado);
				membresiaInvitado.setGrupoExAlumnos(grupo);
				membresiaInvitado.setFechaModificacion(new Date());
				
				try {
					moduloGrupo
							.insertarMiembroGrupoExAlumnos(membresiaInvitado);
				} catch (Exception e) {
					e.printStackTrace();
					return Collections.singletonMap("insertarMiembro.error",
							"true");
				}

			} else
				logger.info("Usuario.id = " + usuarioInvitado.getId()
						+ "existe en membresia de grupo");

		}

		return Collections.singletonMap("flag", "true");

	}

}
