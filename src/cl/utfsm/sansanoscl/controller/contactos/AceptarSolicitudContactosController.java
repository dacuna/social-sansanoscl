package cl.utfsm.sansanoscl.controller.contactos;

import java.util.Collections;
import java.util.Map;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cl.utfsm.aexa.modulos.ModuloUsuarioAexa;
import cl.utfsm.aexa.redsocial.ContactoExAlumnos;

import cl.utfsm.sansanoscl.utils.JsonDeserializer;
import cl.utfsm.sansanoscl.utils.ModuloUtilidades;
import cl.utfsm.sansanoscl.validators.ContactoExAlumnosValidator;

@Controller
@RequestMapping(value = "/contactos/aceptarsolicitud")
public class AceptarSolicitudContactosController {
	@Autowired ModuloUsuarioAexa moduloUsuario;
	@Autowired ContactoExAlumnosValidator validator;

	private static final Logger logger = Logger.getLogger(AceptarSolicitudContactosController.class);

	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody Map<String, ? extends Object> create(@RequestBody Object requested, 
			HttpSession session) {

		ContactoExAlumnos solicitud = new ContactoExAlumnos();
		solicitud = (ContactoExAlumnos) JsonDeserializer.parsingClass(solicitud, requested);

		// validacion
		BindingResult result = new BeanPropertyBindingResult(solicitud, "");
		validator.validateSolicitud(solicitud, result);
		Map<String, String> errores = ModuloUtilidades.parseErrors(result);
		
		if (errores.containsKey("errores.exist"))
			return errores;

		try {
			solicitud.setCodVigencia(1);
			moduloUsuario.updateSolicituContacto(solicitud);
		} catch (Exception e) {
			logger.warn(e);
			return Collections.singletonMap("errores.exist", "true");
		}
		
		return Collections.singletonMap("flag", "true");
	}
	
}
