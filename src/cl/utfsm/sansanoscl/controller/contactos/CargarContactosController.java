package cl.utfsm.sansanoscl.controller.contactos;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cl.utfsm.aexa.AntecedenteLaboral;
import cl.utfsm.aexa.modulos.ModuloUsuarioAexa;
import cl.utfsm.aexa.personas.UsuarioAexa;
import cl.utfsm.sansanoscl.controller.buzon.CargarBuzonController;
import cl.utfsm.sansanoscl.service.UsuarioAexaService;
import cl.utfsm.sansanoscl.utils.ContactoAutocomplete;
import cl.utfsm.sansanoscl.utils.ModuloUtilidades;
import cl.utfsm.sansanoscl.utils.NombresComparator;
import cl.utfsm.sansanoscl.utils.UsuarioSocial;

@Controller
@RequestMapping(value = "/contactos")
public class CargarContactosController {
	@Autowired ModuloUsuarioAexa moduloUsuario;
	@Autowired UsuarioAexaService userService;
	
	private static final Logger logger = Logger.getLogger(CargarBuzonController.class);
	final String VISTA_FORM = "contactos/contactos";

	@RequestMapping(method = RequestMethod.GET)
	public String showForm(Model model, HttpSession session) {
		//se carga la informacion del usuario logeado, para el minibox de la parte superior de la vista
		UsuarioAexa user = userService.getUsuarioAexaPorId((Long)session.getAttribute("userID"));
		logger.info("Cargado usuario: "+user.getId());
		UsuarioSocial usuario = ModuloUtilidades.cargarUsuarioSocial(user);
		usuario.setLaboralActual(userService.getAntecedenteLaboralMasReciente(usuario.getId()));
		Integer userPercentageGood = user.getPorcentajeTrayectoria();
		if(userPercentageGood != null){
			int userPercentageBad = 100 - userPercentageGood;
			model.addAttribute("user_percentage_good", Integer.toString(userPercentageGood));
			model.addAttribute("user_percentage_bad", Integer.toString(userPercentageBad));
		}
		else{
			model.addAttribute("user_percentage_good", "0");
			model.addAttribute("user_percentage_bad", "100");
		}
		model.addAttribute("user_name", ModuloUtilidades.getNombreFormateado((user.getNombreCompleto())));
		usuario.setTitulo(userService.getCarrerasUSMUltimo(user));

		//se cargan los contactos
		List<UsuarioSocial> contactosUsuario = new ArrayList<UsuarioSocial>();
		List<UsuarioAexa> contactos = moduloUsuario.getContactosByIdUser(user.getId());
		logger.info("contactos: "+ contactos.size());
		//se carga la informacion del antecedente laboral mas reciente:
		//al mismo tiempo se crea el sistema de filtros
		Set<String> comunas = new HashSet<String>();
		Set<String> fechasIngreso = new HashSet<String>();
		Set<String> sedes = new HashSet<String>();
		for(UsuarioAexa c:contactos){
			UsuarioSocial contactoUsuario = ModuloUtilidades.cargarUsuarioSocial(c);
			AntecedenteLaboral antecedente = userService.getAntecedenteLaboralMasReciente(c.getId());
			contactoUsuario.setLaboralActual(antecedente);
			contactoUsuario.setTitulo(userService.getCarrerasUSMUltimo(c));
			contactoUsuario.setFechaIngresoUSM(userService.getFechaIngresoExAlumno(c.getRut()));
			contactoUsuario.setNombreSedeIngresoUSM(userService.getSedeIngresoExAlumno(c.getRut()));
			contactosUsuario.add(contactoUsuario);
			comunas.add(contactoUsuario.getComuna().getNombre());
			fechasIngreso.add(ModuloUtilidades.fechaGetYear(contactoUsuario.getFechaIngresoUSM()));
			sedes.add(contactoUsuario.getNombreSedeIngresoUSM());
		}
		
		Collections.sort(contactosUsuario, new NombresComparator());
		model.addAttribute("contactos",contactosUsuario);
		Number totalContactos = userService.getTotalContactosDeUsuario(user.getId());
		model.addAttribute("totalContactos",totalContactos);
		
		model.addAttribute("contactosRecientes",userService.getContactosRecientes(user.getId(), 5));
		model.addAttribute("Utilidades", new ModuloUtilidades());
		
		model.addAttribute("usuario", usuario);
		String[] nombreCompleto = usuario.getNombreCompleto().split(" ");
		if (nombreCompleto.length > 3){
			model.addAttribute("nombrePerfil", nombreCompleto[0] + " " + nombreCompleto[1]);
			model.addAttribute("apellidoPaterno", nombreCompleto[2]);
			model.addAttribute("apellidoMaterno", nombreCompleto[3]);
		}
		else{
			model.addAttribute("nombrePerfil", nombreCompleto[0]);
			model.addAttribute("apellidoPaterno", nombreCompleto[1]);
			model.addAttribute("apellidoMaterno", nombreCompleto[2]);
		}
		model.addAttribute("user", user);
		
		//agrego los filtros precargados
		model.addAttribute("filtroComunas", new TreeSet<String>(comunas));
		model.addAttribute("filtroIngreso", new TreeSet<String>(fechasIngreso));
		model.addAttribute("filtroSedes", new TreeSet<String>(sedes));
		model.addAttribute("sugeridos",userService.getContactosSugeridos(user.getId()));
	
		return VISTA_FORM;

	}
	
	//este metodo se encarga de pasar por ajax los contactos (para el autocomplete)
	@RequestMapping(value="/autocomplete", method=RequestMethod.GET)
	public @ResponseBody Set<ContactoAutocomplete> getContactos(@RequestParam(value="q") String q, HttpSession session) {
		logger.info("PETICION JSON...");
		Integer idUsuario = (Integer) session.getAttribute("userID");
		List<UsuarioAexa> contactos = moduloUsuario.getContactosAutocomplete(q.toUpperCase(), idUsuario);
		Set<ContactoAutocomplete> resultados = new HashSet<ContactoAutocomplete>();
		for(UsuarioAexa u:contactos){
			ContactoAutocomplete agregar = new ContactoAutocomplete();
			agregar.setName(ModuloUtilidades.getNombreFormateado(u.getNombreIncompleto()));
			agregar.setId(u.getId());
			resultados.add(agregar);
		}
		logger.info("se tiene un total de "+resultados.size()+" resultados");
		return resultados;
	}
	@RequestMapping(value="/miniContactos", method=RequestMethod.GET)
	public String getMiniContactos(@RequestParam(value="uid") String uid, Model model, HttpSession session) {
		
		List<UsuarioAexa> contactos = moduloUsuario.getContactosByIdUser(Long.parseLong(uid));
		
		List<ContactoAutocomplete> resultados = new ArrayList<ContactoAutocomplete>();
		for(UsuarioAexa u:contactos){
			ContactoAutocomplete agregar = new ContactoAutocomplete();
			agregar.setName(ModuloUtilidades.getNombreFormateado(u.getNombreIncompleto()));
			agregar.setId(u.getId());
			resultados.add(agregar);
		}
		logger.info("se tiene un total de "+resultados.size()+" resultados");
		model.addAttribute("contactos", resultados);
		return "contactos/miniContactos";
	}
	
	

}
