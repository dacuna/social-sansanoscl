package cl.utfsm.sansanoscl.controller.contactos.testing;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ExtendedModelMap;

import cl.utfsm.sansanoscl.controller.contactos.CargarContactosController;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-context.xml" })
@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional
public class TestCargarContactosController {
	@Autowired
	CargarContactosController controller;

	@Test
	public void showFormTest() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		Assert.assertEquals("formularios/contactos", controller.showForm(
				new ExtendedModelMap(), session));
	}
}
