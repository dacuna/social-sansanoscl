package cl.utfsm.sansanoscl.controller.contactos.testing;

import static org.junit.Assert.assertEquals;

import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import cl.utfsm.sansanoscl.controller.contactos.EliminarContactoController;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-context.xml" })
@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional
public class TestEliminarContactoController {
	@Autowired
	EliminarContactoController controller;

	@Test
	public void deleteTest() {

		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		String idUsuarioEliminar = "11795";

		Map<String, ? extends Object> result = controller.delete(idUsuarioEliminar, session);

		assertEquals(true, result.containsKey("resultado"));
	}
	
	@Test
	public void usuarioFalso() {

		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		String idUsuarioEliminar = "2481240";

		Map<String, ? extends Object> result = controller.delete(idUsuarioEliminar, session);

		assertEquals(true, result.get("error.exist"));
	}
	@Test
	public void eliminarASiMismo() {

		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		String idUsuarioEliminar = "1";

		Map<String, ? extends Object> result = controller.delete(idUsuarioEliminar, session);

		assertEquals(true, result.get("error.exist"));
	}
	
	
}
