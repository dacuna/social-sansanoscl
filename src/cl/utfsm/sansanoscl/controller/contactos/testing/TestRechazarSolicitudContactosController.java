package cl.utfsm.sansanoscl.controller.contactos.testing;

import static org.junit.Assert.assertEquals;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import cl.utfsm.sansanoscl.controller.contactos.RechazarSolicitudContactosController;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-context.xml" })
@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional
public class TestRechazarSolicitudContactosController {
	@Autowired
	RechazarSolicitudContactosController controller;

	@Test
	public void deleteTest() {
		HttpServletResponse response = new MockHttpServletResponse();

		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("idUsuarioAexaEmisor", "11795");
		Map<String, ? extends Object> result = controller.rechazar("11795",
				response, session);
		// recordar que en el controlador devuelve un flag si todo anduvo bien
		assertEquals(false, result.get("resultado"));
	}
	
	@Test
	public void noIdUsuarioAexaEmisor() {
		HttpServletResponse response = new MockHttpServletResponse();

		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		
		data.put("codVigencia", "2");
		Map<String, ? extends Object> result = controller.rechazar(null,
				response, session);
		// recordar que en el controlador devuelve un flag si todo anduvo bien
		assertEquals(true, result.containsKey("error.exist"));
	}

	@Test
	public void idEmisorFalso() {
		HttpServletResponse response = new MockHttpServletResponse();

		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("idUsuarioAexaEmisor", "-1234");
		data.put("codVigencia", "2");
		Map<String, ? extends Object> result = controller.rechazar("1",
				response, session);
		// recordar que en el controlador devuelve un flag si todo anduvo bien
		assertEquals(true, result.get("resultado"));
	}
}
