package cl.utfsm.sansanoscl.controller.contactos.testing;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;

import cl.utfsm.sansanoscl.controller.contactos.ContactosComunesController;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-context.xml" })
@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional
public class TestContactosComunesController {
	@Autowired
	ContactosComunesController controller;

	@Test
	public void showFormTest() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		String idUsuarioComun = "11795";

		Assert.assertEquals("contactos/formularios/contactosComunes", controller.showForm(
				idUsuarioComun, new ExtendedModelMap(), session));
	}
	
	@Test
	public void iucNull() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		
		Model model = new ExtendedModelMap();
		controller.showForm(null, model, session);
		Assert.assertEquals(true, model.containsAttribute("error"));
		
	}
	@Test
	public void iucVacia() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		String idUsuarioComun = "";

		Model model = new ExtendedModelMap();
		controller.showForm(idUsuarioComun, model, session);
		Assert.assertEquals(true, model.containsAttribute("error"));
	}
	@Test
	public void iucFalso() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);
		String idUsuarioComun = "-11795";

		Model model = new ExtendedModelMap();
		controller.showForm(idUsuarioComun, model, session);
		Assert.assertEquals(true, model.containsAttribute("contactosComunes"));
	}
	
}
