package cl.utfsm.sansanoscl.controller.contactos.testing;

import static org.junit.Assert.assertEquals;

import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import cl.utfsm.sansanoscl.controller.contactos.BuscarContactosController;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-context.xml" })
@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional
public class TestBuscarContactosController {
	@Autowired
	BuscarContactosController controller;

	@Test
	public void searchTest() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		String nombre = "ABDON DEL ROSARIO";

		Map<String, ? extends Object> result = controller.search(nombre, session);

		assertEquals(result.containsKey("resultado"), true);
	}
	
	@Test
	public void busquedaVacia() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		String nombre = "";

		Map<String, ? extends Object> result = controller.search(nombre, session);

		assertEquals(result.containsKey("resultado"), true);
	}
	@Test
	public void noBusqueda() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		Map<String, ? extends Object> result = controller.search(null, session);

		assertEquals(result.containsKey("resultado"), true);
	}
}
