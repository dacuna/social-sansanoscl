package cl.utfsm.sansanoscl.controller.contactos.testing;

import static org.junit.Assert.assertEquals;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import cl.utfsm.sansanoscl.controller.contactos.AgregarContactoController;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-context.xml" })
@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional
public class TestAgregarContactoController {
	@Autowired
	AgregarContactoController controller;

	@Test
	public void agregarTest() {
		HttpServletResponse response = new MockHttpServletResponse();

		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("idReceptor", "11800");
		data.put("mensaje", "Un lindo mensaje");

		Map<String, ? extends Object> result = controller.agregar(
				(Object) data, response, session);

		assertEquals(false, result.containsKey("errores.exist"));
	}
	
	@Test
	public void noIdReceptor() {
		HttpServletResponse response = new MockHttpServletResponse();

		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();


		data.put("mensaje", "Un lindo mensaje");

		Map<String, ? extends Object> result = controller.agregar(
				(Object) data, response, session);

		assertEquals(result.containsKey("errores.exist"), true);
	}
	@Test
	public void noMensaje() {
		HttpServletResponse response = new MockHttpServletResponse();

		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("idReceptor", "11800");

		Map<String, ? extends Object> result = controller.agregar(
				(Object) data, response, session);

		assertEquals(result.containsKey("errores.exist"), false);
	}
	
	@Test
	public void idReceptorFalso() {
		HttpServletResponse response = new MockHttpServletResponse();

		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("idReceptor", "-2");
		data.put("mensaje", "Un lindo mensaje");

		Map<String, ? extends Object> result = controller.agregar(
				(Object) data, response, session);

		assertEquals(result.containsKey("errores.exist"), true);
	}
}
