package cl.utfsm.sansanoscl.controller.contactos.testing;

import static org.junit.Assert.assertEquals;

import java.util.LinkedHashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import cl.utfsm.sansanoscl.controller.contactos.AceptarSolicitudContactosController;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-context.xml" })
@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional
public class TestAceptarSolicitudContactosController {
	@Autowired
	AceptarSolicitudContactosController controller;
	
	@Test
	public void todoOK() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("idUsuarioAexaEmisor", "11800");
		data.put("idUsuarioAexaReceptor", "1");

		Map<String, ? extends Object> result = controller.create((Object) data, session);
		// recordar que en el controlador devuelve un flag si todo anduvo bien
		//assertEquals(result.containsKey("errores.exist"), false);
		assertEquals(true, result.containsKey("flag"));
	}
	
	@Test
	public void aceptarSolicitudAjena() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("idUsuarioAexaEmisor", "1");
		data.put("idUsuarioAexaReceptor", "11812");

		Map<String, ? extends Object> result = controller.create((Object) data, session);
		// recordar que en el controlador devuelve un flag si todo anduvo bien
		//assertEquals(result.containsKey("errores.exist"), false);
		assertEquals(result.containsKey("errores.exist"), true);
	}
	
	@Test
	public void noIdUsuarioAexaEmisor() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("idUsuarioAexaReceptor", "1");
		
		Map<String, ? extends Object> result = controller.create((Object) data, session);
		// recordar que en el controlador devuelve un flag si todo anduvo bien
		assertEquals(result.containsKey("errores.exist"), true);
	}
	@Test
	public void noIdUsuarioAexaReceptor() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("idUsuarioAexaEmisor", "11800");
		

		Map<String, ? extends Object> result = controller.create((Object) data, session);
		// recordar que en el controlador devuelve un flag si todo anduvo bien
		assertEquals(result.containsKey("errores.exist"), true);
	}

	@Test
	public void idUsuarioEmisorFalso() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("idUsuarioAexaEmisor", "-2");
		data.put("idUsuarioAexaReceptor", "1");

		Map<String, ? extends Object> result = controller.create((Object) data, session);
		// recordar que en el controlador devuelve un flag si todo anduvo bien
		assertEquals(result.containsKey("errores.exist"), true);
	}
	@Test
	public void idUsuarioReceptorFalso() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("idUsuarioAexaEmisor", "11800");
		data.put("idUsuarioAexaReceptor", "-11812");

		Map<String, ? extends Object> result = controller.create((Object) data, session);
		// recordar que en el controlador devuelve un flag si todo anduvo bien
		assertEquals(result.containsKey("errores.exist"), true);
	}
	@Test
	public void idUsuarioReceptorIgualEmisor() {
		MockHttpSession session = new MockHttpSession();
		session.putValue("userID", 1);

		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

		data.put("idUsuarioAexaEmisor", "1");
		data.put("idUsuarioAexaReceptor", "1");

		Map<String, ? extends Object> result = controller.create((Object) data, session);
		// recordar que en el controlador devuelve un flag si todo anduvo bien
		assertEquals(result.containsKey("errores.exist"), true);
	}
}
