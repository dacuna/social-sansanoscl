package cl.utfsm.sansanoscl.controller.contactos;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import cl.utfsm.aexa.personas.UsuarioAexa;
import cl.utfsm.sansanoscl.service.UsuarioAexaService;
import cl.utfsm.sansanoscl.utils.ModuloUtilidades;

@Controller
@RequestMapping(value = "/contactos/buscarPersonas")
public class BuscarPersonasController {
	@Autowired UsuarioAexaService usuarioService;

	private static final Logger logger = Logger.getLogger(BuscarPersonasController.class);
	
	@RequestMapping(method = RequestMethod.GET)
	public String showForm(@RequestParam(value = "cadena", defaultValue = "") String cadena,
										Model model, HttpSession session) {
		
		List<UsuarioAexa> resultadoBusqueda = usuarioService.buscarPersonas(cadena);
		
		model.addAttribute("Utilidades", new ModuloUtilidades());
		
		model.addAttribute("resultados", resultadoBusqueda);
		
		logger.info("cadena: " + cadena);
		if(resultadoBusqueda != null)
			logger.info("resultados: " + resultadoBusqueda.size());
		
		return "/contactos/buscarPersonas";
	}
}
