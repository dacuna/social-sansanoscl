package cl.utfsm.sansanoscl.controller.contactos;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import cl.utfsm.aexa.modulos.ModuloUsuarioAexa;
import cl.utfsm.aexa.personas.UsuarioAexa;
import cl.utfsm.sansanoscl.dao.UsuarioDao;
import cl.utfsm.sansanoscl.utils.ModuloUtilidades;
import cl.utfsm.sansanoscl.utils.UsuarioSocial;

@Controller
@RequestMapping(value = "/contactos/contactosComunes")
public class ContactosComunesController {
	@Autowired ModuloUsuarioAexa userService;
	@Autowired UsuarioDao usuarioDao;

	private static final Logger logger = Logger.getLogger(ContactosComunesController.class);
	final String VISTA_FORM = "contactos/formularios/contactosComunes";

	@RequestMapping(method = RequestMethod.GET)
	public String showForm(@RequestParam(value="iuc", defaultValue = "null") String iuc, Model model, HttpSession session) {
		//se valida que la id de usuario sea correcta
		if(iuc==null || iuc=="" || !ModuloUtilidades.checkIfNumber(iuc)){
			model.addAttribute("error","true");
			return VISTA_FORM;
		}
		
		UsuarioAexa usuario = userService.loadUsuarioAexaById((Long) session.getAttribute("userID"));

		List<UsuarioAexa> contactos1 = userService.getContactosByIdUser(usuario.getId());
		List<UsuarioAexa> contactos2 = userService.getContactosByIdUser(Long.parseLong(iuc));
		List<UsuarioSocial> contactos = new ArrayList<UsuarioSocial>();

		for (UsuarioAexa c : contactos1) {
			for (UsuarioAexa k : contactos2) {
				if (c.getId() == k.getId()) {
					UsuarioSocial user = new UsuarioSocial();
					user.setNombreCompleto(ModuloUtilidades.getNombreFormateado(c.getNombreCompleto()));
					user.setLaboralActual(usuarioDao.getAntecedenteLaboralMasReciente(c.getId()));
					contactos.add(user);
				}
			}
		}

		logger.info("Agregados un total de " + contactos.size() + " contactos.");
		model.addAttribute("contactosComunes", contactos);

		return VISTA_FORM;
	}

}
