package cl.utfsm.sansanoscl.controller.contactos;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cl.utfsm.aexa.AntecedenteEducacional;
import cl.utfsm.aexa.AntecedenteLaboral;
import cl.utfsm.aexa.modulos.ModuloUsuarioAexa;
import cl.utfsm.aexa.personas.UsuarioAexa;
import cl.utfsm.sansanoscl.service.UsuarioAexaService;
import cl.utfsm.sansanoscl.utils.ModuloUtilidades;
import cl.utfsm.sansanoscl.utils.UsuarioSocial;

@Controller
@RequestMapping(value = "/contactos/sugeridos")
public class CargarContactosSugeridosController {
	@Autowired ModuloUsuarioAexa moduloUsuario;
	@Autowired UsuarioAexaService userService;
	
	private static final Logger logger = Logger.getLogger(CargarContactosSugeridosController.class);
	final String VISTA_FORM = "contactos/sugeridos";
	private static final int contactosMinimos = 2;
	
	@RequestMapping(method = RequestMethod.GET)
	public String showForm(Model model, HttpSession session) {
		//se carga la informacion del usuario logeado, para el minibox de la parte superior de la vista
		UsuarioAexa user = userService.getUsuarioAexaPorId((Long)session.getAttribute("userID"));
		UsuarioSocial usuario = ModuloUtilidades.cargarUsuarioSocial(user);


		List<UsuarioAexa> listaSugeridos= new ArrayList<UsuarioAexa>();
		List<UsuarioAexa> contactos = moduloUsuario.getContactosByIdUser(user.getId());
		
		//System.out.println("cant. contactos: "+ contactos.size());
		
		Set<AntecedenteEducacional> educacionales =  user.getAntecedentesEducacionales();
		Set<AntecedenteLaboral> laborales =  user.getAntecedentesLaborales();
		
		
		for(UsuarioAexa c : contactos){
			List<UsuarioAexa> contactsContacto = moduloUsuario.getContactosByIdUser(c.getId());
			//System.out.println("cant. contactsContacto: "+ contactsContacto.size());
			for(UsuarioAexa cc : contactsContacto){
				if(user.getId()!= cc.getId() && !listaSugeridos.contains(cc) && !moduloUsuario.isContactoExAlumnos(cc.getId(), user.getId())){
					System.out.println("no esta en listasugeridos y no es contacto");
						if(moduloUsuario.getContactosComunes(user, cc).size() > 1){
							listaSugeridos.add(cc);
			                break;
						}
						
				        if(cc.getComuna().getNombre().equals(usuario.getComuna().getNombre())){
				                listaSugeridos.add(c);
				                break;
				        }
				        if(userService.getFechaIngresoExAlumno(cc.getRut()).
				        		equals(userService.getFechaIngresoExAlumno(user.getRut()))){
				                listaSugeridos.add(c);
				                break;
				        }
				        Set<AntecedenteEducacional> educacionesContacto =  c.getAntecedentesEducacionales();
			            for(AntecedenteEducacional e : educacionesContacto )
			                if(comparaEducacional(e,educacionales)){
			                	listaSugeridos.add(cc);
				                break;
			                }
			            Set<AntecedenteLaboral> laboralesContacto = c.getAntecedentesLaborales();
			            for(AntecedenteLaboral e : laboralesContacto ){
			                if(comparaLaboral(e,laborales)){
			                	listaSugeridos.add(cc);
				                break;
			                }
						}
				}
			}
		}
		//if(listaSugeridos.size() <= contactosMinimos ){
			
			List<AntecedenteEducacional> listaAe = moduloUsuario.getAntecedenteEducacional(user);
			System.out.println("listaAe: " + listaAe);
			for(AntecedenteEducacional ae : listaAe){
				Integer anioIngreso = ae.getAnoIngreso();
				Set<UsuarioAexa> lista = moduloUsuario.buscarUsuariosAexaPorAnoIngreso(ae.getSedeCarrera().getCodigo(), ae.getSedeCarrera().getCodigo(), anioIngreso, "Alumnos");
				logger.info("Encontre: " + lista.size());
				for(UsuarioAexa ua : lista )listaSugeridos.add(ua);
			}
		
			
		//}
		logger.info("cant. contactos sugeridoss: "+ listaSugeridos.size());
	
		model.addAttribute("Utilidades", new ModuloUtilidades());
		//Collections.sort(listaSugeridos, new ContactosComunesComparator());
		model.addAttribute("sugeridos", listaSugeridos);
		return VISTA_FORM;

	}
			
	boolean comparaEducacional(AntecedenteEducacional e, Set<AntecedenteEducacional> educaciones ){
		
		for(AntecedenteEducacional i: educaciones){
			try {
				if(i.getCarrera().equals(e.getCarrera()))return true;
				if(i.getIdUniversidad().equals(e.getIdUniversidad()))return true;
				if(i.getOtraInstitucion().equals(e.getOtraInstitucion()))return true;
				if(i.getUniversidad().getNombre().equals(e.getUniversidad().getNombre()))return true;
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			
		}
		
		return false;
		
	}
	
boolean comparaLaboral(AntecedenteLaboral l, Set<AntecedenteLaboral> laborales ){
		
		for(AntecedenteLaboral i: laborales){
			
			try {
				if(i.getCargo().equals(i.getCargo()))return true;
				if(i.getComuna().getNombre().equals(i.getComuna().getNombre()))return true;
				if(i.getDepartamento().equals(i.getDepartamento()))return true;
				if(i.getSucursalEmpresaExtranjeraAexa().getEmpresa().getNombre().
						equals(i.getSucursalEmpresaExtranjeraAexa().getEmpresa().getNombre()))return true;
				else if (i.getSucursalEmpresa().getEmpresa().getNombre().
						equals(i.getSucursalEmpresa().getEmpresa().getNombre()))return true;
			} catch (Exception e) {
				e.printStackTrace();
			}
					
		}
		
		return false;
		
	}
	
	
	
	

}
