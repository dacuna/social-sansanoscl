package cl.utfsm.sansanoscl.controller.contactos;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cl.utfsm.aexa.modulos.ModuloUsuarioAexa;
import cl.utfsm.aexa.personas.UsuarioAexa;

@Controller
@RequestMapping(value = "/contactos/buscarContactos")
public class BuscarContactosController {
	@Autowired ModuloUsuarioAexa moduloUsuario;
	private static final Logger logger = Logger.getLogger(BuscarContactosController.class);

	@RequestMapping(method = RequestMethod.GET)
	public String showForm(Model model, HttpSession session) {
		return "contactos/buscarContactos";
	}

	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody Map<String, ? extends Object> search(@RequestBody
			String nombre, HttpSession session) {

		Integer idUsuario = (Integer) session.getAttribute("userID");

		logger.info(nombre.substring(1, nombre.length()-1));
		List<UsuarioAexa> resultadoBusqueda = moduloUsuario.getContactosAutocomplete(nombre, idUsuario);

		logger.info("size resultado: " + resultadoBusqueda.size());

		return Collections.singletonMap("resultado", resultadoBusqueda);
	}

	
}
