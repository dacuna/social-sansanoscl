package cl.utfsm.sansanoscl.controller.contactos;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cl.utfsm.aexa.modulos.ModuloUsuarioAexa;
import cl.utfsm.aexa.personas.UsuarioAexa;
import cl.utfsm.aexa.redsocial.ContactoExAlumnos;
import cl.utfsm.sansanoscl.service.UsuarioAexaService;
import cl.utfsm.sansanoscl.utils.ModuloUtilidades;
import cl.utfsm.sansanoscl.utils.UsuarioSocial;
import cl.utfsm.sansanoscl.validators.ContactoExAlumnosValidator;

@Controller
@RequestMapping(value = "/contactos/agregarContacto")
public class AgregarContactoController {
	@Autowired ModuloUsuarioAexa moduloUsuario;
	@Autowired UsuarioAexaService userService;
	@Autowired ContactoExAlumnosValidator validator;
	
	final String VISTA_FORM = "contactos/agregarContacto";
	private static final Logger logger = Logger.getLogger(AgregarContactoController.class);

	@RequestMapping(method = RequestMethod.GET)
	public String showForm(@RequestParam(value="id", defaultValue = "null") String id, Model model, HttpSession session) {
		//checkeo que la id del usuario sea valida
		if(!ModuloUtilidades.checkIfNumber(id)){
			model.addAttribute("usuarioAgregarInvalid", true);
			return VISTA_FORM;
		}
		
		//se verifica que el usuario al que se que quiere agregar existe
		UsuarioAexa agregar = moduloUsuario.loadUsuarioAexaById(Long.parseLong(id));

		if(agregar == null){
			model.addAttribute("usuarioAgregarInexistente", true);
			return VISTA_FORM;
		}
		
		UsuarioSocial usuarioAgregar = new UsuarioSocial();
		usuarioAgregar = ModuloUtilidades.cargarUsuarioSocial(agregar);
		usuarioAgregar.setLaboralActual(userService.getAntecedenteLaboralMasReciente(agregar.getId()));
		
		model.addAttribute("usuarioAgregar", usuarioAgregar);

		return VISTA_FORM;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody Map<String, ? extends Object> agregar(@RequestBody Object requested, 
			HttpServletResponse response, HttpSession session) {

		LinkedHashMap<String, String> data = (LinkedHashMap<String, String>) requested;
		String idReceptor = data.get("idReceptor");
		String mensaje = data.get("mensaje");
		ContactoExAlumnos solicitud = new ContactoExAlumnos();
		
		Long receptor = !ModuloUtilidades.checkIfNumber(idReceptor) ? null : Long.parseLong(idReceptor);

		solicitud.setCodVigencia(2);
		solicitud.setIdUsuarioAexaEmisor((Long) session.getAttribute("userID")); //<-- el usuario logeado
		solicitud.setIdUsuarioAexaReceptor(receptor);
		solicitud.setMensaje(mensaje);
		
		BindingResult result = new BeanPropertyBindingResult(solicitud, "");
		validator.validateSolicitud(solicitud, result);
		Map<String, String> errores = ModuloUtilidades.parseErrors(result);
		
		if (!errores.isEmpty())
			return errores;	

		try {
			moduloUsuario.saveContactoEx(solicitud);
		} catch (Exception e) {
			logger.warn(e);
			errores.put("agregarContacto.error", "true");
			return errores;
		}

		return Collections.singletonMap("flag", "true");

	}
 
}
