package cl.utfsm.sansanoscl.controller.contactos;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cl.utfsm.aexa.modulos.ModuloAexa;
import cl.utfsm.aexa.modulos.ModuloUsuarioAexa;
import cl.utfsm.aexa.personas.UsuarioAexa;
import cl.utfsm.aexa.redsocial.ContactoExAlumnos;
import cl.utfsm.sansanoscl.utils.ModuloUtilidades;

@Controller
@RequestMapping(value = "/contactos/rechazarSolicitudContactos")
public class RechazarSolicitudContactosController {
	@Autowired ModuloUsuarioAexa userService;
	@Autowired ModuloAexa aexaService;
	
	@RequestMapping(method = RequestMethod.GET)
	public @ResponseBody
	Map<String, ? extends Object> rechazar(@RequestBody String idUsuarioSolicitudEliminar, 
			HttpServletResponse response, HttpSession session) {

		if(!ModuloUtilidades.checkIfNumber(idUsuarioSolicitudEliminar)){
			return Collections.singletonMap("error.exist", "true");
		}
		
		UsuarioAexa ua1 = userService.loadUsuarioAexaById((Long) session.getAttribute("userID"));

		String idEmisor = idUsuarioSolicitudEliminar;
		Set<ContactoExAlumnos> solicitudContactos = ua1
				.getSolicitudesRecibidas();
		
		for (ContactoExAlumnos sc : solicitudContactos) {
			if (sc.getUsuarioAexaEmisor().getId().equals(
					Integer.parseInt(idEmisor))) {
				try {
					userService.deleteContacto(sc);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return Collections.singletonMap("resultado", false);
				}

			}
		}

		return Collections.singletonMap("resultado", true);

	}

}
