package cl.utfsm.sansanoscl.controller.contactos;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cl.utfsm.aexa.redsocial.ContactoExAlumnos;
import cl.utfsm.sansanoscl.service.UsuarioAexaService;

@Controller
@RequestMapping(value = "/contactos/solicitudes")
public class BuscarSolicitudController {
	@Autowired UsuarioAexaService userService;
	private static final Logger logger = Logger.getLogger(BuscarSolicitudController.class);

	@RequestMapping(method = RequestMethod.GET)
	public String showForm(Model model, HttpSession session) {
		List<ContactoExAlumnos> lista = userService.getSolicitudesContacto((Long) session.getAttribute("userID"));
		
		logger.info("Agregados un total de " + lista.size() + " solicitudes.");
		model.addAttribute("solicitudes", lista);

		return "contactos/solicitudes";

	}

}
