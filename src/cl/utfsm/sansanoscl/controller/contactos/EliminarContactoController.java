package cl.utfsm.sansanoscl.controller.contactos;

import java.util.Collections;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cl.utfsm.aexa.modulos.ModuloAexa;
import cl.utfsm.aexa.modulos.ModuloUsuarioAexa;
import cl.utfsm.aexa.personas.UsuarioAexa;
import cl.utfsm.aexa.redsocial.ContactoExAlumnos;

@Controller
@RequestMapping(value = "/contactos/eliminarContacto")
public class EliminarContactoController {
	@Autowired ModuloUsuarioAexa moduloUsuario;
	@Autowired ModuloAexa aexaService;

	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody Map<String, ? extends Object> delete(@RequestBody String idUsuarioEliminar, 
			HttpSession session) {

		ContactoExAlumnos contacto1 = new ContactoExAlumnos();
		ContactoExAlumnos contacto2 = new ContactoExAlumnos();
		String[] prueba = idUsuarioEliminar.split("\"");
		UsuarioAexa ua2 = moduloUsuario.loadUsuarioAexaById(Long.parseLong(prueba[1]));
		UsuarioAexa ua1 = moduloUsuario.loadUsuarioAexaById((Long) session.getAttribute("userID"));
		
		
		if(ua2==null || ua1==ua2){
			return Collections.singletonMap("error.exist", true);
		}
		
		contacto1.setUsuarioAexaEmisor(ua1);
		contacto1.setUsuarioAexaReceptor(ua2);
		contacto1.setIdUsuarioAexaEmisor(ua1.getId());
		contacto1.setIdUsuarioAexaReceptor(ua2.getId());
		contacto1.setCodVigencia(1);

		contacto2.setUsuarioAexaEmisor(ua2);
		contacto2.setUsuarioAexaReceptor(ua1);
		contacto2.setIdUsuarioAexaEmisor(ua2.getId());
		contacto2.setIdUsuarioAexaReceptor(ua1.getId());
		contacto2.setCodVigencia(1);

		try {
			moduloUsuario.deleteContacto(contacto1);
			moduloUsuario.deleteContacto(contacto2);

		} catch (Exception e) {
			e.printStackTrace();

			return Collections.singletonMap("resultado", true);
		}

		return Collections.singletonMap("resultado", false);

	}

}
