package cl.utfsm.sansanoscl.base;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.apache.log4j.Logger;
import org.springframework.web.context.ConfigurableWebApplicationContext;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.ContextLoaderListener;

public class ConfigLoaderListener extends ContextLoaderListener implements ServletContextListener {
	static Logger log = Logger.getLogger(ConfigLoaderListener.class);
	@SuppressWarnings("unused")
	private static final long serialVersionUID = 8382635910447211020L;
	public static final String CONFIG_FILE_NAME = "contexto.xml";
	public static final String MAPS_FILE_NAME = "mapas.txt";
	private ServletContext servletContext;
	private File rootDir, tempDir;

	public void loadConfigLocations(){
		try{
			BaseUtils utils = new BaseUtils(servletContext);
			rootDir = utils.getRootDirectory();
			tempDir = utils.getTempDirectory("conf");
			servletContext.log("Directorio temporal: " + tempDir.getAbsolutePath());
			List<File> confFiles = new ArrayList<File>();
			List<File> mapFiles = new ArrayList<File>();
			String classpath = System.getProperty("java.class.path");
			servletContext.log("Buscando configuracion en classpath: " + classpath);
			String[] items = classpath.split(File.pathSeparator);
			
			for(String item : items){
				File entry = new File(item);
				procesarEntrada(CONFIG_FILE_NAME, entry, confFiles);
				procesarEntrada(MAPS_FILE_NAME, entry, mapFiles);
			}

			File[] libs = new File(rootDir, "WEB-INF/lib").listFiles();
			
			if(libs != null){
				for(File f : libs){
					procesarEntrada(CONFIG_FILE_NAME, f, confFiles);
					procesarEntrada(MAPS_FILE_NAME, f, mapFiles);
				}
			}
			
			servletContext.log("Se encontro " + confFiles.size() + " archivos de contexto:");
			List<String> unsorted = new ArrayList<String>();
			for ( int i = 0; i < confFiles.size(); i++ ) {
				unsorted.add(confFiles.get(i).toURI().toString());
				servletContext.log("\t--> " + unsorted.get(i));
			}
			
			String[] keywords = {"base", "academia"};
			List<String> sorted = new ArrayList<String>();
			for (String keyword : keywords) {
				for (String s: unsorted.toArray(new String[unsorted.size()])) {
					if (s.indexOf(keyword) != -1) {
						sorted.add(s);
						unsorted.remove(s);
					}	
				}
			}
			sorted.addAll(unsorted);
			servletContext.setAttribute("contextConfigLocations", sorted.toArray(new String[sorted.size()]));
			StringBuffer sb = new StringBuffer();
			for (String s : sorted) {
				sb.append(s + " ");
			}
			servletContext.setAttribute("contextConfigLocation", sb.toString());
			servletContext.log("Se encontro " + mapFiles.size() + " archivos con definiciones de mapas.");
			String[] mapFileLocations = new String[mapFiles.size()];
			for(int i = 0; i < mapFiles.size(); i++){
				mapFileLocations[i] = mapFiles.get(i).getAbsolutePath();
				servletContext.log("\t--> " + mapFileLocations[i]);
			}
			servletContext.setAttribute(SessionBeanLocationsFactoryBean.MAP_LIST_ATTR_NAME,	mapFileLocations);
		}catch (Exception e){
			servletContext.log("Error: ", e);
		}finally{
			if(tempDir != null && tempDir.exists())
				tempDir.delete();
		}
	}

	private void procesarEntrada(String fileName, File entry, List<File> confFiles) {
		try{
			if (entry.isDirectory()){
				File subItem = new File(entry.getAbsolutePath(), fileName);
				if (subItem.exists() && !confFiles.contains(subItem))
					confFiles.add(subItem);
			}else if(entry.exists() && entry.getName().endsWith(".jar")){
				ZipFile jarFile = new ZipFile(entry.getAbsolutePath());
				ZipEntry xmlFile = jarFile.getEntry(fileName);
				if (xmlFile == null)
					return;
				try{
					InputStream is = jarFile.getInputStream(xmlFile);
					File out = new File(tempDir, entry.getName() + fileName.substring(fileName.lastIndexOf('.')));
					if (confFiles.contains(out))
						return;
					FileOutputStream fos = new FileOutputStream(out);
					for (int c = is.read(); c > -1; c = is.read())
						fos.write(c);
					is.close();
					fos.close();
					confFiles.add(out);
				}catch (IOException e){
					servletContext.log("No se pudo leer archivo de configuracion en archivo JAR " + entry.getAbsolutePath());
					servletContext.log(e.getMessage() + " --- " + e.toString());
					return;
				}
			}else if (entry.exists() && entry.getName().equals(fileName) && !confFiles.contains(entry)){
				confFiles.add(entry);
			}
		}catch (Exception e){
			servletContext.log("Error: " + e);
		}
	}
	
	public void contextInitialized(ServletContextEvent sce){
		this.servletContext = sce.getServletContext();
		loadConfigLocations();
		super.contextInitialized(sce);
	}

	protected ContextLoader createContextLoader(){
		return new CustomContextLoader();
	}
}

class CustomContextLoader extends ContextLoader {
	protected void customizeContext(ServletContext servletContext, ConfigurableWebApplicationContext applicationContext){
		applicationContext.setConfigLocation((String) servletContext.getAttribute("contextConfigLocation"));
	}
}