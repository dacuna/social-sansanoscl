package cl.utfsm.sansanoscl.base;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletContext;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.web.context.ServletContextAware;

@SuppressWarnings(value="all")
public class SessionBeanLocationsFactoryBean implements FactoryBean, ServletContextAware {
	public static final String MAP_LIST_ATTR_NAME = "mapListLocations";
	private ServletContext servletContext;
	private String[] mapLocations;
	
	public Object getObject() throws Exception {
		if (mapLocations != null)
			return mapLocations;
		if (servletContext == null)
			return null;
		
		String[] mapLists = (String[]) servletContext.getAttribute(MAP_LIST_ATTR_NAME);
		if (mapLists == null)
			return null;
		
		List<String> tmpMapLocations = new ArrayList<String>();
		for (String fileName: mapLists) {
			try {
				BufferedReader in = new BufferedReader(new FileReader(fileName));
				for (String line = in.readLine(); line != null; line = in.readLine()) {
					line = line.trim();
					if (line.length() > 0 && !line.startsWith("#"))
						tmpMapLocations.add(line);
				}
				in.close();
			} catch (Exception e) {
				servletContext.log("No se pudo leer mapas desde archivo: " + e.getMessage());
				e.printStackTrace();
			}
		}
		mapLocations = tmpMapLocations.toArray(new String[tmpMapLocations.size()]);
		return mapLocations;
	}

	@SuppressWarnings("unchecked")
	public Class getObjectType() {
		return String[].class;
	}

	public boolean isSingleton() {
		return false;
	}

	public void setServletContext(ServletContext sc) {
		this.servletContext = sc;
	}
}
