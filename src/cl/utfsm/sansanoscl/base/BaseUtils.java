package cl.utfsm.sansanoscl.base;

import java.io.File;
import javax.servlet.ServletContext;

public class BaseUtils {
	private ServletContext context;
	
	public BaseUtils (ServletContext ctx) {
		this.context = ctx;
	}
	public ServletContext getContext() {
		return context;
	}
	public void setContext(ServletContext context) {
		this.context = context;
	}
	public String getRootPath() {
		return getRootPath(context);
	}
	public static String getRootPath(ServletContext context) {
		String basePath = context.getRealPath("/");
		if (basePath.lastIndexOf("/") != -1)
			basePath = basePath.substring(0, basePath.lastIndexOf("/"));
		return basePath;
	}
	public File getRootDirectory() {
		return getRootDirectory(getContext());
	}
	public static File getRootDirectory(ServletContext context) {
		return new File(getRootPath(context));
	}
	public File getTempDirectory (String suffix) {
		return getTempDirectory(getContext(), suffix);
	}
	public static File getTempDirectory(ServletContext context, String suffix) {
		File rootDir = getRootDirectory(context);
		String append = "temp/" + suffix;
		File tmpDir = new File (rootDir, append);
		while (tmpDir.exists()) {
			append += "_";
			tmpDir = new File (rootDir, append);
		}		
		tmpDir.mkdirs();
		return tmpDir;
	}
}
