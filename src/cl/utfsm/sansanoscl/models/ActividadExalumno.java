package cl.utfsm.sansanoscl.models;

public class ActividadExalumno {
	private String cargo;
	private String departamento;
	private String nombreEmpresa;
	private String fecha_ingreso;
	private String fecha_egreso;
	private String descripcion;
	
	public ActividadExalumno(String cargo,String departamento,String nombreEmpresa){
		this.cargo=cargo;
		this.departamento=departamento;
		this.nombreEmpresa=nombreEmpresa;
	}
	
	public String getCargo() {
		return cargo;
	}
	
	public void setCargo(String cargo) {
		this.cargo = cargo;
	}
	
	public String getDepartamento() {
		return departamento;
	}
	
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}
	
	public String getNombreEmpresa() {
		return nombreEmpresa;
	}
	
	public void setNombreEmpresa(String nombreEmpresa) {
		this.nombreEmpresa = nombreEmpresa;
	}

	public String getFecha_ingreso() {
		return fecha_ingreso;
	}

	public void setFecha_ingreso(String fecha_ingreso) {
		this.fecha_ingreso = fecha_ingreso;
	}

	public String getFecha_egreso() {
		return fecha_egreso;
	}

	public void setFecha_egreso(String fecha_egreso) {
		this.fecha_egreso = fecha_egreso;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
}
