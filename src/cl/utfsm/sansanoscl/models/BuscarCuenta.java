package cl.utfsm.sansanoscl.models;

public class BuscarCuenta {
	String email;
	String rut;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getRut() {
		return rut;
	}
	public void setRut(String rut) {
		this.rut = rut;
	}
	
}
